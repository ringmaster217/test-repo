package edu.byu.hbll.reporter.queryrunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import org.junit.Test;

public class TimeFacetCalculatorIT {

	@Test
	public void testGetAll() {
		// FIXME: note: last run: 7/3/18, 4 pm
		// April 1, 2018 was a Sunday
		LocalDateTime testTime = LocalDateTime.of(2018, 4, 1, 12, 58);
		// Map<String, List<String>> testTimeResult =
		// TimeFacetCalculator.getFacets(testTime); // FIXME: this is no longer valid
		Map<String, List<String>> testTimeResult = TableTimeFacetCalculator.getAll(testTime);

		// Iterator<Map.Entry<String, String>> it =
		// testTimeResult.entrySet().iterator();
		// Map.Entry<String, String> currentEntry = it.next();
		// assertTrue(currentEntry.getKey().equals("year-0"));
		// assertTrue(currentEntry.getValue().equals("\"_academicYear:2018\""));
		assertTrue(testTimeResult.containsKey("year-0"));
		assertTrue(testTimeResult.get("year-0").get(0).equals("_academicYear:2018"));
		assertTrue(testTimeResult.containsKey("year-1"));
		assertTrue(testTimeResult.get("year-1").get(0).equals("_academicYear:2017"));
		assertTrue(testTimeResult.containsKey("year-2"));
		assertTrue(testTimeResult.get("year-2").get(0).equals("_academicYear:2016"));

		assertTrue(testTimeResult.containsKey("term-0"));
		assertTrue(testTimeResult.get("term-0").get(0).equals("_academicYear:2018"));
		assertTrue(testTimeResult.get("term-0").get(1).equals("_term:2"));
		assertTrue(testTimeResult.containsKey("term-1"));
		assertTrue(testTimeResult.get("term-1").get(0).equals("_academicYear:2018"));
		assertTrue(testTimeResult.get("term-1").get(1).equals("_term:1"));
		assertTrue(testTimeResult.containsKey("term-2"));
		assertTrue(testTimeResult.get("term-2").get(0).equals("_academicYear:2017"));
		assertTrue(testTimeResult.get("term-2").get(1).equals("_term:4"));

		// FIXME: this has to be updated every time we run the test
		String asOfDate = "NOW-3MONTHS-2DAYS";

		assertTrue(testTimeResult.containsKey("week-0"));
		assertTrue(testTimeResult.get("week-0").get(0)
				.equals("_timestamp:[" + asOfDate + "/DAY TO " + asOfDate + "+1DAYS/DAY]"));
		assertTrue(testTimeResult.containsKey("week-1"));
		assertTrue(testTimeResult.get("week-1").get(0)
				.equals("_timestamp:[" + asOfDate + "-7DAYS/DAY TO " + asOfDate + "/DAY]"));
		assertTrue(testTimeResult.containsKey("week-2"));
		assertTrue(testTimeResult.get("week-2").get(0)
				.equals("_timestamp:[" + asOfDate + "-14DAYS/DAY TO " + asOfDate + "-7DAYS/DAY]"));

		assertTrue(testTimeResult.containsKey("day-0"));
		assertTrue(testTimeResult.get("day-0").get(0)
				.equals("_timestamp:[" + asOfDate + "/DAY TO " + asOfDate + "+1DAYS/DAY]"));
		assertTrue(testTimeResult.containsKey("day-1"));
		assertTrue(testTimeResult.get("day-1").get(0)
				.equals("_timestamp:[" + asOfDate + "-1DAYS/DAY TO " + asOfDate + "/DAY]"));
		assertTrue(testTimeResult.containsKey("day-2"));
		assertTrue(testTimeResult.get("day-2").get(0)
				.equals("_timestamp:[" + asOfDate + "-2DAYS/DAY TO " + asOfDate + "-1DAYS/DAY]"));

		// FIXME: this has to be updated every time we run the test
		asOfDate = "NOW-3MONTHS-2DAYS-5HOURS";
		assertTrue(testTimeResult.containsKey("hour-0"));
		assertTrue(testTimeResult.get("hour-0").get(0)
				.equals("_timestamp:[" + asOfDate + "/HOUR TO " + asOfDate + "+1HOURS/HOUR]"));
		assertTrue(testTimeResult.containsKey("hour-1"));
		assertTrue(testTimeResult.get("hour-1").get(0)
				.equals("_timestamp:[" + asOfDate + "-1HOURS/HOUR TO " + asOfDate + "/HOUR]"));
		assertTrue(testTimeResult.containsKey("hour-2"));
		assertTrue(testTimeResult.get("hour-2").get(0)
				.equals("_timestamp:[" + asOfDate + "-2HOURS/HOUR TO " + asOfDate + "-1HOURS/HOUR]"));

		// check the week filter creator more thoroughly
		// April 7, 2018 was a Saturday
		testTime = LocalDateTime.of(2018, 4, 7, 12, 58);
		// testTimeResult = TimeFacetCalculator.getAll(testTime);
		testTimeResult = TableTimeFacetCalculator.getAll(testTime);
		// FIXME: this has to be updated every time we run the test
		asOfDate = "NOW-3MONTHS+4DAYS";
		assertTrue(testTimeResult.containsKey("week-0"));
		assertTrue(testTimeResult.get("week-0").get(0)
				.equals("_timestamp:[" + asOfDate + "-6DAYS/DAY TO " + asOfDate + "+1DAYS/DAY]"));
		assertTrue(testTimeResult.containsKey("week-1"));
		assertTrue(testTimeResult.get("week-1").get(0)
				.equals("_timestamp:[" + asOfDate + "-13DAYS/DAY TO " + asOfDate + "-6DAYS/DAY]"));
		assertTrue(testTimeResult.containsKey("week-2"));
		assertTrue(testTimeResult.get("week-2").get(0)
				.equals("_timestamp:[" + asOfDate + "-20DAYS/DAY TO " + asOfDate + "-13DAYS/DAY]"));

		assertTrue(true);
		assertEquals(8, 4 + 4);
		assertNull(null);
		assertNotNull(" ");
		assertFalse(false);

	}
}
