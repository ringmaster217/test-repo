package edu.byu.hbll.reporter.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import edu.byu.hbll.reporter.Configuration;
import edu.byu.hbll.reporter.statistics.StatisticData;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.util.List;

@Path("")
public class ReportsApi {

    @Inject
    Configuration configuration;

    @Inject
    StatisticData statData;

    ObjectMapper mapper = new ObjectMapper();

    @GET
    @Path("/reports")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getReports() {
        return Response.ok(statData.getReports().toString()).build();
    }

    @GET
    @Path("/reports/{reportId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response testMultParams(
            @PathParam("reportId") String reportId,
            @Context UriInfo ui
    ) {
        return Response.ok(statData.getData(reportId, ui.getQueryParameters()).toString()).build();
    }

    @GET
    @Path("/reports/{reportId}/options")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getOptions(
            @PathParam("reportId") String reportId
    ) {
        return Response.ok(statData.getOptions(reportId).toString()).build();
    }

}
