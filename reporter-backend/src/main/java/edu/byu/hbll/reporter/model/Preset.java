package edu.byu.hbll.reporter.model;

import java.util.ArrayList;

public class Preset {

    String displayName;
    ArrayList<String> values = new ArrayList<>();

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setValues(ArrayList<String> values) {
        this.values = values;
    }

    public ArrayList<String> getValues() {
        return values;
    }

}
