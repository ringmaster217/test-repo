package edu.byu.hbll.reporter.queryrunner.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;

import com.fasterxml.jackson.annotation.JsonProperty;

import edu.byu.hbll.reporter.queryrunner.StatFilterManager;

public class GraphLine implements Comparable<GraphLine> {
	@JsonProperty("LineName")
	private String lineName;

	@JsonProperty("YValues")
	private List<Integer> values;

	private transient String substat;
	private transient int year;
	private transient int term;
	private transient int week;

	public GraphLine(int size) {
		values = new ArrayList<>(Collections.nCopies(size, 0));
	}

	public String getLineName() {
		return lineName;
	}

	public void setLineName(String lineName) {
		this.lineName = lineName;
	}

	public void setLineName(String substat, int year) {
		this.substat = substat;
		this.year = year;
		this.lineName = substat + "-" + year;
	}

	public void setLineName(String substat, int year, int term) {
		this.substat = substat;
		this.year = year;
		this.term = term;
		this.lineName = substat + "-" + year + "-" + term;
	}

	public void setLineName(String substat, int year, int term, int week) {
		this.substat = substat;
		this.year = year;
		this.term = term;
		this.week = week;
		this.lineName = substat + "-" + year + "-" + term + "-" + week;
	}

	public List<Integer> getValues() {
		return values;
	}

	public void setValues(List<Integer> values) {
		this.values = values;
	}

	public void setValue(int value, int index) {
		values.set(index, value);
	}

	/**
	 * After a GraphLine has been completely filled with Solr data, if this was an
	 * off period, there may be some zeroes at the beginning or end of the line.
	 * Rather than showing zeroes on the graph, we'd rather have them set to null.
	 */
	public void fillEmptyValues() {
		// find the first element that isn't zero, and replace the rest with null
		ListIterator<Integer> it = values.listIterator();
		while (it.hasNext() && it.next() == 0) {
			it.set(null);
		}
		if (!it.hasNext()) {
			// we're already at the end
			return;
		}
		// move the iterator to the end
		while (it.hasNext()) {
			it.next();
		}
		// now replace zeroes at the end with null
		while (it.hasPrevious() && it.previous() == 0) {
			it.set(null);
		}
	}

	public void mergeGraphLine(GraphLine newLine) {
		int arrayLen = this.values.size();
		for (int i = 0; i < arrayLen; i++) {
			if (this.values.get(i) == null) {
				this.values.set(i, newLine.values.get(i));
			} else if (newLine.values.get(i) == null) {
				this.values.set(i, this.values.get(i));
			} else {
				this.values.set(i, this.values.get(i) + newLine.values.get(i));
			}
		}
	}

	@Override
	public int compareTo(GraphLine o) {
		if (!this.substat.equals(o.substat)) {
			if (this.substat.equals(StatFilterManager.TOTAL_NAME)) {
				return -1;
			} else if (o.substat.equals(StatFilterManager.TOTAL_NAME)) {
				return 1;
			}
			return this.substat.compareTo(o.substat);
		} else if (this.year < o.year) {
			return -1;
		} else if (this.year > o.year) {
			return 1;
		} else if (this.term < o.term) {
			return -1;
		} else if (this.term > o.term) {
			return 1;
		} else if (this.week < o.week) {
			return -1;
		} else if (this.week > o.week) {
			return 1;
		}
		return 0;
	}
}
