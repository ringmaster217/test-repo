package edu.byu.hbll.reporter.queryrunner;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrRequest.METHOD;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.NoOpResponseParser;
import org.apache.solr.client.solrj.request.QueryRequest;
import org.apache.solr.common.util.NamedList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import edu.byu.hbll.reporter.Configuration;
import edu.byu.hbll.reporter.queryrunner.model.QueryModel;
import edu.byu.hbll.stats.time.Times;
import edu.byu.hbll.stats.time.Times.Marker;

@Path("solrQuery")
@Stateless
public class SolrQueryRunner {

  private static Logger logger = LoggerFactory.getLogger(SolrQueryRunner.class);
  private SolrClient client;
  private ObjectMapper mapper = new ObjectMapper();

  @Inject
  private Configuration config;

  @PostConstruct
  public void initialize() {
    client = config.getSolrClient();
  }

  // --
  // -- PRODUCTION ENDPOINTS --
  // --

  /**
   * Returns a graph response.
   * 
   * @param queryRequest
   * @return Response
   */
  public Response makeRequest(QueryModel queryRequest) { // FIXME: rename method to getGraph; return type: ObjectNode
    logger.info("In makeRequest...");
    return Response.ok(getGraph(queryRequest, false, false).toString()).build();
  }

  /**
   * Returns the stats table as an ObjectNode.
   * 
   * @return ObjectNode
   */
  public ObjectNode getTable() {
    return getTable(false, false, "");
  }

  /**
   * Get a stats table using a custom as-of date.
   * 
   * @param customDate
   *          String which must be in the format "YEAR-MONTH-DAY-HOUR-MINUTE". If
   *          one of those five fields gets left off, the value is defaulted to
   *          "1" for month and day, or "0" for hour and minute. For instance,
   *          "2018-4" will be set to April 1, 2018 0:00.
   * @return ObjectNode
   */

  public ObjectNode getTable(String customDate) {
    return getTable(false, false, customDate);
  }

  public ObjectNode getGraph(QueryModel queryRequest, boolean debug, boolean testLoader) {
    if (queryRequest == null) {
      queryRequest = FakeGraphQueryGenerator.getFakeQuery("week");
    }
    Map<String, JsonNode> jsonQuery = GraphSolrQueryLoader.createQuery(queryRequest, debug);
    if (testLoader) {
      JsonNode returnNode = mapper.convertValue(jsonQuery, JsonNode.class);
      return (ObjectNode) returnNode;
    }

    // prepare the request
    Map<String, JsonNode> solrResJsonMap = new HashMap<>();
    try {
      for (Map.Entry<String, JsonNode> query : jsonQuery.entrySet()) {
        String solrResJsonString = executeSolrQuery(query.getValue().toString(), query.getKey());
        solrResJsonMap.put(query.getKey(), mapper.readTree(solrResJsonString));
      }
      if (debug) {
        logger.info("Returning raw Solr response...");
        JsonNode returnNode = mapper.convertValue(solrResJsonMap, JsonNode.class);
        return (ObjectNode) returnNode;
      }
    } catch (SolrServerException | IOException e) {
      logger.error("Database error: ", e);
      e.printStackTrace();
      return null;
    }

    // Parse the results
    ObjectNode resJson = GraphSolrResponseParser.parseSolrResponse(solrResJsonMap, queryRequest);

    logger.info("Returning constructed graphs...");
    return resJson;
  }

  public ObjectNode getTable(boolean debug, boolean testLoader, String customDate) {
    // create a custom date, if specified
    LocalDateTime customTime = null;
    if (!customDate.equals("")) {
      List<String> customDateFields = new ArrayList<>(Arrays.asList(customDate.split("-")));
      while (customDateFields.size() < 3) {
        customDateFields.add("1");
      }
      while (customDateFields.size() < 5) {
        customDateFields.add("0");
      }
      int[] args = new int[5];
      for (int i = 0; i < 5; i++) {
        args[i] = Integer.parseInt(customDateFields.get(i));
      }
      customTime = LocalDateTime.of(args[0], args[1], args[2], args[3], args[4]);
    }
    Map<String, JsonNode> jsonQuery = TableSolrQueryLoader.createQuery(debug, customTime);
    if (testLoader) {
      JsonNode returnNode = mapper.convertValue(jsonQuery, JsonNode.class);
      return (ObjectNode) returnNode;
    }

    Marker m = Times.single();
    // prepare the request
    Map<String, JsonNode> solrResJsonMap = new HashMap<>();
    try {
      for (Map.Entry<String, JsonNode> query : jsonQuery.entrySet()) {
        String curCollection = query.getKey();
        JsonNode curNode = query.getValue();
        // execute the query
        String solrResJsonString = executeSolrQuery(curNode.toString(), curCollection);
        m.mark();
        solrResJsonMap.put(curCollection, mapper.readTree(solrResJsonString));
      }
      if (debug) {
        logger.info("Returning raw Solr response...");
        JsonNode returnNode = mapper.convertValue(solrResJsonMap, JsonNode.class);
        return (ObjectNode) returnNode;
      }
    } catch (SolrServerException | IOException e) {
      logger.error("Database error: ", e);
      e.printStackTrace();
      return null;
    }

    logger.info(m.toString());

    // parse the results
    ObjectNode resJson = TableSolrResponseParser.parseSolrResponse(solrResJsonMap);

    logger.info("Returning constructed table...");
    return resJson;
  }

  private String executeSolrQuery(String jsonQuery, String collection) throws SolrServerException, IOException {
    logger.info("Querying " + collection + " in Solr cloud...");
    SolrQuery query = new SolrQuery();
    query.setParam("json", jsonQuery);

    // Set "wt" (ResponseWriter) param to "json"; SolrQuery won't let us set this
    QueryRequest request = new QueryRequest(query);
    NoOpResponseParser rawJsonResponseParser = new NoOpResponseParser();
    rawJsonResponseParser.setWriterType("json");
    request.setResponseParser(rawJsonResponseParser);
    request.setMethod(METHOD.POST); // GET method isn't large enough for some of the queries we're sending

    // send the request
    NamedList<Object> res = client.request(request, collection);
    String solrResJsonString = (String) res.get("response");
    logger.info("Successfully retrieved results from " + collection);
    return solrResJsonString;
  }

  // --
  // -- HTTP ENDPOINTS (use for debugging) --
  // --

  @GET
  @Path("testGraph")
  @Produces(MediaType.APPLICATION_JSON)
  public Response testGetGraph() {
    return makeRequest(null);
  }

  @GET
  @Path("testTable")
  @Produces(MediaType.APPLICATION_JSON)
  public Response testGetTable() {
    return Response.ok(getTable().toString()).build();
  }

  /**
   * 
   * @param debug
   *          If true: returns the raw Solr Response
   * @param testLoader
   *          If true: returns the JSON query before it's sent to Solr
   * @param fakeQueryType
   *          Specify one of the fake query types to run. Can do: "week", "term",
   *          "year", or anything else to get a Sessions stat-type query. Defaults
   *          to "week"
   * @return HTTP Response
   */
  @GET
  @Path("graph")
  @Produces(MediaType.APPLICATION_JSON)
  public Response getGraphHttp(@DefaultValue("false") @QueryParam("debug") Boolean debug,
      @DefaultValue("false") @QueryParam("testLoader") Boolean testLoader,
      @DefaultValue("week") @QueryParam("type") String fakeQueryType) {
    QueryModel queryRequest = FakeGraphQueryGenerator.getFakeQuery(fakeQueryType);
    ObjectNode resJson = getGraph(queryRequest, debug, testLoader);
    if (resJson == null) {
      return Response.serverError().build();
    }
    return Response.ok(resJson.toString()).build();
  }

  /**
   * 
   * @param debug
   *          If true: returns the raw Solr Response
   * @param testLoader
   *          If true: returns the JSON query before it's sent to Solr
   * @param customDate
   *          {@link edu.byu.hbll.reporter.queryrunner.SolrQueryRunner#getTable(String)}
   * @return HTTP Response
   */
  @GET
  @Path("table")
  @Produces(MediaType.APPLICATION_JSON)
  public Response getTableHttp(@DefaultValue("false") @QueryParam("debug") Boolean debug,
      @DefaultValue("false") @QueryParam("testLoader") Boolean testLoader,
      @DefaultValue("") @QueryParam("customDate") String customDate) {
    logger.info("getTableEndpoint received response from client...");
    ObjectNode resJson = getTable(debug, testLoader, customDate);
    if (resJson == null) {
      return Response.serverError().build();
    }
    return Response.ok(resJson.toString()).build();
  }
}
