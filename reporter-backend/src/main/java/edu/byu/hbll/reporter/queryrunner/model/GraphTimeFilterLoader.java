package edu.byu.hbll.reporter.queryrunner.model;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import edu.byu.hbll.reporter.queryrunner.model.QueryModel.Timeframe;

/**
 * Note that for all get methods requesting a specific object from a map, if the
 * object doesn't exist, it gets created
 * 
 * @author jtyler17
 *
 */
public class GraphTimeFilterLoader {
	private Map<String, QueryYear> dateRanges;
	private Timeframe timeframe;

	public GraphTimeFilterLoader(QueryModel queryModel) {
		// convert the QueryModel object so that year objects hold terms and term
		// objects hold weeks
		dateRanges = new HashMap<>();
		List<String> queryYears = queryModel.getYears();
		List<Term> queryTerms = queryModel.getTerms();
		List<Week> queryWeeks = queryModel.getWeeks();
		timeframe = queryModel.getTimeframe();
		for (String year : queryYears) {
			addYear(year);
		}
		for (Term term : queryTerms) {
			QueryYear year = getYear(term.getBelongsToYear());
			year.addTerm(term.getTerm());
		}
		for (Week week : queryWeeks) {
			QueryYear year = getYear(week.getBelongsToYear());
			QueryTerm term = year.getTerm(week.getBelongsToTerm());
			term.addWeek(week.getWeek());
		}
	}

	public Timeframe getTimeframe() {
		return timeframe;
	}

	public Set<QueryYear> getAllYears() {
		return new HashSet<QueryYear>(dateRanges.values());
	}

	public QueryYear addYear(String year) {
		QueryYear queryYear = new QueryYear(year);
		dateRanges.put(year, queryYear);
		return queryYear;
	}

	private QueryYear getYear(String year) {
		QueryYear queryYear = dateRanges.get(year);
		if (queryYear == null) {
			queryYear = new QueryYear(year);
			dateRanges.put(year, queryYear);
		}
		return queryYear;
	}

	public static class QueryYear {
		private String year;
		private Map<String, QueryTerm> dateRanges;

		public QueryYear(String year) {
			this.year = year;
			dateRanges = new HashMap<>();
		}

		public String getYear() {
			return year;
		}

		public QueryTerm addTerm(String term) {
			QueryTerm queryTerm = new QueryTerm(term);
			dateRanges.put(term, queryTerm);
			return queryTerm;
		}

		public Set<QueryTerm> getAllTerms() {
			return new HashSet<QueryTerm>(dateRanges.values());
		}

		private QueryTerm getTerm(String term) {
			QueryTerm queryTerm = dateRanges.get(term);
			if (queryTerm == null) {
				queryTerm = new QueryTerm(term);
				dateRanges.put(term, queryTerm);
			}
			return queryTerm;
		}

	}

	public static class QueryTerm {
		private String term;
		private Set<String> weeks;

		public QueryTerm(String term) {
			this.term = term;
			weeks = new HashSet<>();
		}

		public String getTerm() {
			return term;
		}

		public void addWeek(String week) {
			weeks.add(week);
		}

		public Set<String> getWeeks() {
			return weeks;
		}
	}
}
