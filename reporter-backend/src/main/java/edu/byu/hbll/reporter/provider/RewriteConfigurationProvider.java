package edu.byu.hbll.reporter.provider;

import javax.servlet.ServletContext;

import org.ocpsoft.rewrite.annotation.RewriteConfiguration;
import org.ocpsoft.rewrite.config.Configuration;
import org.ocpsoft.rewrite.config.ConfigurationBuilder;
import org.ocpsoft.rewrite.servlet.config.HttpConfigurationProvider;
import org.ocpsoft.rewrite.servlet.config.rule.Join;

@RewriteConfiguration
public class RewriteConfigurationProvider extends HttpConfigurationProvider {

  @Override
  public Configuration getConfiguration(ServletContext arg0) {
    ConfigurationBuilder builder = ConfigurationBuilder.begin();

    builder
      .addRule(Join.path("/").to("/index.html"))
      .addRule(Join.path("/home").to("/index.html"))
      .addRule(Join.path("/report").to("/index.html"));
    
    return builder;
  }

  @Override
  public int priority() {
    return 10;
  }

}
