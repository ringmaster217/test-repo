package edu.byu.hbll.reporter.queryrunner;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import edu.byu.hbll.config.YamlLoader;

public class SolrQueryTemplateManager {
	private static final Logger logger = LoggerFactory.getLogger(SolrQueryTemplateManager.class);
	private static final ObjectMapper mapper = new ObjectMapper();
	private static final YamlLoader yamlLoader = new YamlLoader();
	public static final String CONFIG_TEMPLATES_NODE = "queryTemplates";
	public static final String CONFIG_COLLECTIONS_NODE = "collections";

	private static String queryTemplateDirectory;
	private static List<String> collections;

	// Enforce immutability by making all the get methods return copies
	private static JsonNode templateBase;
	private static JsonNode templateBaseDebug;
	private static JsonNode templateTimeFacets;
	private static JsonNode templateDayFacet;

	public static void initializeTemplates(JsonNode configNode) {
		queryTemplateDirectory = configNode.path("directory").asText();
		templateBase = loadTemplate(configNode.path("base").asText());
		templateBaseDebug = loadTemplate(configNode.path("debug").asText());
		templateTimeFacets = loadTemplate(configNode.path("timeFacet").asText());
		templateDayFacet = loadTemplate(configNode.path("dayFacet").asText());
	}

	public static void initializeCollections(JsonNode configNode) {
		try {
			List<String> collectionsList = mapper.readValue(configNode.toString(), new TypeReference<List<String>>() {
			});
			collections = Collections.unmodifiableList(collectionsList);
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException("Error parsing JSON from config");
		}

		logger.info("Successfully loaded query templates config");
	}

	private static JsonNode loadTemplate(String templatePath) {
		JsonNode returnNode = null;
		logger.info("Loading template file: " + templatePath);
		try {
			InputStream in = GraphSolrQueryLoader.class.getClassLoader()
					.getResourceAsStream(queryTemplateDirectory + templatePath);
			returnNode = yamlLoader.load(new InputStreamReader(in));
		} catch (IOException e) {
			logger.error("Error loading templates", e);
			returnNode = null;
		}
		if (returnNode == null || returnNode.size() == 0) {
			logger.warn("Unable to find template file: " + templatePath);
			throw new RuntimeException("Missing template file");
		}
		logger.info("Successfully loaded template file: " + templatePath);
		return returnNode;
	}

	public static JsonNode getBase() {
		return templateBase.deepCopy();
	}

	public static JsonNode getBaseDebug() {
		return templateBaseDebug.deepCopy();
	}

	public static JsonNode getTimeFacets() {
		return templateTimeFacets.deepCopy();
	}

	public static JsonNode getDayFacet() {
		return templateDayFacet.deepCopy();
	}

	public static List<String> getCollections() {
		return collections;
	}

}
