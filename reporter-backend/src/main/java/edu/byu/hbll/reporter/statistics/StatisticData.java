package edu.byu.hbll.reporter.statistics;

import java.io.IOException;
import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import edu.byu.hbll.json.JsonField;
import edu.byu.hbll.reporter.Configuration;
import edu.byu.hbll.reporter.model.BasicOption;
import edu.byu.hbll.reporter.model.Column;
import edu.byu.hbll.reporter.model.ConditionalOption;
import edu.byu.hbll.reporter.model.GraphReport;
import edu.byu.hbll.reporter.model.Option;
import edu.byu.hbll.reporter.model.OptionValue;
import edu.byu.hbll.reporter.model.PresetGroup;
import edu.byu.hbll.reporter.model.Report;
import edu.byu.hbll.reporter.model.Row;
import edu.byu.hbll.reporter.model.Statistic;
import edu.byu.hbll.reporter.model.TableReport;
import edu.byu.hbll.reporter.queryrunner.SolrQueryRunner;
import edu.byu.hbll.reporter.queryrunner.model.QueryModel;

@Singleton
public class StatisticData {

  @Context
  UriInfo uri;

  @Context
  HttpHeaders httpHeaders;

  @Inject
  Configuration configuration;

  @Inject
  SolrQueryRunner solrQueryRunner;

  ObjectMapper mapper = new ObjectMapper();
  DateInfo dateInfo = new DateInfo();
  Map<String, Report> reports = new HashMap<>();
  Set<String> reportTypes = new HashSet<>();
  Map<String, Statistic> stats = new HashMap<>();
  Map<String, ArrayList<String>> statsByReport = new HashMap<>();

  @PostConstruct
  public void init() {
    setUpStats();
    setUpReports();
  }

  public void setUpStats() {
    JsonNode reportstatsNode = getConfig().path("reportstats");
    JsonNode reporttypesNode = getConfig().path("reporttypes");

    for (JsonNode reporttype : reporttypesNode) {
      reportTypes.add(reporttype.asText());

      // add stats from all reports
      ArrayList<String> statsNames = new ArrayList<>();
      for (JsonNode reportstat : reportstatsNode.path("allreports")) {
        Statistic stat = mapper.convertValue(reportstat, Statistic.class);
        statsNames.add(stat.getName());
        stats.put(stat.getName(), stat);
      }

      for (JsonNode reportstat : reportstatsNode.path(reporttype.asText())) {
        Statistic stat = mapper.convertValue(reportstat, Statistic.class);
        statsNames.add(stat.getName());
        stats.put(stat.getName(), stat);
      }

      statsByReport.put(reporttype.asText(), statsNames);
    }
  }

  public void setUpReports() {
    JsonNode reportsNode = getConfig().path("reports");
    for (JsonNode reportNode : reportsNode) {
      String type = reportNode.path("reportType").asText();
      Report report = new Report();

      if (type.equals("graph")) {
        report = setUpGraphReport(reportNode);
      } else if (type.equals("table")) {
        report = setUpTableReport(reportNode);
      }

      report.setName(reportNode.path("reportTitle").asText());
      report.setIdentifier(report.getName().toLowerCase().replaceAll("[^A-Za-z0-9]", ""));
      reports.put(report.getIdentifier(), report);
    }
  }

  public TableReport setUpTableReport(JsonNode tableInfo) {
    TableReport table = new TableReport();

    // going through columns
    for (JsonNode col : tableInfo.path(("cols"))) {
      if (statsByReport.containsKey(table.getType()) && stats.containsKey(col.path("name").asText())) {
        Statistic currentStat = stats.get(col.path("name").asText());
        Column column = new Column(currentStat.getName(), currentStat.getDisplayName(), true);
        ArrayList<String> substatNames = new ArrayList<>();
        if (col.path("substats").asText().equals("all")) {
          substatNames.addAll(currentStat.getSubstats().keySet());
        } else if (col.path("substats").isArray()) {
          substatNames.addAll(mapper.convertValue(col.path("substats"), ArrayList.class));
        }

        for (String substatName : substatNames) {
          if (col.path("substatsNames").has(substatName)) {
            column.addSubDisplayNames(substatName, col.path("substatsNames").path(substatName).asText());
          } else {
            column.addSubDisplayNames(substatName, currentStat.getSubstat(substatName));
          }
        }
        table.addCol(column);
      }
    }

    // going through rows
    boolean hasSubRows = tableInfo.path("rows").path("hasSubRows").asBoolean();
    JsonNode rowNames = tableInfo.path("rows").path("names");
    JsonNode displayRowNames = tableInfo.path("rows").path("displayNames");
    for (int i = 0; i < rowNames.size(); i++) {
      Row row = new Row(rowNames.path(i).asText(), displayRowNames.path(i).asText(), hasSubRows);
      if (hasSubRows) {
        JsonNode subRowNames;
        JsonNode subRowDisplayNames;
        if (tableInfo.path("rows").path("sameSubRows").asBoolean()) {
          subRowNames = tableInfo.path("rows").path("subNames");
          subRowDisplayNames = tableInfo.path("rows").path("subDisplayNames");
        } else {
          subRowNames = tableInfo.path("rows").path(rowNames.path(i).asText()).path("subNames");
          subRowDisplayNames = tableInfo.path("rows").path(rowNames.path(i).asText()).path("subDisplayNames");
        }

        for (int j = 0; j < subRowNames.size(); j++) {
          row.addSubDisplayNames(rowNames.path(i).asText() + subRowNames.path(j).asText(),
              subRowDisplayNames.path(j).asText());
        }
      }
      table.addRow(row);
    }
    setUpTableOptions(table);
    return table;
  }

  public void setUpTableOptions(TableReport table) {
    // these options will be hardcoded for the meantime...

    // add year
    table.addOption(setUpTableOption(2016, 2019, "year", "Calendar Year"));
    // add month
    table.addOption(setUpTableOption(1, 12, "month", "Month"));
    // add day of month
    table.addOption(setUpTableOption(1, 32, "dom", "Day Of Month"));
    // add Hour
    table.addOption(setUpTableOption(0, 24, "hour", "Hour"));
    // add minute
    table.addOption(setUpTableOption(0, 60, "minute", "Minute"));
  }

  public BasicOption setUpTableOption(int min, int max, String type, String displayName) {
    BasicOption basicOption = new BasicOption();

    for (int i = min; i < max; i++) {
      String current = String.valueOf(i);
      OptionValue optionValue;
      if (type.equals("month")) {
        optionValue = new OptionValue(current, DateFormatSymbols.getInstance().getMonths()[i - 1]);
      } else {
        optionValue = new OptionValue(current, current);
      }

      basicOption.addOptionValue(optionValue);
    }
    basicOption.setUrlParam(type);
    basicOption.setDisplayName(displayName);
    basicOption.setDisplayStyle(1);
    return basicOption;
  }

  public GraphReport setUpGraphReport(JsonNode graphInfo) {
    GraphReport graphReport = new GraphReport();

    // going through groups
    List<String> groups = Arrays.asList(new String[] { "yaxis", "xaxis" });
    for (String group : groups) {
      JsonField groupOptions = new JsonField(graphInfo.path(group).path("options"));
      graphReport.addDisplayName(group, graphInfo.path(group).path("displayName").asText());
      for (JsonField optionField : groupOptions) {
        String optionName = optionField.getKey();
        JsonNode optionNode = optionField.getValue();
        Option option = new Option();

        if (optionNode.path("statOption").asBoolean(false) == true) {
          option = setUpStatOption(graphReport, optionNode);
        } else if (optionNode.path("type").asText().equals("conditional")) {
          option = setUpConditionalOption(graphReport, optionNode);
        } else if (optionNode.path("type").asText().equals("basic")) {
          option = setUpBasicOption(graphReport, optionNode);
        }
        option.setUrlParam(optionName);
        graphReport.addOption(optionName, option);
        graphReport.addDisplayName(optionName, option.getDisplayName());
        if (group.equals("yaxis")) {
          graphReport.addYAxisOption(optionName);
        } else if (group.equals("xaxis")) {
          graphReport.addXAxisOption(optionName);
        }
      }
    }

    return graphReport;
  }

  public ConditionalOption setUpStatOption(GraphReport report, JsonNode optionNode) {
    ConditionalOption option = new ConditionalOption();
    option.setDisplayName(optionNode.path("displayName").asText());
    option.setConditionalParam(optionNode.path("conditionalParam").asText());
    option.setConditionalDisplayName(optionNode.path("conditionalDisplayName").asText());
    for (JsonNode stat : optionNode.path("stats")) {
      if (statsByReport.containsKey(report.getType()) && stats.containsKey(stat.path("name").asText())) {
        BasicOption subStatOption = new BasicOption();
        subStatOption.setUrlParam(stat.path("name").asText());
        Statistic currentStat = stats.get(stat.path("name").asText());
        subStatOption.setDisplayName(currentStat.getDisplayName());
        report.addDisplayName(currentStat.getName(), currentStat.getDisplayName());
        ArrayList<String> substatNames = new ArrayList<>();

        if (stat.path("substats").asText().equals("all")) {
          substatNames.addAll(currentStat.getSubstats().keySet());
        } else if (stat.isArray()) {
          substatNames.addAll(mapper.convertValue(stat, ArrayList.class));
        }

        for (String substatName : substatNames) {
          if (stat.path("substatsNames").has(substatName)) {
            subStatOption
                .addOptionValue(new OptionValue(substatName, stat.path("substatsNames").path(substatName).asText()));
            report.addDisplayName(substatName, stat.path("substatsNames").path(substatName).asText());
          } else {
            subStatOption.addOptionValue(new OptionValue(substatName, currentStat.getSubstat(substatName)));
            report.addDisplayName(substatName, currentStat.getSubstat(substatName));
          }
        }
        option.addOption(subStatOption.getUrlParam(), subStatOption);
      }
    }

    return option;
  }

  public ConditionalOption setUpConditionalOption(GraphReport report, JsonNode optionNode) {
    ConditionalOption option = new ConditionalOption();
    option.setDisplayName(optionNode.path("displayName").asText());
    option.setConditionalParam(optionNode.path("conditionalParam").asText());
    option.setConditionalDisplayName(optionNode.path("conditionalDisplayName").asText());

    JsonField subOptionsNode = new JsonField(optionNode.path("options"));
    for (JsonField subOptionField : subOptionsNode) {
      String subOptionName = subOptionField.getKey();
      JsonNode subOptionNode = subOptionField.getValue();

      // the subOptions of conditional will always be basic?
      BasicOption basicOption = setUpBasicOption(report, subOptionNode);
      basicOption.setUrlParam(subOptionName);
      report.addDisplayName(subOptionName, basicOption.getDisplayName());
      option.addOption(subOptionName, basicOption);

      // check for presets in the options of the conditional
      if (optionNode.path("options").path(subOptionName).path("hasPresets").asBoolean()) {
        PresetGroup preset = mapper.convertValue(optionNode.path("options").path(subOptionName).path("presets"),
            PresetGroup.class);
        report.addPreset(subOptionName, preset);
      }
    }

    return option;
  }

  public BasicOption setUpBasicOption(GraphReport report, JsonNode optionNode) {
    BasicOption option = new BasicOption();
    option.setDisplayName(optionNode.path("displayName").asText());

    int valuesNum = optionNode.path("optionValues").size();
    for (int i = 0; i < valuesNum; i++) {
      String subUrlValue = optionNode.path("optionValues").path(i).asText();
      String subDisplayName = optionNode.path("optionDisplayNames").path(i).asText();
      OptionValue optionValue = new OptionValue(subUrlValue, subDisplayName);
      report.addDisplayName(subUrlValue, subDisplayName);
      option.addOptionValue(optionValue);
    }

    return option;
  }

  public ObjectNode getReports() {
    ObjectNode result = mapper.createObjectNode();
    ArrayNode reportsNode = mapper.createArrayNode();
    Map<String, ArrayNode> reportsByType = new HashMap<>();

    UriBuilder uriBuilder = uri.getRequestUriBuilder();
    if (httpHeaders.getRequestHeader("x-forwarded-proto") != null) {
      uriBuilder.scheme(httpHeaders.getHeaderString("x-forwarded-proto"));
    }

    if (httpHeaders.getRequestHeader("x-forwarded-port") != null) {
      uriBuilder.port(Integer.parseInt(httpHeaders.getHeaderString("x-forwarded-port")));
    }

    if (httpHeaders.getRequestHeader("x-forwarded-host") != null) {
      uriBuilder.host(httpHeaders.getHeaderString("x-forwarded-host"));
    }

    result.put("baseUrl", uriBuilder.build().toString());

    for (String reportType : reportTypes) {
      reportsByType.put(reportType, mapper.createArrayNode());
    }

    for (Report report : reports.values()) {
      ObjectNode reportNode = mapper.createObjectNode();
      reportNode.put("displayName", report.getName());
      reportNode.put("identifier", report.getIdentifier());
      UriBuilder reportUri = UriBuilder.fromUri(uriBuilder.build());
      reportUri.path(report.getIdentifier());

      // table has options temporarily to test all places in the table
      // if (report.getType().equals("graph")) {
      reportUri.path("options");
      reportNode.put("optionsUrl", reportUri.toString());
      // }

      reportsByType.get(report.getType()).add(reportNode);
    }

    for (Map.Entry<String, ArrayNode> entry : reportsByType.entrySet()) {
      String type = entry.getKey();
      ObjectNode report = mapper.createObjectNode();
      report.put("type", type);
      report.put("displayName", (type + "s").toUpperCase());
      report.set("reports", entry.getValue());

      reportsNode.add(report);
    }
    result.put("types", reportsByType.size());
    result.put("amount", reports.size());
    result.set("reports", reportsNode);
    return result;
  }

  public ObjectNode getReportNotFoundNode() {
    ObjectNode node = mapper.createObjectNode();
    node.put("message", "there is no report with that identifier");
    return node;
  }

  public ObjectNode getOptions(String report) {
    if (!reports.containsKey(report)) {
      return getReportNotFoundNode();
    }

    return reports.get(report).getInfo("options");
  }

  public ObjectNode getData(String report, MultivaluedMap<String, String> params) {

    if (!reports.containsKey(report)) {
      return getReportNotFoundNode();
    }

    Report currentReport = reports.get(report);
    if (currentReport.getType().equals("table")) {
      TableReport tableReport = (TableReport) currentReport;
      ObjectNode node = mapper.createObjectNode();
      node.setAll(tableReport.getColsInfo());
      node.setAll(tableReport.getRowsInfo());
      node.put("displayTitle", tableReport.getName());
      if (params.containsKey("year") && params.containsKey("month") && params.containsKey("dom")
          && params.containsKey("hour") && params.containsKey("minute")) {
        String date = params.getFirst("year") + "-" + params.getFirst("month") + "-" + params.getFirst("dom") + "-"
            + params.getFirst("hour") + "-" + params.getFirst("minute");
        node.set("tableData", solrQueryRunner.getTable(date));
      } else {
        node.set("tableData", solrQueryRunner.getTable());
      }
      return node;
    } else if (currentReport.getType().equals("graph")) {
      GraphReport graphReport = (GraphReport) currentReport;
      return getGraphData(graphReport, params);
    }

    return mapper.createObjectNode();
  }

  public ObjectNode getGraphData(GraphReport report, MultivaluedMap<String, String> params) {
    ObjectNode graphData = mapper.createObjectNode();
    String timeFrameOptionName = "timeframe";
    String timeframeParam = report.getConditionalParamOfOption(timeFrameOptionName);
    String statOptionName = "stat";
    String subStatParam = report.getConditionalParamOfOption(statOptionName);
    String aggregationOptionName = "aggr";

    // asking about the necessary stats to make the solr request
    if (params.containsKey(timeFrameOptionName) && params.containsKey(timeframeParam)
        && params.containsKey(statOptionName) && params.containsKey(subStatParam)) {
      QueryModel queryModel = new QueryModel();
      boolean hasMultSubStats = false;

      // add timeframe information in to the queryModel
      String selectedTimeFrame = params.getFirst(timeFrameOptionName);
      for (String time : params.get(timeframeParam)) {
        List<String> timeInfo = Arrays.asList(time.split("-"));
        if (selectedTimeFrame.equals("year")) {
          queryModel.setTimeframe(QueryModel.Timeframe.YEAR);
          queryModel.addYear(timeInfo.get(0));
        } else if (selectedTimeFrame.equals("term")) {
          queryModel.setTimeframe(QueryModel.Timeframe.TERM);
          queryModel.addTerm(timeInfo.get(0), timeInfo.get(1));
        } else if (selectedTimeFrame.equals("week")) {
          queryModel.setTimeframe(QueryModel.Timeframe.WEEK);
          queryModel.addWeek(timeInfo.get(0), timeInfo.get(1), timeInfo.get(2));
        }
      }

      // add the stat and sub stats in the queryModel
      queryModel.setStat(params.getFirst(statOptionName));
      if (params.get(subStatParam).size() > 1)
        hasMultSubStats = true;
      queryModel.addAllSubStats(params.get(subStatParam));

      try {
        graphData = (ObjectNode) mapper.readTree(solrQueryRunner.makeRequest(queryModel).getEntity().toString());
      } catch (IOException e) {
        e.printStackTrace();
      }

      // check up on aggregation and do post processing for aggregation
      if (params.containsKey("aggr")) {
        String aggregation = params.getFirst("aggr");
        graphData.put("Aggregation", aggregation);
        if (aggregation.equals("cumulative")) {
          doCumulativeChanges(graphData);
        } else if (aggregation.equals("percentage")) {
          doPercentageChanges(graphData);
        }
      }

      // iterate through the lines that were grabbed.
      graphData.put("Timeframe", selectedTimeFrame);
      List<String> lines = new ArrayList<>();
      lines.add("GraphLines");

      if (params.containsKey(aggregationOptionName)) {
        if (params.getFirst(aggregationOptionName).equals("percentage")) {
          lines.add("TotalGraphLines");
        }
      }

      for (String line : lines) {
        ArrayNode graphLines = (ArrayNode) graphData.path(line);
        for (int i = 0; i < graphLines.size(); i++) {
          ObjectNode graphLine = (ObjectNode) graphLines.get(i);
          String lineName = graphLine.path("LineName").asText();
          List<String> nameInfo = Arrays.asList(lineName.split("-"));
          String currentSubStat = nameInfo.get(0).trim();
          String year = report.getDisplayName(nameInfo.get(1));
          StringBuilder newLineName = new StringBuilder();
          StringBuilder keyColName = new StringBuilder();
          StringBuilder keyName = new StringBuilder();

          newLineName.append(year);
          keyColName.append(year);
          keyName.append(report.getDisplayName(currentSubStat));
          if (selectedTimeFrame.equals("term")) {
            String term = getTermName(nameInfo.get(2));
            newLineName.insert(0, term + " ");
            if (hasMultSubStats) {
              keyColName.insert(0, term + " ");
            } else {
              keyName.append(" - " + term);
            }
          } else if (selectedTimeFrame.equals("week")) {
            String term = getTermName(nameInfo.get(2));
            String week = nameInfo.get(3);
            newLineName.insert(0, term + " ");
            keyColName.insert(0, term + " ");
            newLineName.append(" / " + report.getDisplayName("week") + " " + week);
            if (hasMultSubStats) {
              keyColName.append(" - " + report.getDisplayName("week") + " " + week);
            } else {
              keyName.append(" - " + report.getDisplayName("week") + " " + week);
            }
          }
          graphLine.put("KeyColName", keyColName.toString());
          graphLine.put("KeyName", keyName.toString());
          if (currentSubStat.equals("tota")) {
            graphLine.put("LineType", "dashed");
          } else {
            graphLine.put("LineType", "solid");
          }
          graphLine.put("LineName", newLineName.toString());
        }
      }
    } else if (params.containsKey(timeFrameOptionName)) {
      graphData = createEmptyGraph(params.getFirst(timeFrameOptionName));
    } else {
      // create an empty graph
      graphData = createEmptyGraph("");
    }

    // creates the title for the graph, y-axis, and x-axis
    if (params.containsKey(statOptionName) && params.containsKey(timeFrameOptionName)) {
      String selectedTimeframe = params.getFirst(timeFrameOptionName);
      String rate;
      String time;

      if (selectedTimeframe.equals("week")) {
        rate = "Day";
        time = report.getDisplayName("term") + " " + report.getDisplayName(selectedTimeframe);
      } else {
        rate = report.getDisplayName("week");
        time = report.getDisplayName(selectedTimeframe);
      }

      String selectedStat = report.getDisplayName(params.getFirst(statOptionName));
      String title = selectedStat + "/" + rate + " by " + time;

      if (params.containsKey(aggregationOptionName)) {
        if (params.getFirst(aggregationOptionName).equals("cumulative")) {
          title = report.getDisplayName(params.getFirst(aggregationOptionName)) + " " + selectedStat + " by " + time;
        }
      }

      graphData.put("GraphTitle", title);
      graphData.put("XAxis", report.getDisplayName(params.getFirst(timeFrameOptionName)));
      graphData.put("YAxis", selectedStat);
    }
    return graphData;
  }

  public ObjectNode createEmptyGraph(String timeframe) {
    ObjectNode graph = mapper.createObjectNode();
    ArrayNode labels = mapper.createArrayNode();
    ArrayNode graphLines = mapper.createArrayNode();
    ArrayNode yValues = mapper.createArrayNode();
    ObjectNode graphLine = mapper.createObjectNode();

    if (timeframe.equals("year")) {
      List<String> terms = Arrays.asList(new String[] { "Fall", "Winter", "Spring", "Summer" });

      for (int i = 0; i < 4; i++) {
        for (int j = 0; j < dateInfo.getWeeksOfTerm(i + 1); j++) {
          labels.add(String.valueOf(j + 1) + "/" + terms.get(i));
          yValues.addNull();
        }
      }
    } else if (timeframe.equals("week")) {
      for (int i = 0; i < 7; i++) {
        labels.add("Day " + String.valueOf(i + 1));
        yValues.addNull();
      }
    } else {
      for (int i = 0; i < 16; i++) {
        labels.add(String.valueOf(i + 1));
        yValues.addNull();
      }
    }
    graphLine.set("YValues", yValues);
    graphLine.put("LineName", "");
    graphLine.put("KeyName", "");
    graphLines.add(graphLine);
    graph.put("LinesNumber", 1);
    graph.set("Labels", labels);
    graph.set("GraphLines", graphLines);
    return graph;
  }

  public void doCumulativeChanges(ObjectNode lines) {
    int numOfLines = lines.path("LinesNumber").asInt();
    int numOfValues = lines.path("Labels").size();
    // iterate through lines
    for (int l = 0; l < numOfLines; l++) {
      JsonNode yValues = lines.path("GraphLines").path(l).path("YValues");
      long sum = 0;
      ArrayNode values = mapper.createArrayNode();
      boolean seenValue = false;
      for (int v = 0; v < numOfValues; v++) {
        if (yValues.hasNonNull(v)) {
          seenValue = true;
          sum += yValues.path(v).asLong();
        }
        if (seenValue) {
          values.add(sum);
        } else {
          values.addNull();
        }
      }

      ObjectNode graphLine = (ObjectNode) lines.path("GraphLines").path(l);
      graphLine.set("YValues", values);
    }
  }

  public void doPercentageChanges(ObjectNode lines) {
    // parse through lines and find totals, store by date of line, (years, terms or
    // week)
    // for each line
    // iterate through the values if nonnull calculate the percentage to the total,
    // and store changes
    Map<String, JsonNode> totalLines = new HashMap<>();
    int numOfLines = lines.path("LinesNumber").asInt();
    int numofValues = lines.path("Labels").size();

    // first iteration to get the total lines
    ArrayNode totalLinesNode = mapper.createArrayNode();
    for (int l = 0; l < numOfLines; l++) {
      JsonNode line = lines.path("GraphLines").path(l);
      String lineName = line.path("LineName").asText();
      String date = lineName.substring(lineName.indexOf("-") + 1);
      String substat = lineName.substring(0, lineName.indexOf("-"));
      if (!totalLines.containsKey(date) && substat.equals("total")) {
        totalLines.put(date, line);
        totalLinesNode.add(line);
      }
    }
    lines.set("TotalGraphLines", totalLinesNode);

    // second iteration go through each line and add it to a new graph info node
    ArrayNode graphLines = mapper.createArrayNode();
    for (int l = 0; l < numOfLines; l++) {
      ObjectNode newLine = mapper.createObjectNode();
      ObjectNode line = (ObjectNode) lines.path("GraphLines").path(l);
      String lineName = line.path("LineName").asText();
      String date = lineName.substring(lineName.indexOf("-") + 1);
      JsonNode total = totalLines.get(date);

      ArrayNode newValues = mapper.createArrayNode();
      JsonNode lineValues = line.path("YValues");
      JsonNode totalValues = total.path("YValues");

      for (int v = 0; v < numofValues; v++) {
        if (lineValues.hasNonNull(v)) {
          newValues.add(lineValues.path(v).asDouble() / totalValues.path(v).asDouble() * 100.0);
        } else {
          newValues.addNull();
        }
      }
      newLine.put("LineName", lineName);
      newLine.set("YValues", newValues);
      graphLines.add(newLine);
    }
    lines.set("GraphLines", graphLines);

  }

  private String getTermName(String term) {
    switch (term) {
    case "1":
      return "Fall";
    case "2":
      return "Winter";
    case "3":
      return "Spring";
    case "4":
      return "Summer";
    }
    return "";
  }

  public JsonNode getConfig() {
    return configuration.getConfig();
  }

}