package edu.byu.hbll.reporter.model;

import java.util.LinkedHashMap;
import java.util.Map;

public class Statistic {

    String name;
    String displayName;
    Map<String, String> substats = new LinkedHashMap<>();


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public Map<String, String> getSubstats() {
        return substats;
    }

    public String getSubstat(String substat){
        return substats.get(substat);
    }

    public void addSubStat(String substat, String displayName){
        substats.put(substat, displayName);
    }

    public void setSubstats(Map<String, String> substats) {
        this.substats = substats;
    }

}
