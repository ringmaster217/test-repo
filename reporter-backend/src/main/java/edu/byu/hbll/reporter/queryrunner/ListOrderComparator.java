package edu.byu.hbll.reporter.queryrunner;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class ListOrderComparator implements Comparator<String> {
	private List<String> lookupList;

	public ListOrderComparator(List<String> lookupList) {
		// clone the list, rather than sharing references
		this.lookupList = new ArrayList<>();
		for (String element : lookupList) {
			this.lookupList.add(element);
		}
	}

	public ListOrderComparator addListElement(String element) {
		lookupList.add(element);
		return this;
	}

	public ListOrderComparator insertListElement(int index, String element) {
		lookupList.add(index, element);
		return this;
	}

	@Override
	public int compare(String s1, String s2) {
		int listLen = lookupList.size();
		int s1Index = listLen + 1; // Strings not found in the list get moved to the back
		int s2Index = listLen + 1;
		for (int i = 0; i < listLen; i++) {
			String curElementName = lookupList.get(i);
			if (curElementName.equals(s1)) {
				s1Index = i;
			}
			if (curElementName.equals(s2)) {
				s2Index = i;
			}
		}
		if (s1Index == s2Index) {
			// either they're identical strings, or neither string was found in the list; do
			// regular string comparison
			return s1.compareTo(s2);
		}
		return s1Index - s2Index;
	}
}
