package edu.byu.hbll.reporter.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Option {

    @JsonIgnore
    String type;
    String displayName;

    @JsonProperty("urlParameter")
    String urlParam;
    boolean multiAllowed = false;
    int displayStyle = 0;

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getUrlParam() {
        return urlParam;
    }

    public void setUrlParam(String urlParam) {
        this.urlParam = urlParam;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isMultiAllowed() {
        return multiAllowed;
    }

    public void setMultiAllowed(boolean multiAllowed) {
        this.multiAllowed = multiAllowed;
    }

    public int getDisplayStyle() {
        return displayStyle;
    }

    public void setDisplayStyle(int displayStyle) {
        this.displayStyle = displayStyle;
    }

    public void setDisplay(String display){
        switch(display){
            case "dropdown":
                displayStyle = 1;
                multiAllowed = false;
                break;
            case "checkbox":
                displayStyle = 0;
                multiAllowed = true;
                break;
            case "radio":
                displayStyle = 0;
                multiAllowed = false;
                break;
        }
    }
}
