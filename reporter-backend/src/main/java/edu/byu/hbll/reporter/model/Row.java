package edu.byu.hbll.reporter.model;

import java.util.*;

public class Row {
    String rowName;
    String displayName;
    boolean hasSubRows;
    Map<String, String> displayNames = new LinkedHashMap<>();

    public Row(String rowName, String displayName, boolean hasSubRows) {
        this.rowName = rowName;
        this.displayName = displayName;
        this.hasSubRows = hasSubRows;
    }

    public String getRowName() {
        return rowName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public boolean getHasSubRows() {
        return hasSubRows;
    }

    public void addSubDisplayNames(String subName, String subDisplayName) {
        displayNames.put(subName, subDisplayName);
    }

    public Map<String, String> getDisplayNames() {
        return displayNames;
    }

}