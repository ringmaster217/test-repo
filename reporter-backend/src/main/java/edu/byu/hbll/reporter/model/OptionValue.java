package edu.byu.hbll.reporter.model;

public class OptionValue {
    String displayName;
    String urlValue;

    public OptionValue(String urlValue, String displayName) {
        this.urlValue = urlValue;
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getUrlValue() {
        return urlValue;
    }

    public void setUrlValue(String urlValue) {
        this.urlValue = urlValue;
    }

}
