package edu.byu.hbll.reporter.queryrunner.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class CustomTimeGroup {
	public static final int MAX_VAL = 12;
	// NOTE: interval is inclusive, ie: [begin, end]
	private final int begin;
	private final int end;

	@JsonCreator
	public CustomTimeGroup(@JsonProperty("begin") int begin, @JsonProperty("end") int end) {
		this.begin = begin;
		this.end = end;
	}

	public int getBegin() {
		return begin;
	}

	public int getEnd() {
		return end;
	}

	public boolean contains(int checkVal) {
		if (begin > end) {
			if ((checkVal >= begin && checkVal <= MAX_VAL) || (checkVal > 0 && checkVal <= end)) {
				return true;
			}
			return false;
		} else {
			if (checkVal >= begin && checkVal <= end) {
				return true;
			}
			return false;
		}
	}
}
