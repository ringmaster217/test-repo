package edu.byu.hbll.reporter.queryrunner.model;

import edu.byu.hbll.reporter.queryrunner.model.QueryModel.Timeframe;

public class GraphLabel implements Comparable<GraphLabel> {
	public static final String[] TERM_LABELS = { "Fall", "Winter", "Spring", "Summer" };
	public static final String[] DAY_LABELS = { "Day 1", "Day 2", "Day 3", "Day 4", "Day 5", "Day 6", "Day 7" };
	private int day;
	private int week;
	private int term;
	private final Timeframe timeframe;

	// FIXME: since we're using this as a map key, either need to make immutable or
	// be careful about how we're using it
	public GraphLabel(Timeframe timeframe) {
		this.timeframe = timeframe;
	}

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}

	public int getWeek() {
		return week;
	}

	public void setWeek(int week) {
		this.week = week;
	}

	public int getTerm() {
		return term;
	}

	public void setTerm(int term) {
		this.term = term;
	}

	public GraphLabel decrLabel() {
		GraphLabel newLabel = new GraphLabel(timeframe);
		newLabel.setTerm(term);
		if (timeframe == Timeframe.WEEK) {
			newLabel.setWeek(week);
			newLabel.setDay(day - 1);
		} else {
			newLabel.setWeek(week - 1);
		}
		return newLabel;
	}

	public GraphLabel incrLabel() {
		GraphLabel newLabel = new GraphLabel(timeframe);
		newLabel.setTerm(term);
		if (timeframe == Timeframe.WEEK) {
			newLabel.setWeek(week);
			newLabel.setDay(day + 1);
		} else {
			newLabel.setWeek(week + 1);
		}
		return newLabel;
	}

	@Override
	public String toString() {
		switch (timeframe) {
		case YEAR:
			return Integer.toString(week) + "/" + TERM_LABELS[term - 1];
		case TERM:
			return Integer.toString(week);
		case WEEK:
			return DAY_LABELS[day - 1];
		}
		return null;
	}

	@Override
	public int compareTo(GraphLabel o) {
		switch (timeframe) {
		case YEAR:
			if (this.term < o.term) {
				return -1;
			} else if (this.term > o.term) {
				return 1;
			} else {
				if (this.week < o.week) {
					return -1;
				} else if (this.week > o.week) {
					return 1;
				}
			}
			break;
		case TERM:
			if (this.week < o.week) {
				return -1;
			} else if (this.week > o.week) {
				return 1;
			}
			break;
		case WEEK:
			if (this.day < o.day) {
				return -1;
			} else if (this.day > o.day) {
				return 1;
			}
			break;
		}
		return 0;
	}

	@Override
	public boolean equals(Object o) {
		if (o == null) {
			return false;
		} else if (!(o instanceof GraphLabel)) {
			return false;
		}
		if (this.hashCode() != o.hashCode()) {
			return false;
		}
		GraphLabel oLabel = (GraphLabel) o;
		switch (timeframe) {
		case YEAR:
			if (this.term != oLabel.term) {
				return false;
			} else if (this.week != oLabel.week) {
				return false;
			}
			break;
		case TERM:
			if (this.week != oLabel.week) {
				return false;
			}
			break;
		case WEEK:
			if (this.day != oLabel.day) {
				return false;
			}
		}
		return true;
	}

	@Override
	public int hashCode() {
		switch (timeframe) {
		case YEAR:
			return (term * 31) + week;
		case TERM:
			return week;
		case WEEK:
			return day;
		}
		return 0;
	}
}
