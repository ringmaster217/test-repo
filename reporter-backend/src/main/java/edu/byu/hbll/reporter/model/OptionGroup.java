package edu.byu.hbll.reporter.model;

import java.util.ArrayList;

public class OptionGroup {
    String displayName;
    ArrayList<String> optionKeys = new ArrayList<>();

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public ArrayList<String> getOptionKeys() {
        return optionKeys;
    }

    public void addOptionKey(String optionKey){
        optionKeys.add(optionKey);
    }

    public void setOptions(ArrayList<String> optionKeys) {
        this.optionKeys = optionKeys;
    }
}
