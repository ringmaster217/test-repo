package edu.byu.hbll.reporter.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import edu.byu.hbll.reporter.statistics.DateInfo;

public class Report {

    String name;
    String type;
    String identifier;
    DateInfo dateInfo = new DateInfo();

    ObjectMapper mapper = new ObjectMapper();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public ObjectNode getInfo(String field, String... params) {
        ObjectNode defaultNode = mapper.createObjectNode();
        defaultNode.put("message", "This report doesn\'t have any " + field);
        return defaultNode;
    }

}