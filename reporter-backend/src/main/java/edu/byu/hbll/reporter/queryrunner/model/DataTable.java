package edu.byu.hbll.reporter.queryrunner.model;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import edu.byu.hbll.reporter.queryrunner.ListOrderComparator;
import edu.byu.hbll.reporter.queryrunner.StatFilterManager;
import edu.byu.hbll.reporter.queryrunner.TableTimeFacetCalculator;

public class DataTable {
	private Map<String, Map<String, Map<String, Map<String, Object>>>> dataRows;

	public DataTable() {
		List<String> rowGroupNames = TableTimeFacetCalculator.getRowGroupNames();
		dataRows = new TreeMap<>(new ListOrderComparator(rowGroupNames));
		for (String rowGroupName : rowGroupNames) {
			dataRows.put(rowGroupName, new TreeMap<>());
		}

		List<String> rowNames = TableTimeFacetCalculator.getRowNames();
		for (String rowName : rowNames) { // do for each of the 15 row names (3 for each row group)
			List<String> primaryStatNames = StatFilterManager.getPrimaryStatNames();
			Map<String, Map<String, Object>> dataRow = new TreeMap<>(new ListOrderComparator(primaryStatNames));

			for (String primaryStatName : primaryStatNames) {
				addStatFunctionsToColumns(dataRow, primaryStatName, null);
				List<String> substatNames = StatFilterManager.getSubstatNames(primaryStatName);
				Map<String, Object> dataColumnGroup = new TreeMap<>(
						new ListOrderComparator(substatNames).insertListElement(0, StatFilterManager.TOTAL_NAME));
				dataColumnGroup.put(StatFilterManager.TOTAL_NAME, null);

				for (String substatName : substatNames) {
					dataColumnGroup.put(substatName, null);
					addStatFunctionsToColumns(dataRow, primaryStatName, substatName);
				}
				dataRow.put(primaryStatName, dataColumnGroup);
			}

			String rowGroupName = getRowGroupName(rowName);
			dataRows.get(rowGroupName).put(rowName, dataRow);
		}
	}

	private void addStatFunctionsToColumns(Map<String, Map<String, Object>> curDataRow, String primaryStatName,
			String substatName) {
		// add column groups and applicable columns for all the functions of a
		// particular substat or primary stat (such as mean and median)
		Map<String, String> functions = null;
		String replaceCountWith = null;
		String columnName = primaryStatName + "-" + substatName;
		if (substatName == null) {
			functions = StatFilterManager.getPrimaryFunctions(primaryStatName);
			replaceCountWith = StatFilterManager.getPrimaryStatReplaceCountWith(primaryStatName);
			columnName = primaryStatName + "-" + StatFilterManager.TOTAL_NAME;
		} else {
			functions = StatFilterManager.getSubstatFunctions(primaryStatName, substatName);
			replaceCountWith = StatFilterManager.getSubstatReplaceCountWith(primaryStatName, substatName);
		}
		if (functions == null) {
			return;
		}
		for (Map.Entry<String, String> function : functions.entrySet()) {
			// add the function if its key does not match the replaceCountWith variable
			String functionName = function.getKey();
			if (replaceCountWith == null || !functionName.equals(replaceCountWith)) {
				Map<String, Object> curColumnGroup = curDataRow.get(functionName);
				if (curColumnGroup == null) {
					// this function hasn't been added yet
					curDataRow.put(functionName, new TreeMap<>());
					curColumnGroup = curDataRow.get(functionName);
				}
				curColumnGroup.put(columnName, null);
			}
		}
	}

	public void putValue(String rowName, String statGroupName, String statName, Object value) {
		// value is type Object because it can be either an Integer or a Double
		String rowGroupName = getRowGroupName(rowName);
		Map<String, Object> leafNode = dataRows.get(rowGroupName).get(rowName).get(statGroupName);
		Object currentValue = leafNode.get(statName);
		if (currentValue == null) {
			leafNode.put(statName, value);
		} else {
			// There's already a value here; add them together
			if (currentValue instanceof Integer && value instanceof Integer) {
				Integer newValue = (int) currentValue + (int) value;
				leafNode.put(statName, newValue);
			} else if (currentValue instanceof Double && value instanceof Double) {
				Double newValue = (double) currentValue + (double) value;
				leafNode.put(statName, newValue);
			} else {
				// something weird happened; either a different type of data value or type
				// mismatch
				throw new RuntimeException("Error putting value in stats table (unexpected types); "
						+ "current value is type " + currentValue.getClass().getName() + ", and new value is type "
						+ value.getClass().getName());
			}
		}
	}

	private String getRowGroupName(String rowName) {
		return rowName.split("-")[0];
	}

	public Map<String, Map<String, Map<String, Map<String, Object>>>> getDataRows() {
		CalculatePercentValues();
		return dataRows;
	}

	private void CalculatePercentValues() {
		// for substats with "percent" marked as true, we need to go in, calculate
		// those values, and replace what's currently there
		Map<String, List<String>> percentSubstats = StatFilterManager.getPercentSubstats();
		// need to do it for each time node
		for (String timeNode : TableTimeFacetCalculator.getRowNames()) {
			for (Map.Entry<String, List<String>> entry : percentSubstats.entrySet()) {
				// set up on the primary stat node (the "stat group" node)
				String rowGroupName = getRowGroupName(timeNode);
				// leaf node contains a map to both the total value and the substat(s) we need
				// to recalculate
				Map<String, Object> leafNode = dataRows.get(rowGroupName).get(timeNode).get(entry.getKey());
				Object totalValue = leafNode.get(StatFilterManager.TOTAL_NAME);
				if (totalValue == null) {
					continue; // can't divide by null value
				}
				double castTotalValue = 0.0;
				if (totalValue instanceof Integer) {
					castTotalValue = ((Integer) totalValue).doubleValue();
				} else if (totalValue instanceof Double) {
					castTotalValue = (Double) totalValue;
				}
				if ((int) castTotalValue == 0) {
					continue; // can't divide by 0
				}
				// our denominator is now ready to go
				for (String substatName : entry.getValue()) {
					Object substatValue = leafNode.get(substatName);
					if (substatValue == null) {
						continue; // can't use a null value in the numerator
					}
					double castSubstatValue = 0.0;
					if (substatValue instanceof Integer) {
						castSubstatValue = ((Integer) substatValue).doubleValue();
					} else if (substatValue instanceof Double) {
						castSubstatValue = (Double) substatValue;
					}
					// Perform the calculation, then replace the value
					Double newValue = castSubstatValue / castTotalValue;
					leafNode.put(substatName, newValue);
				}
			}
		}
	}
}
