package edu.byu.hbll.reporter.model;

import java.util.ArrayList;

public class PresetGroup {

    String displayName;
    ArrayList<Preset> presets = new ArrayList();

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public ArrayList<Preset> getPresets() {
        return presets;
    }

    public void setPresets(ArrayList<Preset> presets) {
        this.presets = presets;
    }
}
