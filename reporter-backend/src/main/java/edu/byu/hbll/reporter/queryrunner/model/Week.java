package edu.byu.hbll.reporter.queryrunner.model;

public class Week {

    String belongsToYear;
    String belongsToTerm;
    String week;

    public Week(String belongsToYear, String belongsToTerm, String week) {
        this.belongsToYear = belongsToYear;
        this.belongsToTerm = belongsToTerm;
        this.week = week;
    }

    public String getBelongsToYear() {
        return belongsToYear;
    }

    public void setBelongsToYear(String belongsToYear) {
        this.belongsToYear = belongsToYear;
    }

    public String getBelongsToTerm() {
        return belongsToTerm;
    }

    public void setBelongsToTerm(String belongsToTerm) {
        this.belongsToTerm = belongsToTerm;
    }

    public String getWeek() {
        return week;
    }

    public void setWeek(String week) {
        this.week = week;
    }
}
