package edu.byu.hbll.reporter.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BasicOption extends Option {

    @JsonIgnore
    List<OptionValue> optionValues = new ArrayList<>();

    @JsonIgnore
    Map<String, String> displayNames = new HashMap<>();

    public BasicOption() {
        type = "basic";
    }

    public void addOptionValue(OptionValue optionValue) {
        optionValues.add(optionValue);
    }

    public List<OptionValue> getOptionValues() {
        return optionValues;
    }

    public void addDisplayName(String optionVal, String optionValDisplayName){
        displayNames.put(optionVal, optionValDisplayName);
    }
}
