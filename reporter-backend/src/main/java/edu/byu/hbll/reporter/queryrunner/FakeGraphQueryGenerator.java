package edu.byu.hbll.reporter.queryrunner;

import edu.byu.hbll.reporter.queryrunner.model.QueryModel;

public class FakeGraphQueryGenerator {

	public static QueryModel getFakeQuery(String queryType) {
		if (queryType.equals("year")) {
			return createFakeYearQuery();
		} else if (queryType.equals("term")) {
			return createFakeTermQuery();
		} else if (queryType.equals("week")) {
			return createFakeWeekQuery();
		} else {
			return createFakeSessionQuery();
		}
	}

	private static QueryModel createFakeYearQuery() {
		QueryModel queryRequest = new QueryModel();
		queryRequest.addYear("2016");
		queryRequest.addYear("2017");
		queryRequest.addYear("2018");
		queryRequest.setTimeframe(QueryModel.Timeframe.YEAR);
		queryRequest.setStat("conversions");
		queryRequest.addSubStat("total");
		queryRequest.addSubStat("fulltext");
		queryRequest.addSubStat("coligo");
		queryRequest.addSubStat("illiad");
		return queryRequest;
	}

	private static QueryModel createFakeTermQuery() {
		QueryModel queryRequest = new QueryModel();
		queryRequest.addTerm("2017", "1");
		queryRequest.addTerm("2017", "2");
		queryRequest.setTimeframe(QueryModel.Timeframe.TERM);
		queryRequest.setStat("searches");
		queryRequest.addSubStat("basic");
		queryRequest.addSubStat("advanced");
		queryRequest.addSubStat("faceted");
		queryRequest.addSubStat("pagination");
		return queryRequest;
	}

	private static QueryModel createFakeWeekQuery() {
		QueryModel queryRequest = new QueryModel();
		queryRequest.addWeek("2017", "1", "1");
		queryRequest.addWeek("2017", "2", "1");
		queryRequest.addWeek("2017", "3", "1");
		queryRequest.addWeek("2017", "4", "1");
		queryRequest.setTimeframe(QueryModel.Timeframe.WEEK);
		queryRequest.setStat("recordview");
		queryRequest.addSubStat("total");
		queryRequest.addSubStat("searchres");
		queryRequest.addSubStat("holding");
		return queryRequest;
	}

	private static QueryModel createFakeSessionQuery() {
		QueryModel queryRequest = new QueryModel();
		queryRequest.addTerm("2017", "1");
		queryRequest.addTerm("2017", "2");
		queryRequest.setTimeframe(QueryModel.Timeframe.TERM);
		queryRequest.setStat("sessions");
		queryRequest.addSubStat("total");
		queryRequest.addSubStat("authenticated");
		queryRequest.addSubStat("distauthusers");
		queryRequest.addSubStat("zombie");
		return queryRequest;
	}
}
