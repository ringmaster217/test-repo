package edu.byu.hbll.reporter.queryrunner;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class TableSolrQueryLoader {
	private static final Logger logger = LoggerFactory.getLogger(GraphSolrQueryLoader.class);
	private static ObjectMapper mapper = new ObjectMapper();

	/**
	 * Primary function call for creating table queries.
	 * 
	 * @return Map of all the queries to run, where the key is the collection name
	 *         and the value is a list of JSON-formatted queries.
	 */

	public static Map<String, JsonNode> createQuery(Boolean debug, LocalDateTime customDate) {
		logger.info("Constructing new query request...");
		Map<String, JsonNode> jsonQueries = new HashMap<>();

		Map<String, List<String>> dateFacets;
		if (customDate != null) {
			dateFacets = TableTimeFacetCalculator.getAll(customDate);
		} else {
			dateFacets = TableTimeFacetCalculator.getAll();
		}

		for (String curCollection : SolrQueryTemplateManager.getCollections()) {
			jsonQueries.put(curCollection, forEachCollection(debug, curCollection, dateFacets));
		}
		return jsonQueries;
	}

	public static JsonNode forEachCollection(Boolean debug, String curCollection,
			Map<String, List<String>> dateFacets) {
		ObjectNode jsonQuery;
		// add basic parameters; if we're doing a debug, use a different template
		if (debug) {
			jsonQuery = (ObjectNode) SolrQueryTemplateManager.getBaseDebug();
		} else {
			jsonQuery = (ObjectNode) SolrQueryTemplateManager.getBase();
		}
		ObjectNode facetNode = jsonQuery.with("facet");
		for (Map.Entry<String, List<String>> curDate : dateFacets.entrySet()) {
			// add a node under "facet" for each date value
			// (ie: each row in the stats table)
			ObjectNode dateNode = (ObjectNode) forEachDateFacet(curCollection, curDate.getKey(), curDate.getValue());
			if (dateNode != null) {
				facetNode.set(curDate.getKey(), dateNode);
			}
		}
		return jsonQuery;
	}

	public static JsonNode forEachDateFacet(String curCollection, String curDateName, List<String> curDateFilters) {
		ObjectNode dateFacetNode = mapper.createObjectNode();
		// add basic parameters
		dateFacetNode.put("type", "query").with("domain").set("filter", mapper.valueToTree(curDateFilters));
		// add a timezone parameter for some date values
		String facetType = curDateName.split("-")[0];
		if (facetType.equals("week") || facetType.equals("day") || facetType.equals("hour")) {
			dateFacetNode.put("TZ", TableTimeFacetCalculator.getTimeZone());
		}

		ObjectNode facetNode = dateFacetNode.with("facet");
		List<String> primaryStatNames = StatFilterManager.getPrimaryStatNames();
		for (String primaryStat : primaryStatNames) {
			// add a node under "facet" for each primary stat
			ObjectNode primaryStatNode = (ObjectNode) forEachPrimaryStat(curCollection, primaryStat);
			if (primaryStatNode != null) {
				facetNode.set(primaryStat, primaryStatNode);
			}
		}
		return dateFacetNode;
	}

	public static JsonNode forEachPrimaryStat(String curCollection, String curPrimaryStat) {
		// if this primary stat isn't on the current collection, don't add it
		if (!StatFilterManager.primaryStatContainsCollection(curPrimaryStat, curCollection)) {
			return null;
		}
		ObjectNode primaryStatNode = mapper.createObjectNode();
		// add basic parameters
		primaryStatNode.put("type", "query");
		String primaryStatFilter = StatFilterManager.getPrimaryStatFilter(curPrimaryStat, curCollection);
		if (primaryStatFilter != null) {
			primaryStatNode.with("domain").put("filter", primaryStatFilter);
		} else {
			// if no filter has been specified, just do a basic "for all"
			primaryStatNode.put("q", "*");
		}
		ObjectNode facetNode = primaryStatNode.with("facet");
		// add any functions under the "facet" node
		Map<String, String> primaryFunctions = StatFilterManager.getPrimaryFunctions(curPrimaryStat);
		if (primaryFunctions != null) { // ex: unique: "unique(_sessionId)"
			for (Map.Entry<String, String> function : primaryFunctions.entrySet()) {
				facetNode.put(function.getKey(), function.getValue());
			}
		}

		List<String> substats = StatFilterManager.getSubstatNames(curPrimaryStat);
		for (String substat : substats) {
			// add a node under "facet" for each substat
			ObjectNode substatNode = (ObjectNode) forEachSubstat(curCollection, curPrimaryStat, substat);
			if (substatNode != null) {
				facetNode.set(substat, substatNode);
			}
		}
		return primaryStatNode;
	}

	public static JsonNode forEachSubstat(String curCollection, String primaryStat, String substat) {
		// if this substat isn't on the current collection, don't add it
		if (!StatFilterManager.substatContainsCollection(primaryStat, substat, curCollection)) {
			return null;
		}
		String substatFilter = StatFilterManager.getSubstatFilter(primaryStat, substat);
		if (substatFilter == null) {
			return null;
		}
		ObjectNode substatNode = mapper.createObjectNode();
		substatNode.put("type", "query");
		substatNode.with("domain").put("filter", substatFilter);
		// add any functions under the "facet" node; don't add the facet node unless
		// there are functions, though!
		Map<String, String> substatFunctions = StatFilterManager.getSubstatFunctions(primaryStat, substat);
		if (substatFunctions != null) { // ex: unique: "unique(_sessionId)"
			ObjectNode facetNode = substatNode.with("facet");
			for (Map.Entry<String, String> function : substatFunctions.entrySet()) {
				facetNode.put(function.getKey(), function.getValue());
			}
		}
		return substatNode;
	}
}
