package edu.byu.hbll.reporter.queryrunner.model;

public class Term {

    String belongsToYear;
    String term;

    public Term(String belongsToYear, String term) {
        this.belongsToYear = belongsToYear;
        this.term = term;
    }

    public String getBelongsToYear() {
        return belongsToYear;
    }

    public void setBelongsToYear(String belongsToYear) {
        this.belongsToYear = belongsToYear;
    }

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

}
