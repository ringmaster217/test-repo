package edu.byu.hbll.reporter.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class ConditionalOption extends Option {

    @JsonIgnore
    Map<String, Option> options = new LinkedHashMap<>();

    @JsonIgnore
    Map<String, String> displayNames = new HashMap<>();

    @JsonIgnore
    String conditionalParam;

    @JsonIgnore
    String conditionalDisplayName;


    public ConditionalOption() {
        type = "conditional";
        displayStyle = 1;
    }

    public void addOption(String key, Option option) {
        option.setMultiAllowed(true);
        options.put(key, option);
    }

    public Option getOption(String key) {
        return options.get(key);
    }

    public Map<String, Option> getOptions() {
        return options;
    }

    public String getConditionalParam() {
        return conditionalParam;
    }

    public void setConditionalParam(String conditionalParam) {
        this.conditionalParam = conditionalParam;
    }

    public String getConditionalDisplayName() {
        return conditionalDisplayName;
    }

    public void setConditionalDisplayName(String conditionalDisplayName) {
        this.conditionalDisplayName = conditionalDisplayName;
    }

    public void addDisplayName(String option, String optionDisplayName){
        displayNames.put(option, optionDisplayName);
    }
}
