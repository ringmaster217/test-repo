package edu.byu.hbll.reporter.model;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.util.*;

public class GraphReport extends Report {

    Map<String, String> displayNames = new HashMap<>();
    Map<String, Option> options = new HashMap<>();
    Map<String, PresetGroup> presets = new HashMap<>();
    Map<String, String> optionKeys = new HashMap<>();
    ArrayList<String> yAxisOptions = new ArrayList<>();
    ArrayList<String> xAxisOptions = new ArrayList<>();
    ArrayList<OptionGroup> optionGroups = new ArrayList<>();
    String optionsDisplayName;

    public GraphReport() {
        this.type = "graph";
    }

    public void setOptionsDisplayName(String optionsDisplayName){
        this.optionsDisplayName = optionsDisplayName;
    }

    public Option getOption(String key) {
        return options.get(key);
    }

    public void addOption(String key, Option option) {
        options.put(key, option);
    }

    public Map<String, Option> getOptions() {
        return options;
    }

    public String getConditionalParamOfOption(String optionName) {
        if (!options.containsKey(optionName))
            return null;

        Option option = options.get(optionName);
        if (option.getType().equals("conditional"))
            return ((ConditionalOption) option).getConditionalParam();

        return option.getUrlParam();

    }

    public void addOptionName(String key, String optionName) {
        optionKeys.put(key, optionName);
    }

    public String getOptionName(String key) {
        return optionKeys.get(key);
    }

    public void addXAxisOption(String optionName) {
        xAxisOptions.add(optionName);
    }

    public void addYAxisOption(String optionName) {
        yAxisOptions.add(optionName);
    }

    public void addDisplayName(String option, String displayName) {
        displayNames.put(option, displayName);
    }

    public String getDisplayName(String key) {
        return displayNames.get(key);
    }

    public void addPreset(String type, PresetGroup presetGroup) {
        presets.put(type, presetGroup);
    }

    public Map<String, PresetGroup> getPresets() {
        return presets;
    }

    public void addOptionGroup(OptionGroup optionGroup){
        optionGroups.add(optionGroup);
    }

    public ObjectNode getPresetsInfo(String timeframe) {
        ObjectNode presetsInfo = mapper.createObjectNode();
        String currentTime = "2018-3-4";
        // will wait until we have live data to use the following line
        // String currentTime = dateInfo.getCurrentDate();
        List<String> current = Arrays.asList(currentTime.split("-"));
        ArrayNode calculatedPresets = mapper.createArrayNode();
        PresetGroup timeframePresets = presets.get(timeframe);
        for (int i = 0; i < timeframePresets.getPresets().size(); i++) {
            ObjectNode preset = mapper.createObjectNode();
            preset.put("displayName", timeframePresets.getPresets().get(i).getDisplayName());
            ArrayNode values = mapper.createArrayNode();
            for (int j = 0; j < timeframePresets.getPresets().get(i).getValues().size(); j++) {
                String value = timeframePresets.getPresets().get(i).getValues().get(j);
                List<String> changes = Arrays.asList(value.split("-"));
                int year = Integer.parseInt(current.get(0)) - Integer.parseInt(changes.get(0));
                int term = Integer.parseInt(current.get(1)) - Integer.parseInt(changes.get(1));
                int week = 0;

                if (changes.size() > 2) {
                    week = Integer.parseInt(current.get(2)) - Integer.parseInt(changes.get(2));
                    if (week <= 0) {
                        term--;
                        week += dateInfo.getWeeksOfTerm(term);
                    }
                }

                if (term <= 0) {
                    term += 4;
                    year--;
                }

                if (changes.size() > 2) {
                    if (week <= dateInfo.getWeeksOfTerm(term))
                        values.add(year + "-" + term + "-" + week);
                } else if (changes.size() == 2) {
                    values.add(year + "-" + term);
                }
            }
            preset.set("optionUrls", values);
            calculatedPresets.add(preset);
        }
        presetsInfo.put("displayName", timeframePresets.getDisplayName());
        presetsInfo.set("presets", calculatedPresets);
        return presetsInfo;
    }

    public ObjectNode getStatInfo(String stat) {
        ConditionalOption statOption = (ConditionalOption) options.get("stat");
        BasicOption subStat = (BasicOption) statOption.getOptions().get(stat);
        ObjectNode statInfo = mapper.valueToTree(subStat);
        statInfo.remove("type");
        statInfo.put("urlParam", statOption.getConditionalParam());
        return statInfo;
    }

    public ObjectNode getYearsInfo() {
        ConditionalOption timeframe = (ConditionalOption) options.get("timeframe");
        BasicOption years = (BasicOption) timeframe.getOptions().get("year");
        ObjectNode yearsInfo = mapper.valueToTree(years);
        yearsInfo.remove("type");
        yearsInfo.put("urlParam", timeframe.getConditionalParam());
        return yearsInfo;
    }

    public ObjectNode getOptionsInfo() {
        ObjectNode reportInfo = mapper.createObjectNode();
        reportInfo.put("displayTitle", "Graph Controls");

        ArrayNode groups = mapper.createArrayNode();
        ObjectNode yAxisGroup = mapper.createObjectNode();
        ArrayNode yOptions = mapper.createArrayNode();
        for (String optionName : yAxisOptions) {
            Option option = options.get(optionName);
            ObjectNode optionNode = mapper.valueToTree(option);
            optionNode.put("optionID", option.getUrlParam());
            yOptions.add(optionNode);

            if (option.getType().equals("conditional")) {
                ConditionalOption currentOption = (ConditionalOption) option;

                ArrayNode subOptions = mapper.createArrayNode();
                ArrayNode defaultSelected = mapper.createArrayNode();
                ArrayNode consequentOptions = mapper.createArrayNode();
                for (Option subOption : currentOption.getOptions().values()) {
                    BasicOption currentSubOption = (BasicOption) subOption;
                    if (!currentSubOption.optionValues.isEmpty()) {
                        ObjectNode subOptionNode = mapper.valueToTree(currentSubOption);
                        subOptionNode.put("optionID", option.getUrlParam() + "=" + currentSubOption.getUrlParam());
                        subOptionNode.remove("displayName");
                        if (!currentOption.getConditionalDisplayName().isEmpty())
                            subOptionNode.put("displayName", currentOption.getConditionalDisplayName());
                        ArrayNode subDefaultSelected = mapper.createArrayNode();
                        subDefaultSelected.add(currentOption.getConditionalParam() + "=" + currentSubOption.optionValues.get(0).getUrlValue());
                        subOptionNode.set("defaultSelected", subDefaultSelected);
                        subOptionNode.put("urlParameter", currentOption.getConditionalParam());
                        subOptionNode.set("tierGroups", mapper.createArrayNode().add(mapper.createObjectNode().set("valueGroups", mapper.createArrayNode().add(mapper.createObjectNode().set("optionValues", mapper.valueToTree(currentSubOption.getOptionValues()))))));
                        yOptions.add(subOptionNode);
                    }
                    consequentOptions.add(currentOption.getUrlParam() + "=" + currentSubOption.getUrlParam());
                    ObjectNode subOptionInfo = mapper.createObjectNode();
                    subOptionInfo.put("displayName", currentSubOption.getDisplayName());
                    subOptionInfo.put("urlValue", currentSubOption.getUrlParam());
                    subOptions.add(subOptionInfo);
                }
                defaultSelected.add(consequentOptions.path(0));
                optionNode.set("defaultSelected", defaultSelected);
                optionNode.set("tierGroups", mapper.createArrayNode().add(mapper.createObjectNode().set("valueGroups", mapper.createArrayNode().add(mapper.createObjectNode().set("optionValues", subOptions)))));
                optionNode.set("consequentOptions", consequentOptions);

            } else if (option.getType().equals("basic")) {
                BasicOption currentOption = (BasicOption) option;
                optionNode.set("defaultSelected", mapper.createArrayNode().add(currentOption.getUrlParam() + "=" + currentOption.getOptionValues().get(0).getUrlValue()));
                optionNode.set("tierGroups", mapper.createArrayNode().add(mapper.createObjectNode().set("valueGroups", mapper.createArrayNode().add(mapper.createObjectNode().set("optionValues", mapper.valueToTree(currentOption.getOptionValues()))))));
            }
        }
        yAxisGroup.put("displayName", displayNames.get("yaxis"));
        yAxisGroup.set("options", yOptions);
        groups.add(yAxisGroup);

        ObjectNode xAxisGroup = mapper.createObjectNode();
        ArrayNode xOptions = mapper.createArrayNode();
        for (String optionName : xAxisOptions) {
            Option option = options.get(optionName);
            ObjectNode optionNode = mapper.valueToTree(option);
            optionNode.put("optionID", option.getUrlParam());
            xOptions.add(optionNode);

            if (option.getType().equals("conditional")) {
                ConditionalOption currentOption = (ConditionalOption) option;

                ArrayNode subOptions = mapper.createArrayNode();
                ArrayNode defaultSelected = mapper.createArrayNode();
                ArrayNode consequentOptions = mapper.createArrayNode();
                for (Option subOption : currentOption.getOptions().values()) {
                    BasicOption currentSubOption = (BasicOption) subOption;
                    if (!currentSubOption.optionValues.isEmpty()) {
                        ObjectNode subOptionNode = mapper.valueToTree(currentSubOption);
                        subOptionNode.put("optionID", option.getUrlParam() + "=" + currentSubOption.getUrlParam());
                        subOptionNode.remove("displayName");
                        if (!currentOption.getConditionalDisplayName().isEmpty())
                            subOptionNode.put("displayName", currentOption.getConditionalDisplayName());
                        subOptionNode.put("urlParameter", currentOption.getConditionalParam());

                        ArrayNode tierGroups = mapper.createArrayNode();
                        ArrayNode valueGroups = mapper.createArrayNode();
                        ArrayNode optionValues = mapper.valueToTree(currentSubOption.getOptionValues());
                        if (currentSubOption.getUrlParam().equals("year")) {
                            tierGroups.add(mapper.createObjectNode().set("valueGroups", valueGroups.add(mapper.createObjectNode().set("optionValues", optionValues))));
                        } else {
                            subOptionNode.put("urlConcatenator", "-");
                            valueGroups.add(mapper.createObjectNode().set("optionValues", optionValues));
                            Option yearOption = currentOption.getOption("year");
                            tierGroups = mapper.valueToTree(((BasicOption) yearOption).getOptionValues());

                            if (currentSubOption.getUrlParam().equals("week")) {
                                Option termOption = currentOption.getOption("term");
                                valueGroups = mapper.valueToTree(((BasicOption) termOption).getOptionValues());

                                for (JsonNode valueGroupInfo : valueGroups) {
                                    ArrayNode newOptionVal = mapper.createArrayNode();
                                    for (int i = 0; i < dateInfo.getWeeksOfTerm(valueGroupInfo.path("urlValue").asInt()); i++) {
                                        newOptionVal.add(optionValues.path(i));
                                    }
                                    ((ObjectNode) valueGroupInfo).set("optionValues", newOptionVal);
                                }
                            }

                            for (JsonNode tierGroupOption : tierGroups) {
                                ObjectNode tierGroup = (ObjectNode) tierGroupOption;
                                tierGroup.set("valueGroups", valueGroups);
                            }
                            subOptionNode.set("presetGroup", getPresetsInfo(currentSubOption.getUrlParam()));
                        }
                        subOptionNode.set("tierGroups", tierGroups);
                        xOptions.add(subOptionNode);
                    }
                    consequentOptions.add(currentOption.getUrlParam() + "=" + currentSubOption.getUrlParam());
                    ObjectNode subOptionInfo = mapper.createObjectNode();
                    subOptionInfo.put("displayName", currentSubOption.getDisplayName());
                    subOptionInfo.put("urlValue", currentSubOption.getUrlParam());
                    subOptions.add(subOptionInfo);
                }
                defaultSelected.add(consequentOptions.path(0));
                optionNode.set("defaultSelected", defaultSelected);
                optionNode.set("consequentOptions", consequentOptions);
                optionNode.set("tierGroups", mapper.createArrayNode().add(mapper.createObjectNode().set("valueGroups", mapper.createArrayNode().add(mapper.createObjectNode().set("optionValues", subOptions)))));
            }
        }
        xAxisGroup.put("displayName", displayNames.get("xaxis"));
        xAxisGroup.set("options", xOptions);
        groups.add(xAxisGroup);
        reportInfo.set("groups", groups);
        return reportInfo;
    }

    @Override
    public ObjectNode getInfo(String field, String... params) {

        switch (field) {
            case "options":
                return getOptionsInfo();
            case "years":
                return getYearsInfo();
            case "stat":
                return getStatInfo(params[0]);
            case "presets":
                return getPresetsInfo(params[0]);
        }
        return super.getInfo(field);
    }

}
