package edu.byu.hbll.reporter.queryrunner.model;

import java.util.List;
import java.util.Map;

public class Substat {
	private String name;
	private String filter;
	private List<String> collections;
	private Map<String, String> functions;
	private String replaceCountWith;
	private boolean percent;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFilter() {
		return filter;
	}

	public void setFilter(String filter) {
		this.filter = filter;
	}

	public List<String> getCollections() {
		return collections;
	}

	public void setCollections(List<String> collections) {
		this.collections = collections;
	}

	public boolean containsCollection(String searchCollection) {
		if (collections == null) {
			return true;
		}
		for (String coll : collections) {
			if (coll.equals(searchCollection)) {
				return true;
			}
		}
		return false;
	}

	public boolean isPercent() {
		return percent;
	}

	public void setPercent(boolean percent) {
		this.percent = percent;
	}

	public Map<String, String> getFunctions() {
		return functions;
	}

	public void setFunctions(Map<String, String> functions) {
		this.functions = functions;
	}

	public String getReplaceCountWith() {
		return replaceCountWith;
	}

	public void setReplaceCountWith(String replaceCountWith) {
		this.replaceCountWith = replaceCountWith;
	}

}
