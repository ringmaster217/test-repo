package edu.byu.hbll.reporter.queryrunner.model;

import java.util.List;
import java.util.Map;

public class PrimaryStat {
	private String name;
	private boolean mutuallyExclusive;
	private String filter;
	private Map<String, String> functions;
	private List<String> collections;
	private List<Substat> substats;
	private String replaceCountWith;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isMutuallyExclusive() {
		return mutuallyExclusive;
	}

	public void setMutuallyExclusive(boolean mutuallyExclusive) {
		this.mutuallyExclusive = mutuallyExclusive;
	}

	public String getFilter() {
		return filter;
	}

	public void setFilter(String filter) {
		this.filter = filter;
	}

	public Map<String, String> getFunctions() {
		return functions;
	}

	public void setFunctions(Map<String, String> functions) {
		this.functions = functions;
	}

	public List<String> getCollections() {
		return collections;
	}

	public void setCollections(List<String> collections) {
		this.collections = collections;
	}

	public List<Substat> getSubstats() {
		return substats;
	}

	public void setSubstats(List<Substat> substats) {
		this.substats = substats;
	}

	public String getReplaceCountWith() {
		return replaceCountWith;
	}

	public void setReplaceCountWith(String replaceCountWith) {
		this.replaceCountWith = replaceCountWith;
	}

	public Substat getSubstat(String substatName) {
		for (Substat substat : substats) {
			if (substat.getName().equals(substatName)) {
				return substat;
			}
		}
		return null;
	}

	public boolean containsCollection(String searchCollection) {
		if (collections == null) {
			return true;
		}
		for (String coll : collections) {
			if (coll.equals(searchCollection)) {
				return true;
			}
		}
		return false;
	}
}
