package edu.byu.hbll.reporter.queryrunner;

import java.io.IOException;
import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.temporal.TemporalField;
import java.time.temporal.WeekFields;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import edu.byu.hbll.reporter.queryrunner.model.CustomTimeGroup;

public class TableTimeFacetCalculator {
	private static final Logger logger = LoggerFactory.getLogger(TableTimeFacetCalculator.class);
	private static final ObjectMapper mapper = new ObjectMapper();
	public static final String CONFIG_NODE = "tableTimeFacets";

	private static final String YEAR_FIELD = "YEAR";
	private static final String MONTH_FIELD = "MONTH";
	private static final String DAY_FIELD = "DAY";
	private static final String HOUR_FIELD = "HOUR";

	private static List<String> rowGroupNames;
	private static DayOfWeek weekBeginsOn;
	private static int numRowsPerGroup;
	private static String timezone;
	private static List<CustomTimeGroup> termDates;
	private static int academicYearBeginsOn;

	// -- CONFIGURATION --

	public static void initialize(JsonNode timeFacetsConfigNode) {
		logger.info("Loading time facets config");
		try {
			List<String> timeGroups = mapper.readValue(timeFacetsConfigNode.path("timeGroups").toString(),
					new TypeReference<List<String>>() {
					});
			rowGroupNames = Collections.unmodifiableList(timeGroups);
			weekBeginsOn = DayOfWeek.valueOf(timeFacetsConfigNode.path("weekBeginsOn").asText());
			numRowsPerGroup = timeFacetsConfigNode.path("numRowsPerGroup").asInt();
			// FIXME: add rowBeginsOnGroupMark
			timezone = timeFacetsConfigNode.path("timezone").asText();
			List<CustomTimeGroup> termGroups = mapper.readValue(timeFacetsConfigNode.path("termDates").toString(),
					new TypeReference<List<CustomTimeGroup>>() {
					});
			termDates = Collections.unmodifiableList(termGroups);
			academicYearBeginsOn = timeFacetsConfigNode.path("academicYearBegins").asInt();
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException("Error parsing JSON from config");
		}

		logger.info("Successfully loaded time facets config");
	}

	// -- PUBLIC METHODS --

	public static Map<String, List<String>> getAll() {
		return getAll(LocalDateTime.now());
	}

	public static Map<String, List<String>> getAll(LocalDateTime today) {
		Map<String, List<String>> returnMap = new TreeMap<>(new ListOrderComparator(getRowNames()));
		int currentAcademicYear = getCurrentAcademicYear(today);
		int currentTerm = getCurrentTerm(today);

		returnMap.putAll(getYearFilters(currentAcademicYear));
		returnMap.putAll(getTermFilters(currentAcademicYear, currentTerm));
		returnMap.putAll(getWeekFilters(today));
		returnMap.putAll(getDayFilters(today));
		returnMap.putAll(getHourFilters(today));
		return returnMap;
	}

	public static List<String> getRowGroupNames() {
		// the reference is to a Collection.unmodifiableList (ie: it's immutable)
		return rowGroupNames;
	}

	public static List<String> getRowNames() {
		List<String> returnList = new ArrayList<>();
		for (String rowGroupName : rowGroupNames) {
			for (int i = 0; i < numRowsPerGroup; i++) {
				returnList.add(rowGroupName + "-" + i);
			}
		}
		return returnList;
	}

	public static String getTimeZone() {
		return timezone;
	}

	// -- HELPER METHODS --

	private static int getCurrentAcademicYear(LocalDateTime today) {
		int month = today.getMonthValue();
		if (month >= academicYearBeginsOn) {
			return today.getYear() + 1;
		} else {
			return today.getYear();
		}
	}

	private static int getCurrentTerm(LocalDateTime today) {
		int month = today.getMonthValue();
		for (int i = 1; i <= termDates.size(); i++) {
			if (termDates.get(i - 1).contains(month)) {
				return i;
			}
		}
		return 1;
	}

	private static Map<String, List<String>> getYearFilters(int currentAcademicYear) {
		Map<String, List<String>> returnMap = new HashMap<>();
		for (int i = 0; i < numRowsPerGroup; i++) {
			returnMap.put("year-" + i, Arrays.asList("_academicYear:" + currentAcademicYear));
			currentAcademicYear--;
		}
		return returnMap;
	}

	private static Map<String, List<String>> getTermFilters(int currentAcademicYear, int currentTerm) {
		Map<String, List<String>> returnMap = new HashMap<>();
		for (int i = 0; i < numRowsPerGroup; i++) {
			List<String> filters = new ArrayList<>();
			filters.add("_academicYear:" + currentAcademicYear);
			filters.add("_term:" + currentTerm);
			returnMap.put("term-" + i, filters);
			currentTerm--;
			if (currentTerm <= 0) {
				currentTerm = termDates.size();
				currentAcademicYear--;
			}
		}
		return returnMap;
	}

	private static Map<String, List<String>> getWeekFilters(LocalDateTime today) {
		Map<String, List<String>> returnMap = new HashMap<>();

		TemporalField dow = WeekFields.of(weekBeginsOn, 1).dayOfWeek();
		int beginRange = today.get(dow) - 1; // subtract one because we're rounding to the beginning of that day
		int endRange = -1;
		String asOfDate = getAsOfDate(today, false);

		for (int i = 0; i < numRowsPerGroup; i++) {
			StringBuilder sb = new StringBuilder();
			sb.append("_timestamp:[").append(asOfDate);
			sb.append(getAdjustedDateValue(beginRange, DAY_FIELD, true));
			sb.append(" TO ").append(asOfDate);
			sb.append(getAdjustedDateValue(endRange, DAY_FIELD, true));
			sb.append("]");
			returnMap.put("week-" + i, Arrays.asList(sb.toString()));
			endRange = beginRange;
			beginRange += 7;
		}
		return returnMap;
	}

	private static Map<String, List<String>> getDayFilters(LocalDateTime today) {
		Map<String, List<String>> returnMap = new HashMap<>();

		String asOfDate = getAsOfDate(today, false);

		for (int i = 0; i < numRowsPerGroup; i++) {
			StringBuilder sb = new StringBuilder();
			sb.append("_timestamp:[").append(asOfDate);
			sb.append(getAdjustedDateValue(i, DAY_FIELD, true));
			sb.append(" TO ").append(asOfDate);
			sb.append(getAdjustedDateValue(i - 1, DAY_FIELD, true));
			sb.append("]");
			returnMap.put("day-" + i, Arrays.asList(sb.toString()));
		}
		return returnMap;
	}

	private static Map<String, List<String>> getHourFilters(LocalDateTime today) {
		Map<String, List<String>> returnMap = new HashMap<>();

		String asOfDate = getAsOfDate(today, true);

		for (int i = 0; i < numRowsPerGroup; i++) {
			StringBuilder sb = new StringBuilder();
			sb.append("_timestamp:[").append(asOfDate);
			sb.append(getAdjustedDateValue(i, HOUR_FIELD, true));
			sb.append(" TO ").append(asOfDate);
			sb.append(getAdjustedDateValue(i - 1, HOUR_FIELD, true));
			sb.append("]");
			returnMap.put("hour-" + i, Arrays.asList(sb.toString()));
		}
		return returnMap;
	}

	private static String getAsOfDate(LocalDateTime today, boolean checkHour) {
		LocalDateTime now = LocalDateTime.now();
		if ((today.getMinute() - now.getMinute()) <= 1) { // simplifies the text in the request
			return "NOW";
		} else {
			StringBuilder sb = new StringBuilder();
			sb.append("NOW");

			int adjustYear = now.getYear() - today.getYear();
			sb.append(getAdjustedDateValue(adjustYear, YEAR_FIELD, false));

			int adjustMonth = now.getMonthValue() - today.getMonthValue();
			sb.append(getAdjustedDateValue(adjustMonth, MONTH_FIELD, false));

			int adjustDay = now.getDayOfMonth() - today.getDayOfMonth();
			sb.append(getAdjustedDateValue(adjustDay, DAY_FIELD, false));

			if (checkHour) {
				int adjustHour = now.getHour() - today.getHour();
				sb.append(getAdjustedDateValue(adjustHour, HOUR_FIELD, false));
			}
			return sb.toString();
		}
	}

	private static String getAdjustedDateValue(int value, String field, boolean round) {
		StringBuilder sb = new StringBuilder();
		if (value > 0) {
			sb.append("-").append(value).append(field).append("S");
		} else if (value < 0) {
			sb.append("+").append(Math.abs(value)).append(field).append("S");
		}
		if (round) {
			sb.append("/").append(field);
		}
		return sb.toString();
	}
}
