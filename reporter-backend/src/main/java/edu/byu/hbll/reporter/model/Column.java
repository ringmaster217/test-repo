package edu.byu.hbll.reporter.model;

import java.util.*;

public class Column {

    String colName;
    String displayName;
    boolean hasSubCols;
    Map<String, String> displayNames = new LinkedHashMap<>();

    public Column(String colName, String displayName, boolean hasSubCols) {
        this.colName = colName;
        this.displayName = displayName;
        this.hasSubCols = hasSubCols;
    }

    public String getColName() {
        return colName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public boolean getHasSubCols() {
        return hasSubCols;
    }

    public void addSubDisplayNames(String subName, String subDisplayName) {
        displayNames.put(subName, subDisplayName);
    }

    public Map<String, String> getDisplayNames() {
        return displayNames;
    }

}
