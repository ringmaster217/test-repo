package edu.byu.hbll.reporter.queryrunner;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.DoubleNode;
import com.fasterxml.jackson.databind.node.IntNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import edu.byu.hbll.json.JsonField;
import edu.byu.hbll.reporter.queryrunner.model.DataTable;

public class TableSolrResponseParser {
	private static Logger logger = LoggerFactory.getLogger(TableSolrResponseParser.class);
	private static ObjectMapper mapper = new ObjectMapper();

	public static ObjectNode parseSolrResponse(Map<String, JsonNode> solrResJson) {
		logger.info("Parsing results...");

		logger.info("Creating empty table frame...");
		DataTable dataTable = new DataTable();

		logger.info("Populating table from Solr response...");
		populateTable(dataTable, solrResJson);

		logger.info("Returning successfully parsed result");
		return mapper.valueToTree(dataTable);
	}

	private static void populateTable(DataTable dataTable, Map<String, JsonNode> solrResJson) {
		for (JsonNode collectionNode : solrResJson.values()) {
			for (JsonField timeNode : new JsonField(collectionNode.path("facets"))) {
				if (!(timeNode.getValue() instanceof ObjectNode)) {
					continue; // skips the "count" field
				}
				for (JsonField primaryStatNode : timeNode) {
					if (!(primaryStatNode.getValue() instanceof ObjectNode)) {
						continue; // skips the "count" field on the timeNode
					}
					addNodeValuesToTable(dataTable, primaryStatNode, timeNode.getKey(), primaryStatNode.getKey(), null);
					for (JsonField substatNode : primaryStatNode) {
						if (!(substatNode.getValue() instanceof ObjectNode)) {
							continue; // skips the "count" and other function fields on the primaryStatNode
						}
						addNodeValuesToTable(dataTable, substatNode, timeNode.getKey(), primaryStatNode.getKey(),
								substatNode.getKey());
					}
				}
			}
		}
	}

	private static void addNodeValuesToTable(DataTable dataTable, JsonField currentNode, String rowName,
			String primaryStatName, String substatName) {
		// for all the primary stats and substats, look at just the numeric values on
		// their ObjectNodes and see which ones we need
		for (JsonField numericNode : currentNode) {
			if (!(numericNode.getValue() instanceof ObjectNode)) {
				// numericNode's value is either related to count or some calculated function
				String nodeKey = numericNode.getKey();
				Object nodeValue = numericNode.getValue();
				if (nodeValue instanceof IntNode) {
					nodeValue = ((IntNode) nodeValue).asInt();
				} else if (nodeValue instanceof DoubleNode) {
					nodeValue = ((DoubleNode) nodeValue).asDouble();
				}
				String replaceCountWith = null;
				String columnName = substatName;
				// String functionColumnName = substatName;
				if (substatName == null) {
					replaceCountWith = StatFilterManager.getPrimaryStatReplaceCountWith(primaryStatName);
					columnName = StatFilterManager.TOTAL_NAME;
					// functionColumnName = primaryStatName;
				} else {
					replaceCountWith = StatFilterManager.getSubstatReplaceCountWith(primaryStatName, substatName);
				}
				String functionColumnName = primaryStatName + "-" + columnName;

				if (replaceCountWith == null) {
					if (nodeKey.equals(StatFilterManager.COUNT_NAME)) {
						dataTable.putValue(rowName, primaryStatName, columnName, nodeValue);
					} else {
						dataTable.putValue(rowName, nodeKey, functionColumnName, nodeValue);
					}
				} else {
					if (nodeKey.equals(StatFilterManager.COUNT_NAME)) {
						// ignore
						continue;
					} else if (nodeKey.equals(replaceCountWith)) {
						dataTable.putValue(rowName, primaryStatName, columnName, nodeValue);
					} else {
						dataTable.putValue(rowName, nodeKey, functionColumnName, nodeValue);
					}
				}
			}
		}
	}
}
