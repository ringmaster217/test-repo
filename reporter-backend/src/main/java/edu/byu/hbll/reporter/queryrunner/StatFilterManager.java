package edu.byu.hbll.reporter.queryrunner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import edu.byu.hbll.reporter.queryrunner.model.PrimaryStat;
import edu.byu.hbll.reporter.queryrunner.model.Substat;

public class StatFilterManager {
	private static final Logger logger = LoggerFactory.getLogger(StatFilterManager.class);
	// Note: see config for stat filter values
	private static final ObjectMapper mapper = new ObjectMapper();
	public static final String CONFIG_NODE = "statFilters";

	private static List<PrimaryStat> primaryStats;
	public static final String TOTAL_NAME = "total";
	public static final String COUNT_NAME = "count";

	public static void initialize(JsonNode statsConfigNode) {
		logger.info("Loading stats config");
		try {
			primaryStats = mapper.readValue(statsConfigNode.toString(), new TypeReference<List<PrimaryStat>>() {
			});
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException("Error parsing JSON from config");
		}
		logger.info("Successfully loaded stats config");
	}

	public static List<String> getPrimaryStatNames() {
		List<String> primaryStatNames = new ArrayList<>();
		for (PrimaryStat primaryStat : primaryStats) {
			primaryStatNames.add(primaryStat.getName());
		}
		return primaryStatNames;
	}

	public static List<String> getSubstatNames(String primaryStatName) {
		PrimaryStat pStat = getPrimaryStat(primaryStatName);
		List<Substat> substats = pStat.getSubstats();
		List<String> returnList = new ArrayList<>();
		for (Substat stat : substats) {
			returnList.add(stat.getName());
		}
		return returnList;
	}

	public static String getPrimaryStatFilter(String primaryStatName, String curCollection) {
		PrimaryStat pStat = getPrimaryStat(primaryStatName);
		if (!pStat.isMutuallyExclusive()) {
			return pStat.getFilter();
		}
		// The substats are mutually exclusive. We want to create a String so that they
		// will be looked up via the OR operator
		List<String> substatFilters = new ArrayList<>();
		List<Substat> substats = pStat.getSubstats();
		for (Substat substat : substats) {
			// only add if substat.getCollections contains curCollection
			if (substat.containsCollection(curCollection)) {
				String substatFilter = substat.getFilter();
				if (substatFilter != null) {
					substatFilters.add("filter(" + substatFilter + ")");
				}
			}
		}
		if (substatFilters.size() == 0) {
			return null;
		}
		return String.join(" ", substatFilters);
	}

	public static String getSubstatFilter(String primaryStatName, String substatName) {
		if (substatName.equals(TOTAL_NAME)) {
			return "*";
		}
		PrimaryStat pStat = getPrimaryStat(primaryStatName);
		Substat substat = pStat.getSubstat(substatName);
		String sFilter = substat.getFilter();
		// if (sFilter == null) { // FIXME: do we want this?
		// return "*";
		// }
		return sFilter;
	}

	public static List<String> getPrimaryCollections(String primaryStatName) {
		PrimaryStat pStat = getPrimaryStat(primaryStatName);
		return pStat.getCollections();
	}

	public static boolean primaryStatContainsCollection(String primaryStatName, String searchCollection) {
		PrimaryStat pStat = getPrimaryStat(primaryStatName);
		return pStat.containsCollection(searchCollection);
	}

	public static boolean substatContainsCollection(String primaryStatName, String substatName,
			String searchCollection) {
		PrimaryStat pStat = getPrimaryStat(primaryStatName);
		if (substatName.equals(TOTAL_NAME)) {
			return pStat.containsCollection(searchCollection);
		}
		Substat substat = pStat.getSubstat(substatName);
		return substat.containsCollection(searchCollection);
	}

	public static Map<String, List<String>> getPercentSubstats() {
		Map<String, List<String>> returnMap = new HashMap<>();
		for (PrimaryStat pStat : primaryStats) {
			for (Substat substat : pStat.getSubstats()) {
				if (substat.isPercent()) {
					List<String> curSubstatList = returnMap.getOrDefault(pStat.getName(), null);
					if (curSubstatList == null) {
						returnMap.put(pStat.getName(), new ArrayList<>(Arrays.asList(substat.getName())));
					} else {
						curSubstatList.add(substat.getName());
					}
				}
			}
		}
		return returnMap;
	}

	public static Map<String, String> getPrimaryFunctions(String primaryStatName) {
		PrimaryStat pStat = getPrimaryStat(primaryStatName);
		return pStat.getFunctions();
	}

	public static Map<String, String> getSubstatFunctions(String primaryStatName, String substatName) {
		PrimaryStat pStat = getPrimaryStat(primaryStatName);
		if (substatName.equals(TOTAL_NAME)) {
			return pStat.getFunctions();
		}
		Substat substat = pStat.getSubstat(substatName);
		return substat.getFunctions();
	}

	public static String getPrimaryStatReplaceCountWith(String primaryStatName) {
		PrimaryStat pStat = getPrimaryStat(primaryStatName);
		return pStat.getReplaceCountWith();
	}

	public static String getSubstatReplaceCountWith(String primaryStatName, String substatName) {
		PrimaryStat pStat = getPrimaryStat(primaryStatName);
		if (substatName.equals(TOTAL_NAME)) {
			return pStat.getReplaceCountWith();
		}
		Substat substat = pStat.getSubstat(substatName);
		return substat.getReplaceCountWith();
	}

	private static PrimaryStat getPrimaryStat(String primaryStatName) {
		for (PrimaryStat pStat : primaryStats) {
			if (pStat.getName().equals(primaryStatName)) {
				return pStat;
			}
		}
		return null;
	}
}
