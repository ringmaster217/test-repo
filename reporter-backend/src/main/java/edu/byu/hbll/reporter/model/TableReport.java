package edu.byu.hbll.reporter.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import javax.persistence.Basic;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class TableReport extends Report {

    // the size includes if it has subcolumns
    int rowSize = 0;
    int colSize = 0;
    ArrayList<Row> rows = new ArrayList<>();
    ArrayList<Column> cols = new ArrayList<>();
    ArrayList<BasicOption> options = new ArrayList<>();

    public TableReport() {
        this.type = "table";
    }

    public void addRow(Row row) {
        if (row.getHasSubRows()) {
            rowSize += row.getDisplayNames().size();
        } else {
            rowSize++;
        }
        rows.add(row);
    }

    public int getRowSize() {
        return rowSize;
    }

    public void addCol(Column col) {
        if (col.getHasSubCols()) {
            colSize += col.getDisplayNames().size();
        } else {
            colSize++;
        }
        cols.add(col);
    }

    public int getColSize() {
        return colSize;
    }

    public void addOption(BasicOption option) {
        options.add(option);
    }

    public ObjectNode getOptionsInfo() {
        ObjectNode optionsInfo = mapper.createObjectNode();
        optionsInfo.put("displayTitle", "Table Controls");

        ObjectNode optionsGroup = mapper.createObjectNode();
        ArrayNode optionsNode = mapper.createArrayNode();
        for (BasicOption option : options) {
            ObjectNode optionNode = mapper.valueToTree(option);
            optionNode.put("optionID", option.getUrlParam());
            optionNode.set("defaultSelected", mapper.createArrayNode().add(option.getUrlParam() + "=" + getCurrent(option.getUrlParam())));
            optionNode.set("tierGroups", mapper.createArrayNode().add(mapper.createObjectNode().set("valueGroups", mapper.createArrayNode().add(mapper.createObjectNode().set("optionValues", mapper.valueToTree(option.getOptionValues()))))));
            optionsNode.add(optionNode);
        }
        optionsGroup.put("displayName", "Custom time to load table");
        optionsGroup.set("options", optionsNode);

        optionsInfo.set("groups", mapper.createArrayNode().add(optionsGroup));
        return optionsInfo;
    }

    public String getCurrent(String type) {

        LocalDateTime current = LocalDateTime.now();

        switch (type) {
            case "year":
                return String.valueOf(current.getYear());
            case "month":
                return String.valueOf(current.getMonth().getValue());
            case "dom":
                return String.valueOf(current.getDayOfMonth());
            case "hour":
                return String.valueOf(current.getHour());
            case "minute":
                return String.valueOf(current.getMinute());
        }
        return "";
    }

    public ObjectNode getRowsInfo() {
        ObjectNode info = mapper.createObjectNode();
        ArrayNode rowsNode = mapper.createArrayNode();
        for (int i = 0; i < rows.size(); i++) {
            ObjectNode rowNode = mapper.createObjectNode();
            rowNode.put(rows.get(i).getRowName(), rows.get(i).getDisplayName());
            rowNode.put("subRowsNum", rows.get(i).getDisplayNames().size());
            rowNode.set("subRowNames", mapper.valueToTree(rows.get(i).getDisplayNames()));
            rowsNode.add(rowNode);
        }
        info.set("rowNames", rowsNode);
        return info;
    }

    public ObjectNode getColsInfo() {
        ObjectNode info = mapper.createObjectNode();
        ArrayNode colsNode = mapper.createArrayNode();
        for (int i = 0; i < cols.size(); i++) {
            ObjectNode colNode = mapper.createObjectNode();
            colNode.put(cols.get(i).getColName(), cols.get(i).getDisplayName());
            colNode.put("subColsNum", cols.get(i).getDisplayNames().size());
            colNode.set("subColNames", mapper.valueToTree(cols.get(i).getDisplayNames()));
            colsNode.add(colNode);
        }
        info.set("colNames", colsNode);
        return info;
    }

    @Override
    public ObjectNode getInfo(String field, String... params) {

        switch (field) {
            case "rows":
                return getRowsInfo();
            case "cols":
                return getColsInfo();
            case "options":
                return getOptionsInfo();
        }
        return super.getInfo(field);
    }

}
