package edu.byu.hbll.reporter.api;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * Application class.
 */
@ApplicationPath("api")
public class MyApplication extends Application {

}
