package edu.byu.hbll.reporter.queryrunner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import edu.byu.hbll.json.JsonField;
import edu.byu.hbll.reporter.queryrunner.model.GraphLabel;
import edu.byu.hbll.reporter.queryrunner.model.GraphLine;
import edu.byu.hbll.reporter.queryrunner.model.QueryModel;
import edu.byu.hbll.reporter.queryrunner.model.QueryModel.Timeframe;
import edu.byu.hbll.reporter.queryrunner.model.Term;
import edu.byu.hbll.reporter.queryrunner.model.Week;

public class GraphSolrResponseParser {

	private static Logger logger = LoggerFactory.getLogger(GraphSolrResponseParser.class);
	private static ObjectMapper mapper = new ObjectMapper();

	public static ObjectNode parseSolrResponse(Map<String, JsonNode> solrResJson, QueryModel queryRequest) {
		logger.info("Parsing results...");
		Timeframe timeframe = queryRequest.getTimeframe();

		logger.info("Generating graph labels...");
		// merge sets of labels as we go through each collection's result
		Set<GraphLabel> labels = new TreeSet<>();
		for (JsonNode resNode : solrResJson.values()) {
			labels.addAll(populateLabels(resNode, timeframe));
		}

		logger.info("Creating graph lines...");
		Map<String, GraphLine> graphsMap = null;
		for (JsonNode resNode : solrResJson.values()) {
			Map<String, GraphLine> newGraphLines = constructGraphs(resNode, labels, timeframe, queryRequest.getStat());
			if (graphsMap == null) {
				graphsMap = newGraphLines;
			} else {
				mergeGraphLines(graphsMap, newGraphLines);
			}
		}

		logger.info("Checking for missing graph lines...");
		Set<GraphLine> graphs = new TreeSet<>(graphsMap.values());
		addMissingGraphLines(graphs, labels, queryRequest);

		ObjectNode resJson = mapper.createObjectNode();
		resJson.put("LinesNumber", graphs.size());
		resJson.set("Labels", mapper.valueToTree(convertLabelsToString(labels)));
		resJson.set("GraphLines", mapper.valueToTree(graphs));

		return resJson;
	}

	private static Set<GraphLabel> populateLabels(JsonNode solrJsonResponse, Timeframe timeframe) {
		Set<GraphLabel> graphLineLabels = new TreeSet<>();
		if (timeframe == Timeframe.WEEK) {
			// we can assume we'll always want all 7 days in a week
			GraphLabel curLabel = new GraphLabel(timeframe);
			curLabel.setDay(1);
			for (int i = 0; i < 7; i++) {
				graphLineLabels.add(curLabel);
				curLabel = curLabel.incrLabel();
			}
			return graphLineLabels;
		}

		final JsonNode solrResSubstats = solrJsonResponse.path("facets");
		for (JsonNode curNodeSubstat : solrResSubstats) {
			final JsonNode solrResYears = curNodeSubstat.path("year").path("buckets");
			for (JsonNode curNodeYear : solrResYears) {
				final JsonNode solrResTerms = curNodeYear.path("term").path("buckets");
				for (JsonNode curNodeTerm : solrResTerms) {
					final JsonNode solrResWeeks = curNodeTerm.path("week").path("buckets");
					for (JsonNode curNodeWeek : solrResWeeks) {
						GraphLabel curLabel = new GraphLabel(timeframe);
						if (timeframe == Timeframe.YEAR) {
							curLabel.setTerm(curNodeTerm.path("val").asInt());
						}
						curLabel.setWeek(curNodeWeek.path("val").asInt());
						if (curLabel.getWeek() > 0) { // filter out bad data
							graphLineLabels.add(curLabel);
						}
					}
				}
			}
		}

		// Create a copy so we can go through each label in the list as we're adding
		// more
		Set<GraphLabel> graphLineLabelsCopy = new HashSet<>();
		graphLineLabelsCopy.addAll(graphLineLabels);

		// Now fill in any holes, so we have a continuous line
		for (GraphLabel checkedLabel : graphLineLabelsCopy) {
			GraphLabel curLabel = checkedLabel.decrLabel();
			while (!graphLineLabels.contains(curLabel) && curLabel.getWeek() > 0) {
				graphLineLabels.add(curLabel);
				curLabel = curLabel.decrLabel();
			}
		}

		return graphLineLabels;
	}

	private static Map<String, GraphLine> constructGraphs(JsonNode solrJsonResponse, Set<GraphLabel> labels,
			Timeframe timeframe, String primaryStatName) {
		// First get a map for where to place counts in the graph arrays
		Map<GraphLabel, Integer> labelMap = convertLabelsToIndexMap(labels);

		// Now match up values in our results to labels
		Map<String, GraphLine> graphLines = new HashMap<>();
		int numValues = labels.size();
		GraphLine currentLine = null;
		GraphLabel currentLabel = new GraphLabel(timeframe);

		final JsonNode solrResSubstats = solrJsonResponse.path("facets");
		for (JsonField substatNode : new JsonField(solrResSubstats)) {
			if (!substatNode.getValue().isObject()) {
				continue; // skips the "count" field
			}
			final JsonNode yearArray = substatNode.getValue().path("year").path("buckets");
			for (JsonNode yearNode : yearArray) {
				int curYear = yearNode.path("val").asInt();
				if (timeframe == Timeframe.YEAR) {
					currentLine = new GraphLine(numValues);
					currentLine.setLineName(substatNode.getKey(), curYear);
				}

				final JsonNode termArray = yearNode.path("term").path("buckets");
				for (JsonNode termNode : termArray) {
					int curTerm = termNode.path("val").asInt();
					if (timeframe == Timeframe.YEAR) {
						currentLabel.setTerm(curTerm);
					} else if (timeframe == Timeframe.TERM) {
						currentLine = new GraphLine(numValues);
						currentLine.setLineName(substatNode.getKey(), curYear, curTerm);
					}

					final JsonNode weekArray = termNode.path("week").path("buckets");
					for (JsonNode weekNode : weekArray) {
						int curWeek = weekNode.path("val").asInt();
						if (timeframe == Timeframe.WEEK) {
							currentLine = new GraphLine(numValues);
							currentLine.setLineName(substatNode.getKey(), curYear, curTerm, curWeek);

							final JsonNode dayArray = weekNode.path("day").path("buckets");
							for (JsonNode dayNode : dayArray) {
								currentLabel.setDay(dayNode.path("val").asInt());
								int index = labelMap.getOrDefault(currentLabel, -1);
								if (index >= 0) { // filter out bad data
									currentLine.setValue(getFieldValue(dayNode, primaryStatName, substatNode.getKey()),
											index);
								}
							}
							currentLine.fillEmptyValues();
							graphLines.put(currentLine.getLineName(), currentLine);

						} else {
							// figure out where in the array this value needs to go
							currentLabel.setWeek(curWeek);
							int index = labelMap.getOrDefault(currentLabel, -1);
							if (index >= 0) { // filter out bad data
								currentLine.setValue(getFieldValue(weekNode, primaryStatName, substatNode.getKey()),
										index);
							}
						}
					}

					if (timeframe == Timeframe.TERM) {
						currentLine.fillEmptyValues();
						graphLines.put(currentLine.getLineName(), currentLine);
					}
				}

				if (timeframe == Timeframe.YEAR) {
					currentLine.fillEmptyValues();
					graphLines.put(currentLine.getLineName(), currentLine);
				}
			}
		}

		return graphLines;
	}

	private static void mergeGraphLines(Map<String, GraphLine> graphs, Map<String, GraphLine> newGraphLines) {
		// adding new graph lines to the map of graph lines; if it's already there,
		// merge the values of the lines
		for (GraphLine gl : newGraphLines.values()) {
			String curLineName = gl.getLineName();
			GraphLine matchingLine = graphs.getOrDefault(curLineName, null);
			if (matchingLine == null) {
				graphs.put(curLineName, gl);
			} else {
				// merge the data
				matchingLine.mergeGraphLine(gl);
			}
		}
	}

	private static void addMissingGraphLines(Set<GraphLine> graphLines, Set<GraphLabel> graphLabels,
			QueryModel queryRequest) {
		// compare the list of graph lines we got from parsing the Solr response against
		// what the client is expecting back; it's possible there are some missing. In
		// that case, just send empty lines
		int size = graphLabels.size();
		Timeframe timeframe = queryRequest.getTimeframe();
		Set<GraphLine> expectedGraphLines = new TreeSet<>();
		List<String> substats = queryRequest.getSubStats();
		if (substats.size() == 0) {
			substats.add(StatFilterManager.TOTAL_NAME);
		}
		for (String substat : substats) {
			if (timeframe == Timeframe.YEAR) {
				List<String> years = queryRequest.getYears();
				for (String year : years) {
					GraphLine gl = new GraphLine(size);
					gl.setLineName(substat, Integer.parseInt(year));
					expectedGraphLines.add(gl);
				}
			} else if (timeframe == Timeframe.TERM) {
				List<Term> terms = queryRequest.getTerms();
				for (Term term : terms) {
					GraphLine gl = new GraphLine(0);
					gl.setLineName(substat, Integer.parseInt(term.getBelongsToYear()),
							Integer.parseInt(term.getTerm()));
					expectedGraphLines.add(gl);
				}
			} else if (timeframe == Timeframe.WEEK) {
				List<Week> weeks = queryRequest.getWeeks();
				for (Week week : weeks) {
					GraphLine gl = new GraphLine(0);
					gl.setLineName(substat, Integer.parseInt(week.getBelongsToYear()),
							Integer.parseInt(week.getBelongsToTerm()), Integer.parseInt(week.getWeek()));
					expectedGraphLines.add(gl);
				}
			}
		}

		// now add the missing nodes
		for (GraphLine gl : expectedGraphLines) {
			if (!graphLines.contains(gl)) { // TreeSet.contains() calls compareTo() method instead of equals()
				gl.fillEmptyValues();
				graphLines.add(gl);
			}
		}
	}

	private static List<String> convertLabelsToString(Set<GraphLabel> graphLabels) {
		List<String> retArray = new ArrayList<>();
		for (GraphLabel label : graphLabels) {
			retArray.add(label.toString());
		}
		return retArray;
	}

	private static Map<GraphLabel, Integer> convertLabelsToIndexMap(Set<GraphLabel> graphLabels) {
		Map<GraphLabel, Integer> labelMap = new HashMap<>();
		int i = 0;
		for (GraphLabel label : graphLabels) {
			labelMap.put(label, i);
			i++;
		}
		return labelMap;
	}

	private static int getFieldValue(JsonNode node, String primaryStatName, String substatName) {
		String substatReplaceCountWith = StatFilterManager.getSubstatReplaceCountWith(primaryStatName, substatName);
		if (substatReplaceCountWith == null) {
			return node.path(StatFilterManager.COUNT_NAME).asInt();
		}
		return node.path(substatReplaceCountWith).asInt();
	}
}
