package edu.byu.hbll.reporter.queryrunner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import edu.byu.hbll.reporter.queryrunner.model.GraphTimeFilterLoader;
import edu.byu.hbll.reporter.queryrunner.model.GraphTimeFilterLoader.QueryTerm;
import edu.byu.hbll.reporter.queryrunner.model.GraphTimeFilterLoader.QueryYear;
import edu.byu.hbll.reporter.queryrunner.model.QueryModel;
import edu.byu.hbll.reporter.queryrunner.model.QueryModel.Timeframe;

//@Stateless
public class GraphSolrQueryLoader {
	private static final Logger logger = LoggerFactory.getLogger(GraphSolrQueryLoader.class);
	private static ObjectMapper mapper = new ObjectMapper();

	private GraphSolrQueryLoader() {
	}

	/**
	 * Primary function call for creating graph queries.
	 * 
	 * @param queryModel
	 * @return HashMap<String, JsonNode> where the key is the collection to access
	 *         and the value is the constructed Json query
	 */
	public static Map<String, JsonNode> createQuery(QueryModel queryModel, boolean debug) {
		logger.info("Constructing new query request...");
		Map<String, JsonNode> returnMap = new HashMap<>();
		String primaryStat = queryModel.getStat();

		List<String> collections = StatFilterManager.getPrimaryCollections(primaryStat);
		for (String collection : collections) {
			ObjectNode jsonQuery;
			if (debug) {
				jsonQuery = (ObjectNode) SolrQueryTemplateManager.getBaseDebug();
			} else {
				jsonQuery = (ObjectNode) SolrQueryTemplateManager.getBase();
			}
			ObjectNode paramsNode = (ObjectNode) jsonQuery.path("params");

			String queryFilter = loadTimeframeFilters(queryModel);
			ArrayNode filtersNode = paramsNode.putArray("fq").add(queryFilter);
			String primaryStatFilter = StatFilterManager.getPrimaryStatFilter(primaryStat, collection);
			if (primaryStatFilter != null) {
				filtersNode.add(primaryStatFilter);
			}
			logger.info("Successfully loaded query (and primary stat) filters");

			jsonQuery.set("facet", loadSubstatFilters(queryModel, collection));
			logger.info("Successfully loaded facet templates and substat filters");

			returnMap.put(collection, jsonQuery);
		}

		return returnMap;
	}

	private static String loadTimeframeFilters(QueryModel queryModel) {
		// FIXME: we'll want to mess with this to optimize filters
		Timeframe timeframe = queryModel.getTimeframe();
		StringBuilder sb = new StringBuilder();
		if (timeframe == Timeframe.YEAR) {
			List<String> years = queryModel.getYears();
			sb.append("_academicYear:(");
			List<String> yearList = new ArrayList<>();
			for (String year : years) {
				yearList.add("filter(" + year + ")");
			}
			Collections.sort(yearList);
			sb.append(String.join(" ", yearList)).append(")");
		} else if (timeframe == Timeframe.TERM) {
			GraphTimeFilterLoader dateRanges = new GraphTimeFilterLoader(queryModel);
			Set<QueryYear> years = dateRanges.getAllYears();
			for (QueryYear year : years) {
				sb.append("filter(_academicYear:").append(year.getYear()).append(" AND ").append("_term:(");
				Set<QueryTerm> terms = year.getAllTerms();
				List<String> termList = new ArrayList<>();
				for (QueryTerm term : terms) {
					termList.add(term.getTerm());
				}
				Collections.sort(termList); // Always needs to be in the same order in order to engage the filterCache
				sb.append(String.join(" ", termList)).append(")) ");
			}
		} else if (timeframe == Timeframe.WEEK) {
			GraphTimeFilterLoader dateRanges = new GraphTimeFilterLoader(queryModel);
			Set<QueryYear> years = dateRanges.getAllYears();
			for (QueryYear year : years) {
				Set<QueryTerm> terms = year.getAllTerms();
				for (QueryTerm term : terms) {
					sb.append("filter(_academicYear:").append(year.getYear()).append(" AND ").append("_term:")
							.append(term.getTerm()).append(" AND ").append("_weekOfTerm:(");
					List<String> weeks = new ArrayList<>(term.getWeeks());
					Collections.sort(weeks);
					sb.append(String.join(" ", weeks));
					sb.append(")) ");
				}
			}
		}
		return sb.toString().trim();
	}

	private static JsonNode loadSubstatFilters(QueryModel queryModel, String curCollection) {
		// Set up the time facet template; it will be duplicated for each substat
		JsonNode timeFacetNode = SolrQueryTemplateManager.getTimeFacets();
		Timeframe timeframe = queryModel.getTimeframe();
		if (timeframe == Timeframe.WEEK) {
			// leafNode points to the bottom of the tree; we just have to add the day facet
			// at the bottom
			ObjectNode leafNode = getLeafNode(timeframe, timeFacetNode);
			leafNode.set("facet", SolrQueryTemplateManager.getDayFacet());
			// leafNode = (ObjectNode) leafNode.path("facet").path("day");
		}

		String pStat = queryModel.getStat();
		List<String> substats = queryModel.getSubStats();
		if (substats.size() == 0) {
			// with no substats, we'll add the "total" substat by default; this will
			// simplify parsing the Solr response
			substats.add(StatFilterManager.TOTAL_NAME);
		}

		ObjectNode retNode = mapper.createObjectNode();
		for (String substat : substats) {
			if (StatFilterManager.substatContainsCollection(pStat, substat, curCollection)) {
				// set up the base facet for each substat
				ObjectNode paramsNode = retNode.with(substat);
				paramsNode.put("type", "query");
				paramsNode.put("q", StatFilterManager.getSubstatFilter(pStat, substat));
				paramsNode.set("facet", timeFacetNode.deepCopy());
				String substatReplaceCountWith = StatFilterManager.getSubstatReplaceCountWith(pStat, substat);
				if (substatReplaceCountWith != null) {
					// we'll be using a function in place of the "count" field in the result
					String function = StatFilterManager.getSubstatFunctions(pStat, substat)
							.get(substatReplaceCountWith);
					ObjectNode leafNode = getLeafNode(timeframe, paramsNode.path("facet"));
					leafNode.with("facet").put(substatReplaceCountWith, function);
				}
			}
		}
		return retNode;
	}

	private static ObjectNode getLeafNode(Timeframe timeframe, JsonNode rootNode) {
		ObjectNode leafNode = (ObjectNode) rootNode.path("year").path("facet").path("term").path("facet").path("week");
		if (timeframe == Timeframe.WEEK) {
			JsonNode dayLeafNode = leafNode.path("facet").path("day");
			if (dayLeafNode.isMissingNode()) { // day facet hasn't been put on yet
				return leafNode;
			}
			leafNode = (ObjectNode) dayLeafNode;
		}
		return leafNode;
	}

}
