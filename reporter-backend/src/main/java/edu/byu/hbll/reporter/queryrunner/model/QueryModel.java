package edu.byu.hbll.reporter.queryrunner.model;

import java.util.ArrayList;
import java.util.List;

public class QueryModel {
	String query;
	int limit;
	// add an array of years

	// if array of terms or weeks is empty, I will assume all terms and weeks are to
	// be included
	ArrayList<String> years = new ArrayList<>();
	ArrayList<Term> terms = new ArrayList<>();
	ArrayList<Week> weeks = new ArrayList<>();
	Timeframe timeframe = Timeframe.YEAR;

	String stat;
	ArrayList<String> subStats = new ArrayList<>();

	public Timeframe getTimeframe() {
		return timeframe;
	}

	public void setTimeframe(Timeframe timeframe) {
		this.timeframe = timeframe;
	}

	public enum Timeframe {
		YEAR, TERM, WEEK
	}

	public ArrayList<Week> getWeeks() {
		return weeks;
	}

	public void setWeeks(ArrayList<Week> weeks) {
		this.weeks = weeks;
	}

	public void addWeek(String year, String term, String week) {
		weeks.add(new Week(year, term, week));
	}

	public ArrayList<String> getYears() {
		return years;
	}

	public void setYears(ArrayList<String> years) {
		this.years = years;
	}

	public void addYear(String year) {
		years.add(year);
	}

	public ArrayList<Term> getTerms() {
		return terms;
	}

	public void addTerm(String year, String term) {
		terms.add(new Term(year, term));
	}

	public void setTerms(ArrayList<Term> terms) {
		this.terms = terms;
	}

	public void setStat(String stat) {
		this.stat = stat;
	}

	public String getStat() {
		return stat;
	}

	public ArrayList<String> getSubStats() {
		return subStats;
	}

	public void addSubStat(String substat) {
		subStats.add(substat);
	}

	public void addAllSubStats(List<String> substats) {
		subStats.addAll(substats);
	}
}
