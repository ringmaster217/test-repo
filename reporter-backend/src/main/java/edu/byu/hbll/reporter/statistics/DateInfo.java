package edu.byu.hbll.reporter.statistics;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.time.*;
import java.time.temporal.TemporalField;
import java.time.temporal.WeekFields;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

public class DateInfo {

    ObjectMapper mapper = new ObjectMapper();

    public boolean isLeapYear(int year) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        return cal.getActualMaximum(Calendar.DAY_OF_YEAR) > 365;
    }

    public int getWeeksOfTerm(int term) {
        // fixing term #
        if (term <= 0)
            term += 4;

        if (term == 1 || term == 2) {
            return 16;
        } else if (term == 3 || term == 4) {
            return 8;
        }
        return 0;
    }

    public String getCurrentDate() {
        StringBuilder currentDate = new StringBuilder();

        LocalDateTime date = LocalDateTime.ofInstant(Instant.now(), ZoneId.of("US/Mountain"));
        TemporalField woy = WeekFields.of(DayOfWeek.SUNDAY, 1).weekOfWeekBasedYear();
        int month = date.getMonthValue();
        int weekOfYear = date.get(woy);

        if (month == Month.DECEMBER.getValue() && weekOfYear == 1) {
            LocalDateTime tmpDate = date;
            while (weekOfYear == 1) {
                tmpDate = tmpDate.minusDays(1);
                weekOfYear = tmpDate.get(woy);
            }
            weekOfYear++;
        }

        int academicYear = date.getYear();
        if (month >= 9) {
            academicYear++;
        }

        int term = 0;
        LocalDateTime termStart;
        if (month >= 1 && month <= 4) {
            term = 2;
            termStart = LocalDateTime.of(date.getYear(), 1, 1, 0, 0);
        } else if (month >= 5 && month <= 6) {
            term = 3;
            termStart = LocalDateTime.of(date.getYear(), 5, 1, 0, 0);
        } else if (month >= 7 && month <= 8) {
            term = 4;
            termStart = LocalDateTime.of(date.getYear(), 7, 1, 0, 0);
        } else {
            term = 1;
            termStart = LocalDateTime.of(date.getYear(), 9, 1, 0, 0);
        }

        int termWeek = weekOfYear - termStart.get(woy) + 1;
        currentDate.append(academicYear);
        currentDate.append("-");
        currentDate.append(term);
        currentDate.append("-");
        currentDate.append(termWeek);
        return currentDate.toString();
    }

    public ObjectNode getWeek(int academicYear, int term, int week) {
        ObjectNode weekInfo = mapper.createObjectNode();
        Calendar cal = Calendar.getInstance();

        switch (term) {
            case 1: // fall semester
                cal.setTime(getFallStartDate(academicYear));
                break;
            case 2: // winter semester
                cal.setTime(getWinterStartDate(academicYear + 1));
                break;
            case 3: // spring term
                cal.setTime(getSpringStartDate(academicYear + 1));
                break;
            case 4: // summer term
                cal.setTime(getSummerStartDate(academicYear + 1));
                break;
            default: // default is fall semester
                cal.setTime(getFallStartDate(academicYear + 1));
                break;
        }

        cal.add(Calendar.WEEK_OF_YEAR, week - 1);
        cal.set(Calendar.DAY_OF_WEEK, 1);

        weekInfo.put("start", cal.getTime().getTime());
        weekInfo.put("start_day", cal.getTime().toString());

        cal.set(Calendar.DAY_OF_WEEK, 7);
        weekInfo.put("end", cal.getTime().getTime());
        weekInfo.put("end_day", cal.getTime().toString());

        return weekInfo;
    }

    public boolean inSemester(Date date) {
        int year = calcAcademicYear(date);
        if (inFallSemester(date, year)) {
            return true;
        } else if (inWinterSemester(date, year)) {
            return true;
        } else if (inSpringSemester(date, year)) {
            return true;
        } else if (inSummerSemester(date, year)) {
            return true;
        }
        return false;
    }

    public boolean inFallSemester(Date date, int year) {
        Date start = getFallStartDate(year);
        Date end = getFallEndDate(year);
        return (start.before(date) && end.after(date)) || date.equals(start) || date.equals(end);
    }

    public boolean inWinterSemester(Date date, int year) {
        Date start = getWinterStartDate(year + 1);
        Date end = getWinterEndDate(year + 1);
        return (start.before(date) && end.after(date)) || date.equals(start) || date.equals(end);
    }

    public boolean inSpringSemester(Date date, int year) {
        Date start = getSpringStartDate(year + 1);
        Date end = getSpringEndDate(year + 1);
        return (start.before(date) && end.after(date)) || date.equals(start) || date.equals(end);
    }

    public boolean inSummerSemester(Date date, int year) {
        Date start = getSummerStartDate(year + 1);
        Date end = getSummerEndDate(year + 1);
        return (start.before(date) && end.after(date)) || date.equals(start) || date.equals(end);
    }

    public ObjectNode getSemesterInfo(Date date) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int week = calendar.get(Calendar.WEEK_OF_YEAR);
        int year = calendar.get(Calendar.YEAR);

        ObjectNode result = getSemesterDatesbyWeek(week, year);

        result.put("date", date.toString());
        return result;
    }

    public int calcAcademicYear(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int week = calendar.get(Calendar.WEEK_OF_YEAR);
        int year = calendar.get(Calendar.YEAR);

        if (week < 36) {
            // if it is winter, spring or summer
            // the start of the academic year is the year before
            year--;
        }

        return year;
    }

    public ObjectNode getAdjustedYearDates(ArrayNode years) {
        ObjectNode result = mapper.createObjectNode();
        Calendar cal = Calendar.getInstance();
        Date earliest = new Date();
        Date latest = new Date();

        int year = cal.get(Calendar.YEAR);
        boolean firstEarly = true;
        boolean firstLatest = true;

        for (Iterator<JsonNode> it = years.iterator(); it.hasNext(); ) {
            ObjectNode yearInfo = (ObjectNode) it.next();

            cal.setTime(new Date(yearInfo.path("start").asLong()));
            cal.set(Calendar.YEAR, year);
            if (firstEarly) {
                earliest = cal.getTime();
                firstEarly = false;
            }
            if (cal.getTime().before(earliest)) {
                earliest = cal.getTime();
            }

            cal.setTime(new Date(yearInfo.path("end").asLong()));
            cal.set(Calendar.YEAR, year);
            if (firstLatest) {
                latest = cal.getTime();
                firstLatest = false;
            }
            if (cal.getTime().after(latest)) {
                latest = cal.getTime();
            }
        }

        result.put("start", earliest.getTime());
        result.put("end", latest.getTime());
        result.put("start_day", earliest.toString());
        result.put("end_day", latest.toString());

        result.set("years", years);

        return result;
    }

    public ObjectNode getAdjustedYearDates2(ArrayNode years) {
        ObjectNode result = mapper.createObjectNode();

        Calendar cal = Calendar.getInstance();
        int startDayFirstWeek = 7;
        long days = 0;
        boolean has3WeekBreak = false;
        ArrayNode stuff = mapper.createArrayNode();

        for (Iterator<JsonNode> it = years.iterator(); it.hasNext(); ) {
            ObjectNode yearInfo = (ObjectNode) it.next();
            Date start = new Date(yearInfo.path("start").asLong());

            cal.setTime(start);
            int startDay = cal.get(Calendar.DAY_OF_WEEK);
            if (startDayFirstWeek > startDay) {
                startDayFirstWeek = startDay;
            }
        }


        //once we have when the first day of the week starts, we caan see the amount of days needed

        for (Iterator<JsonNode> it = years.iterator(); it.hasNext(); ) {
            ObjectNode info = mapper.createObjectNode();

            ObjectNode yearInfo = (ObjectNode) it.next();
            Calendar start = Calendar.getInstance();
            start.setTime(new Date(yearInfo.path("start").asLong()));
            start.set(Calendar.DAY_OF_WEEK, startDayFirstWeek);
            Date end = new Date(yearInfo.path("end").asLong());

            long diffInMillies = Math.abs(end.getTime() - start.getTime().getTime());
            long dayDiff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS) + 1;
            long weeks = dayDiff / 7;

            if (dayDiff > days)
                days = dayDiff;

            if (weeks > 52)
                has3WeekBreak = true;

            info.put("three_week_break", weeks > 52);
            info.put("weeks", weeks);
            info.put("days", dayDiff);
            info.set("year", yearInfo.path("academic_year"));
            result.set(Integer.toString(calcAcademicYear(start.getTime())), info);
        }

        result.put("three_week_break", has3WeekBreak);
        result.put("start_day", startDayFirstWeek);
        result.put("days", days);
        return result;
    }

    public String getWeek(Date date) {
        if (!inSemester(date))
            return "Break";

        int year = calcAcademicYear(date);
        Calendar startday = Calendar.getInstance();
        Calendar current = Calendar.getInstance();
        current.setTime(date);

        if (inFallSemester(date, year))
            startday.setTime(getFallStartDate(year));
        else if (inWinterSemester(date, year))
            startday.setTime(getWinterStartDate(year + 1));
        else if (inSpringSemester(date, year))
            startday.setTime(getSpringStartDate(year + 1));
        else if (inSummerSemester(date, year))
            startday.setTime(getSummerStartDate(year + 1));

        int weekNum = current.get(Calendar.WEEK_OF_YEAR) - startday.get(Calendar.WEEK_OF_YEAR) + 1;

        return "Week " + weekNum;
    }

    public ObjectNode getAdjustedSemInfo(ArrayNode semesters) {
        ObjectNode adjustedDates = mapper.createObjectNode();
        int weeks = 0;
        int startDayFirstWeek = 7;
        int endDayLastWeek = 0;

        for (Iterator<JsonNode> it = semesters.iterator(); it.hasNext(); ) {
            ObjectNode semesterInfo = (ObjectNode) it.next();
            int currentWeeks = 0;
            String semester = semesterInfo.path("semester").asText().toLowerCase();

            if (semester.equals("fall") || semester.equals("winter")) {
                currentWeeks = 16;
            } else if (semester.equals("spring") || semester.equals("summer")) {
                currentWeeks = 8;
            }

            Calendar cal = Calendar.getInstance();
            cal.setTime(new Date(semesterInfo.path("start").asLong()));
            int currentStartDay = cal.get(Calendar.DAY_OF_WEEK);

            cal.setTime(new Date(semesterInfo.path("end").asLong()));
            int currentEndDay = cal.get(Calendar.DAY_OF_WEEK);

            if (currentWeeks > weeks) {
                weeks = currentWeeks;
            }

            if (currentStartDay < startDayFirstWeek) {
                startDayFirstWeek = currentStartDay;
            }

            if (currentEndDay > endDayLastWeek) {
                endDayLastWeek = currentEndDay;
            }
        }

        adjustedDates.put("weeks", weeks);
        adjustedDates.put("start_day", startDayFirstWeek);
        adjustedDates.put("end_day", endDayLastWeek);
        adjustedDates.set("semesters", semesters);

        return adjustedDates;
    }

    public ObjectNode getAcademicYearbyDate(Date date) {
        int year = calcAcademicYear(date);
        ObjectNode result = getYearDates(year);

        ArrayNode semesters = mapper.createArrayNode();

        semesters.add(getSemesterDatesbySemester("fall", year));
        semesters.add(getSemesterDatesbySemester("winter", year + 1));
        semesters.add(getSemesterDatesbySemester("spring", year + 1));
        semesters.add(getSemesterDatesbySemester("summer", year + 1));

        result.set("semesters", semesters);

        return result;
    }

    public ObjectNode getAcademicYearbyYear(int year) {
        ObjectNode result = getYearDates(year);

        ArrayNode semesters = mapper.createArrayNode();

        semesters.add(getSemesterDatesbySemester("fall", year));
        semesters.add(getSemesterDatesbySemester("winter", year + 1));
        semesters.add(getSemesterDatesbySemester("spring", year + 1));
        semesters.add(getSemesterDatesbySemester("summer", year + 1));

        result.set("semesters", semesters);

        return result;
    }

    public ArrayNode getPreviousAcademicYears(int year, int amt) {
        ArrayNode years = mapper.createArrayNode();

        for (int i = 0; i <= amt; i++) {
            years.add(getAcademicYearbyYear(year - i));
        }
        return years;
    }

    public ArrayNode getPreviousSemestersbyDate(Date date, int amt) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int week = calendar.get(Calendar.WEEK_OF_YEAR);
        int year = calendar.get(Calendar.YEAR);

        ArrayNode semesters = mapper.createArrayNode();

        ObjectNode currentSem = getSemesterDatesbyWeek(week, year);

        semesters.add(currentSem);

        for (int i = 0; i < amt; i++) {
            String semester = currentSem.path("semester").asText().toLowerCase().trim();
            int currentYear = currentSem.path("year").asInt();

            switch (semester) {
                case "fall":
                    currentSem = getSemesterDatesbySemester("summer", currentYear);
                    break;
                case "winter":
                    currentSem = getSemesterDatesbySemester("fall", currentYear - 1);
                    break;
                case "spring":
                    currentSem = getSemesterDatesbySemester("winter", currentYear);
                    break;
                case "summer":
                    currentSem = getSemesterDatesbySemester("spring", currentYear);
                    break;
                default:
                    currentSem = getSemesterDatesbySemester("summer", currentYear);
                    break;
            }
            semesters.add(currentSem);
        }

        return semesters;
    }

    public ObjectNode getSemesterDatesbyWeek(int week, int year) {

        if (week < 18) {
            return getWinterDates(year);
        } else if (week < 26) {
            return getSpringDates(year);
        } else if (week < 36) {
            return getSummerDates(year);
        } else {
            return getFallDates(year);
        }
    }

    public ObjectNode getSemesterDatesbySemester(String semester, int year) {

        switch (semester) {
            case "fall":
                return getFallDates(year);
            case "winter":
                return getWinterDates(year);
            case "spring":
                return getSpringDates(year);
            case "summer":
                return getSummerDates(year);
            default:
                return getFallDates(year);
        }
    }

    public ObjectNode getSemesterDates(int term, int academicYear) {

        switch (term) {
            case 1:
                return getFallDates(academicYear);
            case 2:
                return getWinterDates(academicYear + 1);
            case 3:
                return getSpringDates(academicYear + 1);
            case 4:
                return getSummerDates(academicYear + 1);
            default:
                return getFallDates(academicYear);
        }
    }

    public ObjectNode getYearDates(int year) {
        ObjectNode result = mapper.createObjectNode();

        Date start = getFallStartDate(year);
        Calendar end = Calendar.getInstance();
        end.setTime(getFallStartDate(year + 1));
        end.add(Calendar.MILLISECOND, -1);

        result.put("academic_year", year + "-" + (year + 1));
        result.put("start", start.getTime());
        result.put("end", end.getTime().getTime());
        result.put("start_day", start.toString());
        result.put("end_day", end.getTime().toString());

        return result;
    }

    public ObjectNode getFallDates(int year) {
        ObjectNode fallDates = mapper.createObjectNode();

        fallDates.put("start", getFallStartDate(year).getTime());
        fallDates.put("end", getFallEndDate(year).getTime());
        fallDates.put("start_day", getFallStartDate(year).toString());
        fallDates.put("end_day", getFallEndDate(year).toString());
        fallDates.put("semester", "Fall");
        fallDates.put("year", year);
        fallDates.put("name", "Fall " + year);

        return fallDates;
    }

    public ObjectNode getWinterDates(int year) {
        ObjectNode winterDates = mapper.createObjectNode();

        winterDates.put("start", getWinterStartDate(year).getTime());
        winterDates.put("end", getWinterEndDate(year).getTime());
        winterDates.put("start_day", getWinterStartDate(year).toString());
        winterDates.put("end_day", getWinterEndDate(year).toString());
        winterDates.put("semester", "Winter");
        winterDates.put("year", year);
        winterDates.put("name", "Winter " + year);

        return winterDates;
    }

    public ObjectNode getSpringDates(int year) {
        ObjectNode springDates = mapper.createObjectNode();

        springDates.put("start", getSpringStartDate(year).getTime());
        springDates.put("end", getSpringEndDate(year).getTime());
        springDates.put("start_day", getSpringStartDate(year).toString());
        springDates.put("end_day", getSpringEndDate(year).toString());
        springDates.put("semester", "Spring");
        springDates.put("year", year);
        springDates.put("name", "Spring " + year);

        return springDates;
    }

    public ObjectNode getSummerDates(int year) {
        ObjectNode summerDates = mapper.createObjectNode();

        summerDates.put("start", getSummerStartDate(year).getTime());
        summerDates.put("end", getSummerEndDate(year).getTime());
        summerDates.put("start_day", getSummerStartDate(year).toString());
        summerDates.put("end_day", getSummerEndDate(year).toString());
        summerDates.put("semester", "Summer");
        summerDates.put("year", year);
        summerDates.put("name", "Summer " + year);

        return summerDates;
    }

    public Date getWinterStartDate(int year) {
        Calendar calendar = Calendar.getInstance();

        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.WEEK_OF_YEAR, 2);
        calendar.set(Calendar.DAY_OF_WEEK, 2);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        return calendar.getTime();
    }

    public Date getWinterEndDate(int year) {
        Calendar calendar = Calendar.getInstance();

        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.WEEK_OF_YEAR, 17);
        calendar.set(Calendar.DAY_OF_WEEK, 5);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        // This will make it the day before at the last second
        calendar.add(Calendar.MILLISECOND, -1);

        return calendar.getTime();
    }

    public Date getSpringStartDate(int year) {
        Calendar calendar = Calendar.getInstance();

        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.WEEK_OF_YEAR, 18);
        calendar.set(Calendar.DAY_OF_WEEK, 3);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        return calendar.getTime();
    }

    public Date getSpringEndDate(int year) {
        Calendar calendar = Calendar.getInstance();

        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.WEEK_OF_YEAR, 25);
        calendar.set(Calendar.DAY_OF_WEEK, 6);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        // This will make it the day before at the last second
        calendar.add(Calendar.MILLISECOND, -1);

        return calendar.getTime();
    }

    public Date getSummerStartDate(int year) {
        Calendar calendar = Calendar.getInstance();

        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.WEEK_OF_YEAR, 26);
        calendar.set(Calendar.DAY_OF_WEEK, 2);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        return calendar.getTime();
    }

    public Date getSummerEndDate(int year) {
        Calendar calendar = Calendar.getInstance();

        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.WEEK_OF_YEAR, 33);
        calendar.set(Calendar.DAY_OF_WEEK, 6);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        // This will make it the day before at the last second
        calendar.add(Calendar.MILLISECOND, -1);

        return calendar.getTime();
    }

    public Date getFallStartDate(int year) {
        Calendar calendar = Calendar.getInstance();

        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.WEEK_OF_YEAR, 36);
        calendar.set(Calendar.DAY_OF_WEEK, 2);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        // accounts for labor day
        if (calendar.get(Calendar.DAY_OF_WEEK_IN_MONTH) == 1) {
            calendar.set(Calendar.DAY_OF_WEEK, 3);
        }

        return calendar.getTime();
    }

    public Date getFallEndDate(int year) {
        Calendar calendar = Calendar.getInstance();

        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.WEEK_OF_YEAR, 51);
        calendar.set(Calendar.DAY_OF_WEEK, 6);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        // This will make it the day before at the last second
        // add one hour for daylight saving time


        return calendar.getTime();
    }

}