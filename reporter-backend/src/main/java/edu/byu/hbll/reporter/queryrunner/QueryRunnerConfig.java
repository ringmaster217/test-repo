package edu.byu.hbll.reporter.queryrunner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;

public class QueryRunnerConfig {
	private static boolean initialized = false;
	private static final Logger logger = LoggerFactory.getLogger(QueryRunnerConfig.class);

	public static QueryRunnerConfig instance() {
		return new QueryRunnerConfig();
	}

	public static void initialize(JsonNode config) {
		if (initialized) {
			logger.warn("Attempt to re-initialize " + QueryRunnerConfig.class.getName());
			return;
		}
		StatFilterManager.initialize(config.path(StatFilterManager.CONFIG_NODE));
		TableTimeFacetCalculator.initialize(config.path(TableTimeFacetCalculator.CONFIG_NODE));
		SolrQueryTemplateManager.initializeTemplates(config.path(SolrQueryTemplateManager.CONFIG_TEMPLATES_NODE));
		SolrQueryTemplateManager.initializeCollections(config.path(SolrQueryTemplateManager.CONFIG_COLLECTIONS_NODE));
		initialized = true;
	}
}
