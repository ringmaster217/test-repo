import { ChartTemplate } from './charttemplate';
import { OptionsTemplate } from './optionstemplate';
import { OptionMenu } from '../options/optionmenu';

export class Report {
  public linegraph: ChartTemplate;
  public table: ChartTemplate;
  public options: OptionsTemplate;

  constructor () { }
  
  static getGraphReport (baseURL: string, reportID: string) : Report {
    var report = new Report();
    report.graphReport(baseURL, reportID);
    return report;
  }
  
  static getGraphReportWithOptions (baseURL: string, reportID: string, optionMenuJSON: string) : Report {
    var report = new Report();
    report.graphReportWithOptions(baseURL, reportID, optionMenuJSON);
    return report;
  }
  
  static getTableReport (baseURL: string, reportID: string) : Report {
    var report = new Report();
    report.tableReport(baseURL, reportID);
    return report;
  }
  
  static getTableReportWithOptions (baseURL: string, reportID: string, optionMenuJSON) : Report {
    var report = new Report();
    report.tableReportWithOptions(baseURL, reportID, optionMenuJSON);
    return report;
  }
  
  
  
  
  
  
  
  
  private graphReport (baseURL: string, reportID: string) {
    this.linegraph = new ChartTemplate();
    this.linegraph.identifier = "linegraph";

    if (baseURL[baseURL.length - 1] == "/") {
      this.linegraph.defaultValueUrl = baseURL + reportID;
    } 
    else {
      this.linegraph.defaultValueUrl = baseURL + "/" + reportID + "?"; 
    } 
  }
  
  private graphReportWithOptions (baseURL: string, reportID: string, optionMenuJSON: string) {
    this.linegraph = new ChartTemplate();
    this.linegraph.identifier = "linegraph";
    
    this.options = new OptionsTemplate(); 
    this.options.identifier = "linegraphOptions"; 
     
    var reconstitutedMenu = <OptionMenu> JSON.parse(optionMenuJSON);
    this.options.optionMenu = reconstitutedMenu;
    
    if (baseURL[baseURL.length - 1] == "/") {
      this.options.optionMenu.urlStub = baseURL + reportID + "?";
    } 
    else {
      this.options.optionMenu.urlStub = baseURL + "/" + reportID + "?"; 
    } 
    
  }
  
  private tableReport(baseURL: string, reportID: string) {
    this.table = new ChartTemplate();
    this.table.identifier = "table";
    if (baseURL[baseURL.length - 1] == "/") {
      this.table.defaultValueUrl = baseURL + reportID;
    }
    else {
      this.table.defaultValueUrl = baseURL + "/" + reportID;
    }
  }
  
  private tableReportWithOptions (baseURL: string, reportID: string, optionMenuJSON: string) {
    this.table = new ChartTemplate();
    this.table.identifier = "table";
     
    this.options = new OptionsTemplate(); 
    this.options.identifier = "tableOptions"; 
     
    var reconstitutedMenu = <OptionMenu> JSON.parse(optionMenuJSON);
    this.options.optionMenu = reconstitutedMenu;
    
    if (baseURL[baseURL.length - 1] == "/") {
      this.options.optionMenu.urlStub = baseURL + reportID + "?";
    }
    else {
      this.options.optionMenu.urlStub = baseURL + "/" + reportID + "?"; 
    }
  }
} 