
import { OptionMenu } from '../options/optionmenu';

export class OptionsTemplate {
  public identifier: string;
  public linkedChartIdentifier: string;
  public optionMenu: OptionMenu;
}