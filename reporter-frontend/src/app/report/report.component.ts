import {
  Component,
  OnInit,
  ViewChild,
  ViewContainerRef,
  ComponentFactoryResolver,
  ComponentRef,
  ComponentFactory
} from '@angular/core';
import { environment } from '../../environments/environment'; 
import { OptionsComponent } from '../options/options.component';
import { LineGraphComponent } from '../line-graph/line-graph.component';
import { TableComponent } from '../table/table.component';
import { ReporterService } from '../reporter.service';
import { LineGraphData } from '../line-graph/linegraphdata';
import { ProtoReportList } from '../protoreportlist';
import { ProtoGraphResult } from '../protographresult';
import { ProtoTable } from '../prototable';
import { TableData } from '../table/tabledata'; 
import { OptionResult } from '../options/optionresult';
import { Report } from './report';
import { ProtoReportTypeList } from "../protoreporttypelist";
import { OptionMenu } from "../options/optionmenu";

@Component( {
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.css']
} )
export class ReportComponent implements OnInit {

  private showSideBar: boolean; 

public testing: string;

  private report: Report;
  private reportList: ProtoReportList;

  showGraph: boolean;
  showTable: boolean; 
  showOptions: boolean; 
  
  loadedTable: boolean;

  reportOptionMenu: OptionMenu; 
 // private reSetUp: boolean = false;
  
  graphComponentRef: any;
  tableComponentRef: any;
  
 // private charts: Map<string, string>; //report identifier, chart type

  @ViewChild('graphcontainer', { read: ViewContainerRef } ) graphEntry: ViewContainerRef;
  @ViewChild('tablecontainer', { read: ViewContainerRef}) tableEntry: ViewContainerRef;
  @ViewChild(OptionsComponent) options;
  
  constructor( private resolver: ComponentFactoryResolver, private reporterService: ReporterService ) {
    this.showSideBar = false;
    this.showOptions = false;
    this.showGraph = false;
    this.showTable = false;
    this.loadedTable = false;
   // this.charts = new Map<string, string>();  
    
  }

  ngOnInit() {
    this.makeReport(); 
  }
  
  createGraphComponent( graphData ) {
    if ( this.graphEntry != null ) {
      this.graphEntry.clear();
    }

    const factory = this.resolver.resolveComponentFactory( LineGraphComponent );
    this.graphComponentRef = this.graphEntry.createComponent( factory );
    this.graphComponentRef.instance.graphData = graphData;

  }

  createTableComponent( tableData ) {
    if (this.tableEntry != null) {
      this.tableEntry.clear();
    }
    const factory = this.resolver.resolveComponentFactory(TableComponent);
    this.tableComponentRef = this.tableEntry.createComponent(factory);
    this.tableComponentRef.instance.tableData = tableData;
  }

  destroyComponents() {
    this.graphComponentRef.destroy();
    this.tableComponentRef.destroy();
  }
   
  private catchOptions( event ) {
   // this.reSetUp = false;
    var optionResult = <OptionResult> JSON.parse(event);
    
    if (this.report.linegraph != null) {
      var protoGraph = <ProtoGraphResult> JSON.parse(optionResult.chartJSON);
      this.createGraphComponent( new LineGraphData(protoGraph));
    }
    else if (this.report.table != null) {
      var protoTable = <ProtoTable> JSON.parse(optionResult.chartJSON);
      this.createTableComponent( new TableData(protoTable));
      this.loadedTable = true;
    }
    else {
     // this.testing = "both are null";
    }
  }
  
  private catchReport( event ) {
    var reportID = event.toString();
    
    if (this.reportList != null) {
      for (var i = 0; i < this.reportList.reports.length; ++i) {
        var reportTypeList = this.reportList.reports[i];
        for (var j = 0; j < reportTypeList.reports.length; ++j) {
          if (reportTypeList.reports[j].identifier == reportID) {
            if (reportTypeList.type == "graph") {
               if (reportTypeList.reports[j].optionsUrl != null) {
                 this.reporterService.getReport(reportTypeList.reports[j].optionsUrl).then(response => this.processGraphReport(reportID, JSON.stringify(response)));
               }
               else {
                 this.processGraphReport(reportID, null);
               }
            }
            else if (reportTypeList.type == "table") {
              if (reportTypeList.reports[j].optionsUrl != null) {
                this.showTable = true;
                this.loadedTable = false;
                this.reporterService.getReport(reportTypeList.reports[j].optionsUrl).then(response => this.processTableReport(reportID, JSON.stringify(response)));
              }
              else {
                this.processTableReport(reportID, null); 
              }
            }
            j = reportTypeList.reports.length;
            i = this.reportList.reports.length;
          }
        }
      }
    }
    
  }
  
  private processReports (json: string) { 
    this.reportList = <ProtoReportList> JSON.parse(json);
  }
  
  private processGraphReport(reportID: string, optionMenuJSON: string) {
    if (optionMenuJSON == null) {
      this.report = Report.getGraphReport(this.reportList.baseUrl, reportID);
    }
    else {
      this.report = Report.getGraphReportWithOptions(this.reportList.baseUrl, reportID, optionMenuJSON);
    }
    this.makeReport();
  }
  
  private processTableReport(reportID: string, optionMenuJSON: string) {
    
    if (optionMenuJSON != null) {
      this.testing = JSON.stringify(optionMenuJSON);
      this.report = Report.getTableReportWithOptions(this.reportList.baseUrl, reportID, optionMenuJSON);
      this.testing = JSON.stringify(this.report.options);
    }
    else {
      this.report = Report.getTableReport(this.reportList.baseUrl, reportID);
    }
    this.makeReport();
  }
  
  private makeReport() {
    if (this.report != null) {
      if (this.report.linegraph != null) { 
        this.showGraph = true;
      //  this.charts.set(this.report.linegraph.identifier, "linegraph");
      //  this.testing = this.testing + " set in chart: " + this.report.linegraph.identifier;
        if (this.report.linegraph.defaultValueUrl != null) {
          this.reporterService.getReport(this.report.table.defaultValueUrl).then(response => this.initGraph(JSON.stringify(response)));
        }
      }
      else {
        this.showGraph = false;
      }
      if (this.report.table != null) {
        this.showTable = true; 
        this.loadedTable = false;
      //  this.charts.set(this.report.table.identifier, "table");
        if (this.report.table.defaultValueUrl != null) {
          this.reporterService.getReport(this.report.table.defaultValueUrl).then(response => this.initTable(JSON.stringify(response)));
        }
        else {
         // this.loadedTable = true;
        } 
        
      }
      else {
        this.showTable = false;
      }
      if (this.report.options != null) {
        this.reportOptionMenu = this.report.options.optionMenu;
        this.showOptions = true;
      }
      else {
        this.showOptions = false;
      }
    }
  }

  private initTable (response: string) {
    this.showTable = true; 
    var protoTable = <ProtoTable> JSON.parse(response);
    var tableData = new TableData (protoTable); 
    this.createTableComponent(tableData);
    this.loadedTable = true;
  }
  
  private initGraph (response: string) {
    this.showGraph = true;
    var protoGraph = <ProtoGraphResult> JSON.parse(response);
    var graphData = new LineGraphData(protoGraph);
    this.createGraphComponent(graphData);
  }

  checkSideBar() : boolean {
    return this.showSideBar;
  }
  
  private toggleSideBar() {
    this.showSideBar = !this.showSideBar;
  } 
  
  private getReportGroups() : ProtoReportTypeList[] {
    if (this.reportList == null) { 
      var query = environment["urlBase"]+ "/reporter/api/reports";
      this.reporterService.getReport(query).then(response => this.processReports(JSON.stringify(response)));
      return new Array<ProtoReportTypeList>(0);
    }
    return this.reportList.reports;
  }
}
