import { ProtoReport } from './protoreport';

export class ProtoReportTypeList {
  public type: string;
  public displayName: string;
  public reports: ProtoReport[];
}