import { TableSubset } from './tablesubset';

export class TableGroup {
  public displayName: string;
  public barColor: string;
  public textColor: string;
  public subsets: TableSubset[];
    
  getBarColor() : string {
    if (this.barColor == null) {
      return "rgba(225, 225, 225, 1)";
    }
    return this.barColor;
  }
    
  getTextColor() : string {
    if (this.textColor == null) {
      return this.getBarColor();
    }
    return this.textColor;
  }
    
  add (subset: TableSubset) {
    if (this.subsets == null) {
      this.subsets = new Array<TableSubset> ();
    }
    this.subsets.push(subset);
  }
  
}