
import { TableGroup } from './tablegroup'; 
import { TableSubset } from './tablesubset'; 
import { ProtoTable } from '../prototable';
import { ObjectMapper } from "json-object-mapper/dist/ObjectMapper";


export class TableData { 

  public title: string;
  public rows: Array<Array<any>>;
  public columnGroups: Array<TableGroup>;
  public rowGroups: Array<TableGroup>;

  public rowEmphasisColor: string = "rgb(237, 245, 255)";
  public colGroupBaseColors: string[] = 
                                    ["rgb(255, 96, 96)",
                                      "rgb(37, 204, 37)",
                                      "rgb(97, 123, 255)",
                                      "rgb(255, 176, 97)",
                                      "rgb(194, 97, 255)"];
  public colCellBorderColors: string[] = 
                                    ["rgb(255, 173, 173)",
                                     "rgb(150, 232, 109)",
                                     "rgb(150, 168, 255)",
                                     "rgb(255, 210, 166)",
                                     "rgb(223, 176, 252)"];
  public colCellBackgroundColors: string[] = 
                                    ["rgb(255, 245, 245)",
                                     "rgb(248, 255, 245)",
                                     "rgb(245, 247, 255)",
                                     "rgb(255, 250, 245)",
                                     "rgb(249, 244, 252)"];
                                    
  constructor(protoTable: ProtoTable) {
    this.title = protoTable.displayTitle;
    
    if ( (protoTable == null)
          || (protoTable.colNames == null)
          || (protoTable.rowNames == null)
          || (protoTable.tableData == null)
          || (protoTable.tableData['dataRows'] == null)) {
      return;
    }
      
    var colNamesArray = protoTable.colNames;
    var rowNamesArray = protoTable.rowNames;
    var dataRowsObj = protoTable.tableData["dataRows"];
       
    var rowGroups = new Array<TableGroup>();
    var colGroups = new Array<TableGroup>(); 
       
    var data = [];
       
    //get groups, cols & rows (& their displayNames), assign colors
       
    var displayCols = [];
       
    for (var i = 0; i < colNamesArray.length; ++i) {
      var colGroup = new TableGroup();
      var colGroupObj = colNamesArray[i];
           
      colGroup.barColor = this.colGroupBaseColors[i % this.colGroupBaseColors.length];
      colGroup.textColor = this.colGroupBaseColors[i % this.colGroupBaseColors.length];
           
      var colGroupID;
      var subsets;
      for (var j = 0; j < Object.keys(colGroupObj).length; ++j) {
        //assumed: only keys of colGroupObj are (1) [column group name], (2) "subColsNum", and (3) "subColNames"
        if (Object.keys(colGroupObj)[j] == "subColNames") {
          subsets = colGroupObj["subColNames"];
        }
        else if (Object.keys(colGroupObj)[j] != "subColsNum") {
          colGroup.displayName = colGroupObj[Object.keys(colGroupObj)[j]];
          colGroupID = Object.keys(colGroupObj)[j];
        }
      }
      for (var k = 0; k < Object.keys(subsets).length; ++k) {
        var col = new TableSubset();
        col.displayName = subsets[Object.keys(subsets)[k]];
        displayCols.push(colGroupID + "+" + Object.keys(subsets)[k]);
               
        col.headTextColor = this.colGroupBaseColors[i % this.colGroupBaseColors.length];
        if (col.displayName == "Total") {
          col.cellBorderColor = this.colCellBorderColors[i % this.colCellBorderColors.length];
        }
        
        colGroup.add(col);
      }
           
      colGroups.push(colGroup);
    }
    this.columnGroups = colGroups;
       
    for (var i = 0; i < rowNamesArray.length; ++i) {
           var rowGroup = new TableGroup();
           var rowGroupObj = rowNamesArray[i];
           
           var subsets;
           for (var j = 0; j < Object.keys(rowGroupObj).length; ++j) {
               if (Object.keys(rowGroupObj)[j] == "subRowNames") {
                   subsets = rowGroupObj["subRowNames"];
               }
               else if (Object.keys(rowGroupObj)[j] != "subRowsNum") {
                   rowGroup.displayName = rowGroupObj[Object.keys(rowGroupObj)[j]];
               }
           }
           
           for (var k = 0; k < Object.keys(subsets).length; ++k) {
               var row = new TableSubset();
               row.displayName = subsets[Object.keys(subsets)[k]];
               
               if  (row.displayName == "C") {
                   row.headFillColor = this.rowEmphasisColor;
               }
               
               rowGroup.add(row);
           }
           
           rowGroups.push(rowGroup);
    }
    this.rowGroups = rowGroups;

       
    //get the data--ignore if not in column to display
       
    for (var i = 0; i < Object.keys(dataRowsObj).length; ++i) {
      var rowGroupObj = dataRowsObj[Object.keys(dataRowsObj)[i]];

      for (var j = 0; j < Object.keys(rowGroupObj).length; ++j) {
        var rowObj = rowGroupObj[Object.keys(rowGroupObj)[j]]; 
        var dataLine = [];
               
        for (var k = 0; k < Object.keys(rowObj).length; ++k) {
          var colGroupObj = rowObj[Object.keys(rowObj)[k]];
          var colGroupId = Object.keys(rowObj)[k];
                
          for (var l = 0; l < Object.keys(colGroupObj).length; ++l) {
            if (displayCols.indexOf(colGroupId + "+" + Object.keys(colGroupObj)[l]) != -1) {
              var dataPt = colGroupObj[Object.keys(colGroupObj)[l]];
              dataLine.push(dataPt);
            }
          }             
        }  
        data.push(dataLine);
      } 
    }
        
    this.preProcess(data);
       
  }
    
    
  preProcess(data: Array<Array<any>>) {

    //prepend rowNames (data/rows to be updated)
        
    //if have at least two rowGroup, prepend a column
    //if have one rowGroup, & rowGroup.displayName != null, prepend a column
    //if have at least one row name in the first rowGroup, prepend another column
        
    if ((this.rowGroups.length > 1) || ( (this.rowGroups.length == 1) && (this.rowGroups[0].displayName != null)) ) {
      var emptyGroup = new TableGroup();
      emptyGroup.displayName = "";
      emptyGroup.subsets = new Array<TableSubset>(1); 
            
      var emptyColumn = new TableSubset();
      emptyColumn.displayName = "";
      emptyGroup.subsets[0] = emptyColumn;
            
      this.columnGroups.unshift(emptyGroup);
            
      if (this.rowGroups[0].subsets.length > 0) {
        this.columnGroups[0].subsets.push(emptyColumn);
      }
    }

    var rowNumber = 0;
    for (var i = 0; i < this.rowGroups.length; ++i) {
      var thisRowGroup = this.rowGroups[i];
      for (var j = 0; j < thisRowGroup.subsets.length; ++j) {
        data[rowNumber].unshift(thisRowGroup.subsets[j].displayName);
        if (!((this.rowGroups.length == 1) && (this.rowGroups[0].displayName == null))) {
          if (j == 0) {
            data[rowNumber].unshift(thisRowGroup.displayName); 
          }
          else {
            data[rowNumber].unshift("");
          }
        }
        rowNumber = rowNumber + 1;
      }
    } 
        this.rows = data;
  }
    
  getColumnColor(groupNumber: number, columnNumber: number) : string {
    // 'color': (), 'border-bottom-color': ()
    var group = this.columnGroups[groupNumber];
    if (group.subsets.length <= columnNumber) {
      return "initial";
    }
        
    var column = group.subsets[columnNumber];
    if (column == null) {
      return "initial";
    }
        
    if (column.headTextColor == null) {
      return "initial";
    }
    else {
      return column.headTextColor;
    }
  }
    
  getRowCellBackgroundColor(rowNumber: number, cellNumber: number) : string {
        
    // Decide first if row group, row name, or data
        
    var isRowGroup = false; //matters if cellNumber is 0 or 1
    var isRowName = false; //matter if cellNumber is 0 or 1
        
    if ((cellNumber == 0) && (this.rowGroups != null)){
      if ((this.rowGroups.length > 1) || ((this.rowGroups.length == 1) &&  (this.rowGroups[0].displayName != null))) {
        isRowGroup = true;
      }
      else {
        isRowName = true;
      }
    }
        
    if (cellNumber == 1) {   
      if ((this.rowGroups.length > 1) || ((this.rowGroups.length == 1) &&  (this.rowGroups[0].displayName == null))) {
        isRowName = true;
      }
    }
        
    if (isRowGroup == true) {
      return "initial";
    }
    else if (isRowName == true) {
            var thisRowNumber = 0;
            for (var i = 0; i < this.rowGroups.length; ++i) {
                var group = this.rowGroups[i];
                for (var j = 0; j < group.subsets.length; ++j) {
                    if (thisRowNumber == rowNumber) {
                        return group.subsets[j].headFillColor;
                    }
                    thisRowNumber = thisRowNumber + 1;
                }
            }
    }
    else {
      var thisColNumber = 0;
      for (var i = 0; i < this.columnGroups.length; ++i) {
        var group = this.columnGroups[i];
        for (var j = 0; j < group.subsets.length; ++j) {
          if (thisColNumber == cellNumber) {
            return group.subsets[j].cellFillColor;
          }
          thisColNumber = thisColNumber + 1;
        }
      }
    }
    
    return "initial"; 
  }
    
  getRowCellBorderColor(rowNumber: number, cellNumber: number) : string {
        
    // Decide first if row group, row name, or data
        
    var isRowGroup = false; //matters if cellNumber is 0 or 1
    var isRowName = false; //matter if cellNumber is 0 or 1
        
    if ((cellNumber == 0) && (this.rowGroups != null)){
      if ((this.rowGroups.length > 1) || ((this.rowGroups.length == 1) &&  (this.rowGroups[0].displayName != null))) {
        isRowGroup = true;
      }
      else {
        isRowName = true;
      }
    }
        
    if (cellNumber == 1) {
      if ((this.rowGroups.length > 1) || ((this.rowGroups.length == 1) &&  (this.rowGroups[0].displayName == null))) {
        isRowName = true;
      }
    }
          
    if (isRowGroup == true) {
      return "rgba(225, 225, 225, 1)";
    }
    else if (isRowName == true) {
      return "rgba(225, 225, 225, 1)";
    }
    else {
      var thisColNumber = 0;
      for (var i = 0; i < this.columnGroups.length; ++i) {
        var group = this.columnGroups[i];
        for (var j = 0; j < group.subsets.length; ++j) {
          if (thisColNumber == cellNumber) {
            return group.subsets[j].cellBorderColor;
          }
          thisColNumber = thisColNumber + 1;
        }
      }
    }
        
    return "rgba(225, 225, 225, 1)"; 
  }
    
  getRowCellRightBorderColor(rowNumber: number, cellNumber: number) : string {
        
    // Decide first if row group, row name, or data
        
    var isRowGroup = false; //matters if cellNumber is 0 or 1
    var isRowName = false; //matter if cellNumber is 0 or 1
        
    if ((cellNumber == 0) && (this.rowGroups != null)){
      if ((this.rowGroups.length > 1) || ((this.rowGroups.length == 1) &&  (this.rowGroups[0].displayName != null))) {
        isRowGroup = true;
      }
      else {
        isRowName = true;
      }
    }
        
    if (cellNumber == 1) {
      if ((this.rowGroups.length >= 1) || ((this.rowGroups.length == 1) &&  (this.rowGroups[0].displayName == null))) {
        isRowName = true;
      }
    }
        
    if (isRowGroup == true) {
      return "rgba(225, 225, 225, 1)";
    }
    else if (isRowName == true) {
      return this.columnGroups[1].subsets[0].cellBorderColor;
    }
    else {
      var thisColNumber = 0;
      for (var i = 0; i < this.columnGroups.length; ++i) {
        var group = this.columnGroups[i];
        for (var j = 0; j < group.subsets.length; ++j) {
          if (thisColNumber == cellNumber) {
            var color = group.subsets[j].cellBorderColor;
            if (color != null) {
              return color;
            }
          }
          if (thisColNumber == (cellNumber + 1)) {
            var rightColor =  group.subsets[j].cellBorderColor;
            if (rightColor != null) {
              return rightColor;
            }
            else {
              return "rgba(225, 225, 225, 1)";
            }
          }
          thisColNumber = thisColNumber + 1;
        }
      }
    }
        
    return "rgba(225, 225, 225, 1)"; 
  }

  getRowCellSpan(rowNumber: number, cellNumber: number) : number {
    if (cellNumber != 0) {
      return 1;
    }
    if (this.rowGroups != null) {
      var thisRowNumber = 0;
            
      for (var i = 0; i < this.rowGroups.length; ++i) {
        var group = this.rowGroups[i];
        for (var j = 0; j < group.subsets.length; ++j) {
          if (thisRowNumber == rowNumber) {
            return group.subsets.length;
          }
          thisRowNumber = thisRowNumber + 1;
        }
      }
    }
    return 1;
  }
    
  getRowCellWeight(rowNumber: number, cellNumber: number) : number {
        
    // Decide if row group, row name, or data

    var isRowGroup = false; //matters if cellNumber is 0 or 1
    var isRowName = false; //matter if cellNumber is 0 or 1
        
    if ((cellNumber == 0) && (this.rowGroups != null)){
      if ((this.rowGroups.length > 1) || ((this.rowGroups.length == 1) &&  (this.rowGroups[0].displayName != null))) {
        isRowGroup = true;
      }
      else {
        isRowName = true;
      }
    }
        
    if (cellNumber == 1) {
      if ((this.rowGroups.length >= 1) || ((this.rowGroups.length == 1) &&  (this.rowGroups[0].displayName == null))) {
        isRowName = true;
      }
    }
        
    if (isRowGroup == true) {
      return 900;
    }
    else if (isRowName == true) {
      return 900;
    }
    
    return 400;
  }

  getRowCellTransform(rowNumber: number, cellNumber: number) : string {
    if (cellNumber != 0) {
      return null;
    }
    if (this.rowGroups != null) {
      var thisRowNumber = 0;
            
      for (var i = 0; i < this.rowGroups.length; ++i) {
        var group = this.rowGroups[i];
        for (var j = 0; j < group.subsets.length; ++j) {
          if (thisRowNumber == rowNumber) {
            return "rotate(-90deg)";
          }
          thisRowNumber = thisRowNumber + 1;
        }
      }
    }
    
    return null;
  }
    
  checkDrawCell(rowNumber: number, cellNumber: number) : boolean {
    //Don't draw cell if part of rowGroup expanded cell but not the first one
    if (cellNumber != 0) {
      return true;
    }
    if (this.rowGroups != null) {
      var thisRowNumber = 0;
            
      for (var i = 0; i < this.rowGroups.length; ++i) {
        var group = this.rowGroups[i];
        if (thisRowNumber == rowNumber) {
          return true;
        }
        for (var j = 0; j < group.subsets.length; ++j) {
          thisRowNumber = thisRowNumber + 1;
        }
      }
    }
    
    return false;
  }
    
}    

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
