
export class TableSubset {
  public displayName: string;
  public headFillColor: string;
  public headTextColor: string;
  public cellBorderColor: string; 
  public cellFillColor: string; 
}