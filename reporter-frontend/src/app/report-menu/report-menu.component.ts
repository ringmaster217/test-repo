import { Component, OnInit } from '@angular/core';
import { ReporterService } from '../reporter.service';

import { ProtoReportList } from '../protoreportlist';
import { ProtoReportTypeList } from '../protoreporttypelist'; 

@Component({
  selector: 'app-report-menu',
  templateUrl: './report-menu.component.html',
  styleUrls: ['./report-menu.component.css']
})
export class ReportMenuComponent implements OnInit {

  private reportGroups: ProtoReportTypeList[];

  constructor() { }
    
    
  ngOnInit() { }

}
