import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { LineGraphComponent } from './line-graph/line-graph.component';
import { OptionsComponent } from './options/options.component';
import { TableComponent } from './table/table.component';
import { ReportComponent } from './report/report.component';
import { ReportMenuComponent } from './report-menu/report-menu.component';

import { ReporterService } from './reporter.service';
import { RoutingModule } from './routing.module';
import { SideBarComponent } from './side-bar/side-bar.component';

@NgModule({
  declarations: [
    AppComponent,
    LineGraphComponent,
    OptionsComponent,
    TableComponent,
    ReportComponent,
    ReportMenuComponent,
    ReportComponent,
    SideBarComponent
  ],
  imports: [
    BrowserModule,
    ChartsModule,
    HttpModule,
    RoutingModule
  ],
  providers: [
    ReporterService
  ],
  bootstrap: [AppComponent],
  entryComponents: [LineGraphComponent, TableComponent, OptionsComponent]
})
export class AppModule { }
