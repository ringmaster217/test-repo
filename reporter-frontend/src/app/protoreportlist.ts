import { ProtoReportTypeList } from './protoreporttypelist';

export class ProtoReportList {
  public amount: string;
  public types: number;
  public baseUrl: string;
  public reports: ProtoReportTypeList[];
}