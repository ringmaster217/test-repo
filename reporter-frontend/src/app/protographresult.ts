import { ProtoLine } from './protoline';

export class ProtoGraphResult {
  public LinesNumber: number;
  public Labels: string[];
  public GraphLines: ProtoLine[];
  public TotalGraphLines: ProtoLine[];
  public GraphTitle: string;
  public XAxis: string;
  public YAxis: string;
  public Aggregation: string;
  public Timeframe: string;

  getPointIndex(pointLabel: string) : number {
    if (this.Labels == null) { 
      return -1;
    }
    for (var i = 0; i < this.Labels.length; ++i) {
      if (this.Labels[i] == pointLabel) {
        return i; 
      }
    }
    return -1;
  }
  
}