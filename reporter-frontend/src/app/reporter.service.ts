import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';

@Injectable()
export class ReporterService {

  private headers = new Headers({'Content-Type': 'application/json', 'Access-Control-Allow-Origin' : 'http://localhost:4200'});

  constructor(private http: Http) { }

  getReport(serviceUrl: string): Promise<any> {
    return this.http.get(serviceUrl)
                .toPromise()
                .then(response => response.json())
                .catch(this.handleError); 
  }

   private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }


}