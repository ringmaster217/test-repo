export class ProtoLine {
  public LineName: string;
  public YValues: Array<number>;
  public KeyName: string;
  public KeyColName: string;
  public LineType: string;
  public ShowPoints: boolean;
}