import { Preset } from './preset';

export class PresetGroup {
  public displayName: string;
  public presets: Preset[];
}