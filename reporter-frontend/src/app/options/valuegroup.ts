import { OptionValue } from './optionvalue';

export class ValueGroup {
  public displayName;
  public urlValue;
  public optionValues: OptionValue[];
}