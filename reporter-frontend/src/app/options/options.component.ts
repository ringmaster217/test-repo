import { Component, EventEmitter, Input, Output, OnInit } from '@angular/core';
import { ChangeDetectionStrategy, SimpleChanges, OnChanges } from '@angular/core';
import { ReporterService } from '../reporter.service';
import { Headers, Http } from '@angular/http';
import { OptionValue } from './optionvalue';

import { OptionMenu } from './optionmenu'; 

import { TierGroup } from './tiergroup';
import { ValueGroup } from './valuegroup';

import { Option } from './option'; 

import { OptionResult } from './optionresult';

@Component({
  selector: 'app-options',
  templateUrl: './options.component.html',
  styleUrls: ['./options.component.css']
})
export class OptionsComponent implements OnInit, OnChanges {

  testing: string;
  
  @Input()
  optionMenu: OptionMenu;
  loadingChart: boolean;
  
  private optionValueUrls: string[];
  private showGroups: string[];
  private setDefaults: boolean;
  private showDropdowns: string[];
  
  

  @Output()
  optionsEmitter: EventEmitter<string> = new EventEmitter<string>();
  
  constructor(private reporterService: ReporterService) { 
    this.optionValueUrls = new Array<string>();
    this.showGroups = new Array<string>();
    this.setDefaults = false;
    this.showDropdowns = new Array<string>(); 
  }
 
  ngOnInit() { 
    this.setUpMenuOptions();
    this.setUpDefaults();
  }
  
  ngOnChanges(changes: SimpleChanges) {
    this.reSetUp();
  }
  
  reSetUp() {
    if (this.optionMenu != null) {
      this.loadingChart = false;
   //   this.testing = this.testing + " STARTING resetup";
      this.optionValueUrls = new Array<string>(0);
      this.showGroups = new Array<string>(0);
      this.setDefaults = false;
      this.showDropdowns = new Array<string>(0);
      
      this.setUpMenuOptions();
      this.setUpDefaults();
    //  this.testing = this.testing + " OVER resetup";
    }
  }
  
  private setUpMenuOptions() {
    for (var g = 0; g < this.optionMenu.groups.length; ++g) {
      for (var o = 0; o < this.optionMenu.groups[g].options.length; ++o) {
        var option = new Option(this.optionMenu.groups[g].options[o]);
        var test = option.getSingleDefaultName();
        this.optionMenu.groups[g].options[o] = option;
      }
    } 
  }
  
  private setUpDefaults() {
    for (var g = 0; g < this.optionMenu.groups.length; ++g) {
      for (var o = 0; o < this.optionMenu.groups[g].options.length; ++o) {
        var option = new Option(this.optionMenu.groups[g].options[o]);
        if ((option.defaultSelected != null) && (this.showOption(option.optionID))) {
          for (var d = 0; d < option.defaultSelected.length; ++d) {
            if (this.optionValueUrls.indexOf(option.defaultSelected[d]) == -1) {
              this.optionValueUrls.push(option.defaultSelected[d]);
            //  this.testing = this.testing + " ADDED DEFAULT " + d + " " + option.defaultSelected[d];
              var defaultShowGroups = option.getShowGroups(option.defaultSelected[d]);
              if (defaultShowGroups != null) {
                for (var s = 0; s < defaultShowGroups.length; ++s) {
                  if (this.showGroups.indexOf(defaultShowGroups[s]) == -1) {
                    this.showGroups.push(defaultShowGroups[s]); 
                  }
                }
              }
            }
          }
        }
      }
    }
    
    this.testing = JSON.stringify(this.optionMenu);

    this.submitOptions();
  }
  
  
  
  private getDefaultName(optionID: string) : string {
    var option = this.findOptionByID(optionID);
    if (option == null) {
      return null;
    }
    //check for already selected values
    for (var i = 0; i < this.optionValueUrls.length; ++i) {
      if (this.optionValueUrls[i].indexOf( option.getUrlValue(null, null, null) ) == 0) {
        return option.getDisplayNames(this.optionValueUrls[i])[0]; 
      }
    }
    
    return option.getSingleDefaultName();
  }
  
  private getUrlValue(optionID: string, tier: string, vgroup: string, choice: string) {
    var option = this.findOptionByID(optionID);
    if (option == null) {
      return null;
    }
    return  option.getUrlValue(tier, vgroup, choice);
  }
  
  private findOptionByID(optionID: string) : Option {
    for (var g = 0; g < this.optionMenu.groups.length; ++g) {
      for (var o = 0; o < this.optionMenu.groups[g].options.length; ++o) {
        var option = new Option(this.optionMenu.groups[g].options[o]);
        if (option.optionID == optionID) {
          return option;
        }
      }
    } 
    return null;
  }
  
  private findOptionsByParameter(urlParameter: string) : Option[] {
    var options = new Array<Option>();
    for (var g = 0; g < this.optionMenu.groups.length; ++g) {
      for (var o = 0; o < this.optionMenu.groups[g].options.length; ++o) {
        var option = new Option(this.optionMenu.groups[g].options[o]);
        if (option.urlParameter == urlParameter) {
           options.push(option);
        }
      }
    } 
    return options;
  }
  
  
  private checkOption(urlValue: string) : boolean {
    var index = this.optionValueUrls.indexOf( urlValue );
    if (index == -1) {
      return false;
    }
    return true;
  }

  private setOption(urlParameter: string, urlValue: string, multiAllowed: boolean) {
    
    if (multiAllowed == true) {
      if (this.checkOption(urlValue) == true) {
        var index = this.optionValueUrls.indexOf( urlValue );
        this.optionValueUrls.splice(index, 1);
        this.optionValueUrls = this.optionValueUrls.slice();
      }
      else {
        this.optionValueUrls.push( urlValue );
      }
    }
    else {
      
      //could be antecedent option -> need to clear old consequent option values & showGroups, set default values & showGroups for new consequent option
      var options = this.findOptionsByParameter(urlParameter);
      
      if (options.length == 1) {
        var option = options[0];
        for (var i = 0; i < this.optionValueUrls.length; ++i) {
          if (this.optionValueUrls[i].indexOf( option.getUrlValue(null, null, null) ) == 0) { 
            this.optionValueUrls.splice(i, 1);
            this.optionValueUrls = this.optionValueUrls.slice();
            i = i - 1;
          }
        }
        
        if (option.consequentOptions != null) {
          var newConseq = null;
          
          for (var i = 0; i < option.consequentOptions.length; ++i) {
            var conseqOptionID = option.consequentOptions[i];
            var conseqOption = this.findOptionByID(conseqOptionID);
            
            if (conseqOption != null) {
              //remove old conseq values
              for (var j = 0; j < this.optionValueUrls.length; ++j) {
                if (this.optionValueUrls[j].indexOf( conseqOption.getUrlValue(null, null, null) ) == 0 ) {
                  this.optionValueUrls.splice(j, 1);
                  this.optionValueUrls = this.optionValueUrls.slice();
                  j = j - 1;
                }
              }
              //remove old conseq show groups
              for (var k = 0; k < this.showGroups.length; ++k) {
                if (this.showGroups[k].indexOf( conseqOption.getShowGroup(null, null) ) == 0) {
                  this.showGroups.splice(k, 1);
                  this.showGroups = this.showGroups.slice();
                  k = k - 1;
                }
              }
              
              if (conseqOptionID == urlValue) {
                //found new consequent option
                newConseq = conseqOption;
              }
              
            }
          }
          
          if (newConseq != null) {
            //set default for conseq option
            if (newConseq.defaultSelected != null) {
              for (var l = 0; l < newConseq.defaultSelected.length; ++l) {
                if (this.optionValueUrls.indexOf(newConseq.defaultSelected[l]) == -1) {
                  this.optionValueUrls.push(newConseq.defaultSelected[l]);
                  var defaultShowGroups = newConseq.getShowGroups(newConseq.defaultSelected[l]);  
                  if (defaultShowGroups != null) {
                    for (var s = 0; s < defaultShowGroups.length; ++s) {
                      if (this.showGroups.indexOf(defaultShowGroups[s]) == -1) {
                        this.showGroups.push(defaultShowGroups[s]); 
                      }
                    }
                  }
                } 
              }
            }
          }
        }
      }
      this.optionValueUrls.push( urlValue );
    }
    
    this.loadingChart = true;
    this.submitOptions();
  }
  
  private showOption(optionID: string) : boolean {
    for (var g = 0; g < this.optionMenu.groups.length; ++g) {
      for (var o = 0; o < this.optionMenu.groups[g].options.length; ++o) {
        var option = new Option(this.optionMenu.groups[g].options[o]);
        if (option.consequentOptions != null) {
          if (option.consequentOptions.indexOf(optionID) != -1) {
            if (this.optionValueUrls.indexOf(optionID) != -1) {
              return true;
            }
            return false;
          }
        }
      } 
    }
    return true;
  }
  
  
  private checkShowDropdown(optionID: string) : boolean {
    if (this.showDropdowns.indexOf(optionID) == -1) {
      return false;
    }
    return true;
  }

  private changeShowDropdown(optionID: string) {
    if (this.showDropdowns.indexOf(optionID) == -1) {
      this.showDropdowns.push(optionID);
      return;
    }
    var index = this.showDropdowns.indexOf(optionID);
    this.showDropdowns.splice(index, 1);
    this.showDropdowns = this.showDropdowns.slice();
  }
  

  
  private checkShowGroup(groupName: string) : boolean {
    if (groupName == "") {
      return true;
    }
    if (this.showGroups.indexOf(groupName)  == -1) {
      return false;
    }
    return true;
  }
  
  private formatShowGroup(urlParameter: string, tierGroupUrl: string, valueGroupUrl: string) : string {
    if (tierGroupUrl == null) {
      return "";
    }
    if (valueGroupUrl == null) {
      return urlParameter + "="  + tierGroupUrl;
    }
    return urlParameter + "=" + tierGroupUrl + "-" + valueGroupUrl;
  }
  
  private setShowGroup(groupName: string) {
    if (this.checkShowGroup(groupName) == false) {
      this.showGroups.push(groupName);
      this.showGroups = this.showGroups.slice(); 
    }
    else {
      var index = this.showGroups.indexOf(groupName);
      this.showGroups.splice(index, 1);
      this.showGroups = this.showGroups.slice();
    }
  }

 
  
  private usePreset(optionID: string, optionUrls: string[]) {
    var option = this.findOptionByID(optionID);
     
    if (option != null) {
      //remove old values 
      for (var j = 0; j < this.optionValueUrls.length; ++j) {
        if (this.optionValueUrls[j].indexOf(  option.getUrlValue(null, null, null) ) == 0) {
          this.optionValueUrls.splice(j, 1);
          this.optionValueUrls = this.optionValueUrls.slice();
          j = j - 1;
        }
      }
      //remove old show groups
      for (var k = 0; k < this.showGroups.length; ++k) {
        if (this.showGroups[k].indexOf( option.getShowGroup(null, null) ) == 0) {
          this.showGroups.splice(k, 1);
          this.showGroups = this.showGroups.slice();
          k = k - 1;
        }
      }
      //add new values and show groups
      for (var l = 0; l < optionUrls.length; ++l) {
        this.optionValueUrls.push(option.urlParameter + "=" + optionUrls[l]);
        var newShowGroups = option.getShowGroups(option.urlParameter + "=" + optionUrls[l]);
        for (var m = 0; m < newShowGroups.length; ++m) {
          if (this.showGroups.indexOf(newShowGroups[m]) == -1)  {
            this.showGroups.push(newShowGroups[m]);
          }
        }
      }
    }

    this.loadingChart = true;
    this.submitOptions();
  }

  
  private submitOptions () { 
    var query = this.optionMenu.urlStub;
    for (var i = 0; i < this.optionValueUrls.length; ++i) {
      if (i != 0) {
        query = query + "&";
      }
      query = query + this.optionValueUrls[i];
    }
    
    //this.testing = this.testing + " " + query;
    this.reporterService.getReport(query).then(response => this.emitOption( JSON.stringify(response) ));

  }
  
  private emitOption(responseString: string) {
  //  this.testing = this.testing + " in emitOption";
    this.optionsEmitter.emit(JSON.stringify( new OptionResult (responseString)));
    this.loadingChart = false;
  }
  
}
