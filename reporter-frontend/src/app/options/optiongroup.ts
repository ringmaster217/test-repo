import { Option } from  './option'; 

export class OptionGroup {
  public displayName: string;
  public options: Option[];  
}                                                                                                                                                                             