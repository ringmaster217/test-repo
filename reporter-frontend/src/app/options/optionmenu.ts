import { OptionGroup } from './optiongroup';
  
export class OptionMenu {
  public displayTitle: string;
  public urlStub: string;
  public groups: OptionGroup[]; 

  constructor () {}

}