import { PresetGroup } from './presetgroup';
import { TierGroup } from './tiergroup';

export class Option {
  public optionID: string;
  public displayName: string;

  public multiAllowed: boolean;
  public displayStyle: number;

  public urlParameter: string;
  public urlConcatenator: string; 
  public defaultSelected: string[];
  public consequentOptions: string[]; // selectUrl==optionID

  public presetGroup: PresetGroup;
  public tierGroups: TierGroup[];

  constructor(oldOption: Option) {
    if (oldOption != null) {
      this.optionID = oldOption.optionID.slice();
      
      if (oldOption.displayName == null) {
        this.displayName = null;
      }
      else {
        this.displayName = oldOption.displayName.slice();
      }
      
      if (oldOption.defaultSelected == null) {
        this.defaultSelected = null;
      }
      else {
        this.defaultSelected = oldOption.defaultSelected.slice();
      }

      this.presetGroup = oldOption.presetGroup; 
      this.multiAllowed = oldOption.multiAllowed;
      this.displayStyle = oldOption.displayStyle;
      
      if (oldOption.urlParameter == null) {
        this.urlParameter = null;
      }
      else {
        this.urlParameter = oldOption.urlParameter.slice();
      }
      
      this.tierGroups = oldOption.tierGroups.slice();
      
      if (oldOption.urlConcatenator  == null) {
        this.urlConcatenator = null;
      }
      else {
        this.urlConcatenator = oldOption.urlConcatenator.slice();
      }
       
      
      if (oldOption.consequentOptions == null) {
        this.consequentOptions == null;
      }
      else {
        this.consequentOptions = oldOption.consequentOptions.slice(); 
      }
    }
  }

  getUrlValue(tier: string, vgroup: string, choice: string) : string {
    if (tier == null) {
      if (choice == null) {
        return this.urlParameter + "=";
      }
      return this.urlParameter + "=" + choice;
    }
    if (vgroup == null) {
      return this.urlParameter + "=" + tier + this.urlConcatenator + choice;
    }
    return this.urlParameter + "=" + tier + this.urlConcatenator + vgroup + this.urlConcatenator + choice;
  }
  
  getShowGroup(tier: string, vgroup: string) : string {
    if (vgroup == null) {
      if (tier == null) {
        return this.optionID + "=";
      }
      return this.optionID + "=" + tier;
    }
    return this.optionID + "=" + tier + this.urlConcatenator + vgroup;
  }
  
  getShowGroups(urlValue: string) : string[] {
    var groups = [];
    for (var t = 0; t < this.tierGroups.length; ++t) {
      var tier = this.tierGroups[t];
      for (var v = 0; v < tier.valueGroups.length; ++v) {
        var vgroup = tier.valueGroups[v];
        for (var c = 0; c < vgroup.optionValues.length; ++c) {
          var choice = vgroup.optionValues[c];
          if (this.getUrlValue(tier.urlValue, vgroup.urlValue, choice.urlValue) == urlValue) {
             if (tier.urlValue != null) {
               groups.push( this.getShowGroup(tier.displayName, null) );
               if (vgroup.urlValue != null) {
                 groups.push( this.getShowGroup(tier.displayName, vgroup.displayName) );
               }
             }
           }
        }
      }
    }
    return groups;
  }
  
  getSingleDefaultName() : string {
    if ((this.defaultSelected == null) || (this.defaultSelected[0] == null) || (this.defaultSelected[0] == "")) {
      return "";
    }
    
    for (var t = 0; t < this.tierGroups.length; ++t) {
      var tier = this.tierGroups[t];
      for (var v = 0; v < tier.valueGroups.length; ++v) {
        var vgroup = tier.valueGroups[v];
        for (var c = 0; c < vgroup.optionValues.length; ++c) {
          var choice = vgroup.optionValues[c];
          if (this.defaultSelected[0] == this.getUrlValue(tier.urlValue, vgroup.urlValue, choice.urlValue)) {
            return choice.displayName;
          }
        }
      }
    } 
    
    return null;
  }
  
  getDisplayNames (urlValue) : string[] {
    var names = [];
    for (var t = 0; t < this.tierGroups.length; ++t) {
      var tier = this.tierGroups[t];
      for (var v = 0; v < tier.valueGroups.length; ++v) {
        var vgroup = tier.valueGroups[v];
        for (var c = 0; c < vgroup.optionValues.length; ++c) {
          var choice = vgroup.optionValues[c];
          if (this.getUrlValue(tier.urlValue, vgroup.urlValue, choice.urlValue) == urlValue) {
            names.push(choice.displayName);
          }
        }
      }
    }
    return names;
  }
  
}


