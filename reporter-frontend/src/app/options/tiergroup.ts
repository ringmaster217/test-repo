import { ValueGroup } from './valuegroup';

export class TierGroup {
  public displayName;
  public urlValue;
  public valueGroups: ValueGroup[];
}