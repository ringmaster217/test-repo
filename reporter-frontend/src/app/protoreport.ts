export class ProtoReport {
  public displayName: string;
  public identifier: string;
  public optionsUrl: string;
}