import { KeyItem } from './keyitem';

export class KeyGroup {
  public displayName: string;
  public items: KeyItem[];

  public addLine(lineDisplayName: string, lineColor: string, lineType: string) {
    if ( (lineDisplayName != null) && (lineDisplayName != "")){ 
      if (this.items == null) {
        this.items = new Array<KeyItem>(1);
        var newItem = new KeyItem();
        newItem.displayName = lineDisplayName; 
        newItem.color = lineColor;
        newItem.lineType = lineType;
        this.items[0] = newItem;
      }
      else {
        var newItem = new KeyItem();  
        newItem.displayName = lineDisplayName;
        newItem.color = lineColor;
        newItem.lineType = lineType;
        this.items.push(newItem);
      }
    }
  }
   
}