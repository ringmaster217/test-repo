import { ProtoGraphResult } from '../protographresult'; 
import { KeyGroup } from './keygroup'; 

export class LineGraphData {

  private graphTitle = "Graph";
  private chartType = 'line'; 
  private legend = false;
  
  private chartOptions: any = {
    responsive: true,
    scales: {
        yAxes: [{
            ticks: {
                max: 1000,
                min: 0,
                stepSize: 100
            }
        }]
    }
  };
  
  private chartColors : Array<any>;

  /* Color: red, light blue, orange, brown, purple, green, yellow,  pink */
  private static chartColorsStore: Array<any> = [                                                                        
     { // Red
       backgroundColor: 'rgba(0,0,0,0)',
       borderColor: 'rgba(255, 96, 96, 1)',
       pointRadius: '0',
       pointBorderWidth: '0',
       pointBackgroundColor: 'rgba(255, 96, 96, 1)',
       pointBorderColor: 'rgba(0, 0, 0, 0)',
       pointHoverRadius: '5',
       pointHoverBorderWidth: '0',
       pointHoverBackgroundColor: 'rgba(255, 96, 96, 1)',
       pointHoverBorderColor: 'rgba(250, 250, 250, 0)'
     },
     { // Light Blue
         backgroundColor: 'rgba(0,0,0,0)',
         borderColor: 'rgba(96, 214, 255, 1)',
         pointRadius: '0',
         pointBorderWidth: '0',
         pointBackgroundColor: 'rgba(96, 214, 255, 1)',
         pointBorderColor: 'rgba(0, 0, 0, 0)',
         pointHoverRadius: '5',
         pointHoverBorderWidth: '0',
         pointHoverBackgroundColor: 'rgba(96, 214, 255, 1)',
         pointHoverBorderColor: 'rgba(250, 250, 250, 0)'
     },
     { // Orange
         backgroundColor: 'rgba(0,0,0,0)',
         borderColor: 'rgba(255, 200, 96, 1)',
         pointRadius: '0',
         pointBorderWidth: '0',
         pointBackgroundColor: 'rgba(255, 200, 96, 1)',
         pointBorderColor: 'rgba(0, 0, 0, 0)',
         pointHoverRadius: '5',
         pointHoverBorderWidth: '0',
         pointHoverBackgroundColor: 'rgba(255, 200, 96, 1)',
         pointHoverBorderColor: 'rgba(250, 250, 250, 0)'
     },
     { // Brown
         backgroundColor: 'rgba(0,0,0,0)',
         borderColor: 'rgba(156, 102, 0, 1)',
         pointRadius: '0',
         pointBorderWidth: '0',
         pointBackgroundColor: 'rgba(156, 102, 0, 1)',
         pointBorderColor: 'rgba(0, 0, 0, 0)',
         pointHoverRadius: '5',
         pointHoverBorderWidth: '0',
         pointHoverBackgroundColor: 'rgba(156, 102, 0, 1)',
         pointHoverBorderColor: 'rgba(250, 250, 250, 0'
     },
     { // Purple
         backgroundColor: 'rgba(0,0,0,0)',
         borderColor: 'rgba(131, 96, 255, 1)',
         pointRadius: '0',
         pointBorderWidth: '0',
         pointBackgroundColor: 'rgba(131, 96, 255, 1)',
         pointBorderColor: 'rgba(0, 0, 0, 0)',
         pointHoverRadius: '5',
         pointHoverBorderWidth: '0',
         pointHoverBackgroundColor: 'rgba(131, 96, 255, 1)',
         pointHoverBorderColor: 'rgba(250, 250, 250, 0)'
     },
     { // Green
         backgroundColor: 'rgba(0,0,0,0)',
         borderColor: 'rgba(119, 233, 63, 1)',
         pointRadius: '0',
         pointBorderWidth: '0',
         pointBackgroundColor: 'rgba(119, 233, 63, 1)',
         pointBorderColor: 'rgba(0, 0, 0, 0)',
         pointHoverRadius: '5',
         pointHoverBorderWidth: '0',
         pointHoverBackgroundColor: 'rgba(119, 233, 63, 1)',
         pointHoverBorderColor: 'rgba(250, 250, 250, 0)'
     },
     { // Yellow
         backgroundColor: 'rgba(0,0,0,0)',
         borderColor: 'rgba(225, 233, 63, 1)',
         pointRadius: '0',
         pointBorderWidth: '0',
         pointBackgroundColor: 'rgba(225, 233, 63, 1)',
         pointBorderColor: 'rgba(0, 0, 0, 0)',
         pointHoverRadius: '5',
         pointHoverBorderWidth: '0',
         pointHoverBackgroundColor: 'rgba(225, 233, 63, 1)',
         pointHoverBorderColor: 'rgba(250, 250, 250, 0)'
     },
     { // Pink
         backgroundColor: 'rgba(0,0,0,0)',
         borderColor: 'rgba(221, 96, 255, 1)',
         pointRadius: '0',
         pointBorderWidth: '0', 
         pointBackgroundColor: 'rgba(221, 96, 255, 1)',
         pointBorderColor: 'rgba(0, 0, 0, 0)',
         pointHoverRadius: '5',
         pointHoverBorderWidth: '0',
         pointHoverBackgroundColor: 'rgba(221, 96, 255, 1)',
         pointHoverBorderColor: 'rgba(250, 250, 250, 0)'
     }  
  ];

  private chartData: Array<any> = [
    { data: [300, 600, 260, 700], label: 'Account A'},
    { data: [120, 455, 100, 340], label: 'Account B'},
    { data: [45, 67, 800, 500], label: 'Account C'}
  ];

  private chartLabels: Array<any> = ['January', 'February', 'March', 'April'];

  chartKey: KeyGroup[]; 
  
  constructor (graphData: ProtoGraphResult) {
    this.graphTitle = graphData.GraphTitle;
    this.chartLabels = graphData.Labels;
    
    var lines = graphData.LinesNumber;
    
    this.chartOptions = {
      responsive: true,
      pointHitDetectionRadius: '1',
      elements: {
        line: {
          tension: 0
        }
      },
      tooltips: { 
        mode: 'nearest',
        intersect: false,
        enabled: false,
        custom: function(tooltipModel) {
          // Tooltip Element
          var tooltipEl = document.getElementById('chartjs-tooltip');

          // Create element on first render
          if (!tooltipEl) {
            tooltipEl = document.createElement('div');
            tooltipEl.id = 'chartjs-tooltip';
            tooltipEl.innerHTML = "<table></table>";
            document.body.appendChild(tooltipEl);
          }

          // Hide if no tooltip
          if (tooltipModel.opacity === 0) {
            tooltipEl.style.opacity = '0';
            return;
          }

          // Set caret Position
          tooltipEl.classList.remove('above', 'below', 'no-transform');
          if (tooltipModel.yAlign) {
            tooltipEl.classList.add(tooltipModel.yAlign);
          } else {
            tooltipEl.classList.add('no-transform');
          } 
          
          function getBody(bodyItem) {
            return bodyItem.lines;
          }

          // Set Text
          if (tooltipModel.body) {
            var titleLines = tooltipModel.title || [];
            var bodyLines = tooltipModel.body.map(getBody);

            var innerHtml = '<thead>';

          /*  titleLines.forEach(function(title) {
              //  var titleStyle = "text-align:left; font-weight: 700; font-size: 12px; font-family: 'Source Sans Pro', sans-serif;";
              //  var slashStyle = "color: rgba(137, 137, 137, 1)";
              //just practicing style
              //  var weekNumber = title.toString().split('/')[0];
              //  var term = title.toString().split('/')[1];
               // innerHtml += '<tr><th style="' + titleStyle + '">' + term+ '<span style="' + slashStyle +'">' + ' / </span>' + 'Week ' + weekNumber + '</th></tr>';
              //  innerHtml += '<tr><th style="' + titleStyle + '">' + tooltipModel.body + '</th></tr>';
            });
            innerHtml += '</thead><tbody>'; */

            bodyLines.forEach(function(body, i) { //only one bodyLine for use
              if (i == 0) {
                var colors = tooltipModel.labelColors[i];
                
                var titleStyle = "text-align:left; font-weight: 700; font-size: 10px; font-family: 'Source Sans Pro', sans-serif; padding-top: 2px;";
                var slashStyle = "color: rgba(137, 137, 137, 1)";
                var lineNameStyle = "padding-left: 4px; font-weight: 700; font-size: 10px; font-family: 'Source Sans Pro', sans-serif;";
                var dataStyle = "font-weight: 900; font-size: 16px; font-family: 'Source Sans Pro', sans-serif; color: " + colors.backgroundColor;
                
                var titleLine = "never wrote title line";
                
                var lineKeyName = "LINEKEYNAME";
                
                var circleStyle = 'background:' + colors.backgroundColor;
                circleStyle += '; border-radius: 4px; width: 8px; height: 8px; display: inline-block';
                var circle= '<span style="' + circleStyle + '"></span>';
                var circleContainer = '<div>' + circle + '</div>';
                
                
                if ((graphData.Timeframe.toLowerCase() == "year") || (graphData.Timeframe.toLowerCase() == "term")) {
                  var weekNumber = tooltipModel.title.toString().split('/')[0];
                  var term = tooltipModel.title.toString().split('/')[1];
                  if (term == null) {
                    term = weekNumber;
                    if ((weekNumber == 'Fall') || (weekNumber == "Winter") ) {
                      weekNumber = '9';
                    }
                    else {
                      weekNumber = '5';
                    }
                  }
                  
                  var lineID = body.toString().split(":")[0];
                  var lineName = lineID.split("/")[0];
                  lineKeyName = lineID.split("/")[1];
                  
                  titleLine = lineName + '<span style="' + slashStyle + '"> / </span>Week ' + weekNumber;
                }
                else if (graphData.Timeframe.toLowerCase() == "week") {
                  var termName = body.toString().split(" / ")[0];
                  var bodyLeft = body.toString().split(" / ")[1];
                  var weekName = bodyLeft.split("/")[0];
                  bodyLeft = bodyLeft.split("/")[1];
                  bodyLeft = bodyLeft.split(":")[0];
                  lineKeyName = bodyLeft.split(" - ")[0];
                  
                  titleLine = termName + '<span style="' + slashStyle + '"> / </span>'
                        + weekName + '<span style="' + slashStyle + '"> / </span>'
                        + tooltipModel.title;
                }

                innerHtml += '<tr><th style="' + titleStyle + '">' + titleLine + '</th></tr>';
                innerHtml += '</thead><tbody>';
                
                if (graphData.Timeframe.toLowerCase() == "term") {
                  lineKeyName = lineKeyName.split("-")[0];
                }
   
                var styledLineName = '<span style="' + lineNameStyle + '">' + lineKeyName + '</span>'; 
                var keyLine = "<div style='display: flex'>" + circleContainer + styledLineName + "</div>";
                
                var number = body.toString().split(":")[1];
                var formatNumber; 
                
                if (graphData.Aggregation.toLowerCase() == "percentage") {
                  formatNumber = Math.round(number) + "%";
                    
                  //find point index
                  var index = -1;
                  for (var j = 0; j < graphData.Labels.length; ++j) {
                    if (graphData.Timeframe.toLowerCase() == "year") {
                      if (graphData.Labels[j] == weekNumber + "/"  + term) {
                        index = j;
                      }
                    }
                    else if (graphData.Timeframe.toLowerCase() == "term") {
                      if (graphData.Labels[j] == tooltipModel.title) {
                        index = j;
                      }
                    }
                    else if (graphData.Timeframe.toLowerCase() == "week") {
                      if (graphData.Labels[j] == tooltipModel.title) {
                        index = j;
                      }
                    }
                  }
                  
                  //find predicted total line name
                  var totalLineName;
                  if (graphData.Timeframe.toLowerCase() == "year") {
                    totalLineName = lineName;
                  } 
                  else if (graphData.Timeframe.toLowerCase() == "term") {
                    totalLineName = lineName;
                  }
                  else if (graphData.Timeframe.toLowerCase() == "week") {
                    totalLineName = termName + " / " + weekName;
                  }
                  
                  //if percent, find total # 
                  var totalNumber = -1;
                  var totalLine;
                  if (graphData.TotalGraphLines != null) {
                    for (var k = 0; k < graphData.TotalGraphLines.length; ++k) {
                      totalLine = graphData.TotalGraphLines[k];
                      if (totalLine.LineName == totalLineName) {
                        if (index == -1) {
                          totalNumber = -4;
                        }
                        else {
                          totalNumber = totalLine.YValues[index];
                          k = graphData.TotalGraphLines.length;
                        }
                      } 
                    }
                  }
                  if (totalNumber == -1) {
                    totalNumber = -3;
                  }
                }
                else { //graphData.Aggregation.toLowerCase() == rate || cumulative
                  if (number < 1000) {
                    formatNumber = number;
                  } 
                  else if (number < 1000000) {
                    formatNumber = Math.round(number/100)/10 + "k";
                  }
                  else if (number < 1000000000) {
                    formatNumber = Math.round(number/100000)/10 + "M";
                  }
                }
                
                var data;
                
                if (graphData.Aggregation.toLowerCase() == "percentage") {
                  if (totalNumber != -1) {
                    var darkDataStyle = "font-weight: 900; font-size: 16px; font-family: 'Source Sans Pro', sans-serif; color: rgb(37, 37, 37)";
                    var totalColor = "rgb(37, 37, 37)"; //default
                    var totalLineIndex = -1;
                    
                    for (var j = 0; j < graphData.GraphLines.length; ++j) {
                      var line = graphData.GraphLines[j];
                      if (line.LineName == body.toString().split(":")[0]) {
                        totalLineIndex = j;
                      }
                    }
                    if (j != -1) {
                      var realTotalLineIndex = -1;
                      for (var k = 0; k < graphData.GraphLines.length; ++k) {
                        var testLine = graphData.GraphLines[k];
                        if ( (testLine.LineName == totalLine.LineName) 
                            && (testLine.KeyColName == totalLine.KeyColName) 
                            && (testLine.KeyName == totalLine.KeyName)) {
                          realTotalLineIndex = k;
                        }
                      }
                      var loopPosition = realTotalLineIndex % LineGraphData.chartColorsStore.length;
                      var loopNumber = ( realTotalLineIndex - loopPosition ) / LineGraphData.chartColorsStore.length;
                      
                      var baseColor = LineGraphData.chartColorsStore[loopPosition]['borderColor'];
                      var minColor = 0;
                      var maxColor = 255;
                      var colorShift = 35;
                      
                      baseColor = baseColor.slice('rgba('.length, baseColor.length - 1 );
                      var baseRed = parseInt(baseColor.slice(0, baseColor.indexOf(',')));
                      baseColor = baseColor.slice( baseColor.indexOf(',') + 1, baseColor.length );
                      var baseGreen = parseInt(baseColor.slice(0, baseColor.indexOf(',')));
                      baseColor = baseColor.slice( baseColor.indexOf(',') + 1, baseColor.length );
                      var baseBlue = parseInt(baseColor.slice(0, baseColor.indexOf(',')));
                      
                      var colorDiff = colorShift * (loopNumber);
                      
                      var red = baseRed - colorDiff;
                      var green = baseGreen - colorDiff;
                      var blue = baseBlue - colorDiff;
                      
                      if (red < minColor) {
                        red = minColor
                      }
                      if (green < minColor) {
                        green = minColor;
                      }
                      if (blue < minColor) {
                        blue = minColor;
                      }
                      
                      totalColor = "rgba(" + red + ", " + green + ", " + blue + ", 1)";
                    }
                    
                    var totalStyle = "font-weight: 900; font-size: 16px; font-family: 'Source Sans Pro', sans-serif; color: " + totalColor;
                    
                    data = '<span style="' + dataStyle + '">' + formatNumber + '</span>'
                    + '<span style="'  + darkDataStyle + '">' + " of " + '</span>'
                    + '<span style="' + totalStyle + '">' + totalNumber; 
                  }
                  else {
                    data = '<span style="' + dataStyle + '">' + formatNumber + '</span>';
                  }
                }
                else {//graphData.Aggregation.toLowerCase() == rate || cumulative
                  data = '<span style="' + dataStyle + '">' + formatNumber + '</span>';
                }
                
                var dataLine = '<div>' + data + '</div>'; 
                
                if ((body == null) || (body == "")) {
                  innerHtml += '<tr><td>' + "<div>BODY IS NULL / EMPTY</div"+ '</td></tr>';
                }
                else {
                  innerHtml += '<tr><td>' + keyLine + dataLine + '</td></tr>';
                }
              }
            });
            innerHtml += '</tbody>';

            var tableRoot = tooltipEl.querySelector('table');
            tableRoot.innerHTML = innerHtml;
          }

          // `this` will be the overall tooltip
          var position = this._chart.canvas.getBoundingClientRect();
          var canvas = this._chart.canvas;

          // Display styling, position, and font styling
          tooltipEl.style.opacity = '1';
          tooltipEl.style.position = 'absolute';
           // tooltipEl.style.left = Number(canvas.offsetLeft) + Number(tooltipModel.caretX) + 'px';
          tooltipEl.style.top = Number(canvas.offsetTop) + Number(tooltipModel.caretY) + 100 + 'px';
          tooltipEl.style.left = position.left + tooltipModel.caretX + 'px';
         //   tooltipEl.style.top = position.top + tooltipModel.caretY + 'px';
          
          tooltipEl.style.fontFamily = tooltipModel._bodyFontFamily;
          tooltipEl.style.fontSize = tooltipModel.bodyFontSize + 'px';
          tooltipEl.style.fontStyle = tooltipModel._bodyFontStyle;
          tooltipEl.style.padding = (Number(tooltipModel.yPadding) - 5) + 'px ' + tooltipModel.xPadding + 'px';
          
          tooltipEl.style.borderRadius= "0px 5px 5px 5px";
          tooltipEl.style.backgroundColor = "white";
          
          tooltipEl.style['-webkit-box-shadow'] = "0px 2px 10px 1px rgba(0,0,0,0.3)";
          tooltipEl.style['-moz-box-shadow'] = "0px 2px 10px 1px rgba(0,0,0,0.3)";
          tooltipEl.style['box-shadow'] = "0px 2px 10px 1px rgba(0,0,0,0.3)";
        },
      },
      scales: {
        xAxes: [{
          scaleLabel: {
            display: true,
            labelString: graphData.XAxis,
            fontColor: "rgba(37, 37, 37, 1)",
            fontFamily: "'Source Sans Pro', sans-serif",
            fontStyle: "900",
            fontSize: "16"
          },
          gridLines: {
            color: "rgba(225, 225, 225, 1)",
            borderDash: [6, 2],
            lineWidth: 1,
            drawTicks: false
          },
          ticks: {
            padding: 15,
            fontFamily: "'Source Sans Pro', sans-serif",
            fontSize: "12",
            autoSkip: false,
            maxRotation: 0,
            fontColor: "rgba(147, 147, 147,1)",
            min: 0,
            callback:function(label) {
              if (graphData.Timeframe == null) {
                if ((label.split("/")[1] == "Fall") 
                  || (label.split("/")[1] == "Winter") 
                  || (label.split("/")[1] == "Spring") 
                  || (label.split("/")[1] == "Summer")) {
                  if (label == "9/Fall") {
                  return "Fall";
                  }
                  if (label == "9/Winter") {
                    return "Winter";
                  }
                  if (label == "5/Spring") {
                    return "Spring";
                  }
                  if (label == "5/Summer") {
                    return "Summer";
                  }
                  return "";
                }
                return label; 
              }
              
              if (graphData.Timeframe.toLowerCase() == "year") {
                if (label == "9/Fall") {
                  return "Fall";
                }
                if (label == "9/Winter") {
                  return "Winter";
                }
                if (label == "5/Spring") {
                  return "Spring";
                }
                if (label == "5/Summer") {
                  return "Summer";
                }
                return "";
              }
              else if (graphData.Timeframe.toLowerCase() == "term") {
                return label;
              }
              else if (graphData.Timeframe.toLowerCase() == "week") {
                return label;
              }
              else {
                return label;
              }
            }
          }
        }],
        yAxes: [{
          scaleLabel: {
            display: true, 
            labelString: graphData.YAxis,
            fontColor: "rgba(37, 37, 37, 1)",
            fontFamily: "'Source Sans Pro', sans-serif",
            fontStyle: "900",
            fontSize: "16"
          },
          gridLines: {
            drawBorder:false,
            drawOnChartArea: false,
            drawTicks: false
          },
          ticks: {
            fontColor: "rgba(147, 147, 147,1)",
            fontFamily: "'Source Sans Pro', sans-serif",
            fontSize: "12",
            min: 0,
            callback:function(label) {
              if (label == 0) {
                return "0  ";
              }
              if (label < 1000) {
                return label + "  ";
              } 
              if (label < 1000000) {
                return label/1000 + "k  ";
              }
              if (label < 1000000000) {
                return label/1000000 + "M  ";
              } 
            }
          }
        }]
      }
    }; 
    
    this.calcChartColors(lines, graphData);
    this.chartData = [];
    this.chartKey = [];
    
    for (var i = 0; i < lines; ++i) {
      var graphLine = graphData.GraphLines[i];
      if (graphLine.LineType == "dashed") {
        this.chartData.push({ data: graphLine.YValues, label: graphLine.LineName + "/" + graphLine.KeyName, borderWidth: 2, borderDash: [5, 2]});
      }
      else {
        this.chartData.push({ data: graphLine.YValues, label: graphLine.LineName + "/" + graphLine.KeyName, borderWidth: 2});
      }
       
      var lineKeyColName = graphLine.KeyColName;
      if (lineKeyColName == null) {
        if (this.chartKey.length == 0) {
          this.chartKey = new Array<KeyGroup>(1);
          this.chartKey[0] = new KeyGroup;
        }
          this.chartKey[0].addLine(graphLine.LineName, this.chartColors[i].borderColor, graphLine.LineType);    
      }
      else {
        if (this.chartKey.length == 0) {
          this.chartKey = new Array<KeyGroup>(1);
          var newKeyGroup = new KeyGroup;
          newKeyGroup.displayName = lineKeyColName;
          newKeyGroup.addLine(graphLine.KeyName, this.chartColors[i].borderColor, graphLine.LineType);
          this.chartKey[0] = newKeyGroup;
        } 
        else {
          var foundKeyGroup = false;
          for (var j = 0; j < this.chartKey.length; ++j) {
            if (this.chartKey[j].displayName == lineKeyColName) {
              this.chartKey[j].addLine(graphLine.KeyName, this.chartColors[i].borderColor, graphLine.LineType);
              foundKeyGroup = true;
              j = this.chartKey.length + 1;
            }
          }
              
          if (foundKeyGroup == false) {
            var newKeyGroup = new KeyGroup;
            newKeyGroup.displayName = lineKeyColName;
            newKeyGroup.addLine(graphLine.KeyName, this.chartColors[i].borderColor, graphLine.LineType);
            this.chartKey.push(newKeyGroup);
          }
        }
      }
      
    }
    
  }
  
  private calcChartColors(lines: number, graphData: ProtoGraphResult) {
    this.chartColors = [];
      
    for (var i = 0; i < lines; ++i) {
      var line = graphData.GraphLines[i];
      var lineBaseColor = this.calcLineColor(i + 1);
          
      var show = line.ShowPoints;
      if (show == null) {
        show = false;
        var foundOne = false;
        for (var j = 0; j < line.YValues.length; ++j) {
          if ( !(line.YValues[j] == null) ) {
            if (foundOne == false) {
              show = true;
              foundOne = true;
            }
            else {
              show = false;
            }
          }
        }
      }
          
      var lineColors;
      if (show == true) {
        lineColors = {
          backgroundColor: 'rgba(0,0,0,0)',
          borderColor: lineBaseColor,
          pointRadius: '5',
          pointBorderWidth: '0',
          pointBackgroundColor: lineBaseColor,
          pointBorderColor: 'rgba(0, 0, 0, 0)',
          pointHoverRadius: '5',
          pointHoverBorderWidth: '0',
          pointHoverBackgroundColor: lineBaseColor,
          pointHoverBorderColor: 'rgba(250, 250, 250, 0)'
        }
      }
      else {
        lineColors = {
          backgroundColor: 'rgba(0,0,0,0)',
          borderColor: lineBaseColor,
          pointRadius: '0',
          pointBorderWidth: '0',
          pointBackgroundColor: lineBaseColor,
          pointBorderColor: 'rgba(0, 0, 0, 0)',
          pointHoverRadius: '5',
          pointHoverBorderWidth: '0',
          pointHoverBackgroundColor: lineBaseColor,
          pointHoverBorderColor: 'rgba(250, 250, 250, 0)'
        }
      }
          this.chartColors.push(lineColors);
    }
      
    //rotate colors by 1
    var firstColor = LineGraphData.chartColorsStore[0];
    LineGraphData.chartColorsStore = LineGraphData.chartColorsStore.slice(1, LineGraphData.chartColorsStore.length);
    LineGraphData.chartColorsStore.push(firstColor);
  }
  
  private calcLineColor(i : number) : string {
    //i: 1 . . . (num of lines)
    var storePosition = i % LineGraphData.chartColorsStore.length;
    var loopNum = (i - storePosition) / LineGraphData.chartColorsStore.length;
    if (storePosition == 0) {
      loopNum = loopNum - 1;
    }
      
    var baseColor = LineGraphData.chartColorsStore[storePosition]['borderColor'];
    var minColor = 0;
    var maxColor = 255;
    var colorShift = 35;
      
    baseColor = baseColor.slice('rgba('.length, baseColor.length - 1 );
    var baseRed = parseInt(baseColor.slice(0, baseColor.indexOf(',')));
    baseColor = baseColor.slice( baseColor.indexOf(',') + 1, baseColor.length );
    var baseGreen = parseInt(baseColor.slice(0, baseColor.indexOf(',')));
    baseColor = baseColor.slice( baseColor.indexOf(',') + 1, baseColor.length );
    var baseBlue = parseInt(baseColor.slice(0, baseColor.indexOf(',')));
      
    var colorDiff = colorShift * (loopNum);
     
    var red = baseRed - colorDiff;
    var green = baseGreen - colorDiff;
    var blue = baseBlue - colorDiff;
      
    if (red < minColor) {
      red = minColor
    }
    if (green < minColor) {
      green = minColor;
    }
    if (blue < minColor) {
      blue = minColor;
    }

    return "rgba(" + red + ", " + green + ", " + blue + ", 1)";
  }
   
  public getGraphTitle(): string { return this.graphTitle;  }
  public getChartData(): Array<any> { return this.chartData; }
  public getChartLabels(): Array<any> { return this.chartLabels; }
  public getChartOptions(): Array<any> { return this.chartOptions; }
  public getChartColors(): Array<any> { return this.chartColors; }
  public getChartType(): string { return this.chartType; }
  public getLegend(): boolean { return this.legend; }
}