
export class KeyItem {
  public displayName: string;
  public color: string;
  public lineType: string;

  public checkDashedType() : boolean {
    if ((this.lineType == "dashed") || (this.lineType == "dotted")) {
      return true;
      }
    return false;
  }
    
  public getBorderStyle() : string {
    if (this.lineType == "dashed") {
      return "dotted";
    }
    return "solid";
  }
}