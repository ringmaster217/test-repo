import { Component, OnInit, Input, OnChanges, SimpleChanges, SimpleChange } from '@angular/core';
import { LineGraphData } from './linegraphdata';
import { KeyItem } from './keyitem';

@Component( {
  selector: 'app-line-graph',
  templateUrl: './line-graph.component.html',
  styleUrls: ['./line-graph.component.css']
} )
export class LineGraphComponent implements OnInit {

  @Input()
  graphData: LineGraphData;

  constructor() { }

  ngOnInit() { }

  private checkDashedType( lineType: string ): boolean {
    var keyItem = new KeyItem();
    keyItem.lineType = lineType;
    return keyItem.checkDashedType();
  }

}
