import { Component, EventEmitter, Input, Output, OnInit } from '@angular/core';
import { ReporterService } from '../reporter.service';

import { ProtoReportTypeList } from '../protoreporttypelist'; 

@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.css']
})
export class SideBarComponent implements OnInit {

  @Input()
  reportGroups: ProtoReportTypeList[];

  @Output()
  reportEmitter: EventEmitter<string> = new EventEmitter<string>();

  constructor() {}
  
  ngOnInit() {}
  
  emitReport(report: string) {
      this.reportEmitter.emit(report); 
  }
  
}
