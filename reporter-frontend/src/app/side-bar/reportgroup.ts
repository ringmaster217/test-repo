import { ProtoReport } from '../protoreport';

export class ReportGroup {
  public type: string;
  public reports: ProtoReport[];
}