export class ProtoTable {
  public colNames: Array<any>;
  public rowNames: Array<any>;
  public tableData: any;
  public displayTitle: string;
}
